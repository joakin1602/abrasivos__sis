$(document).ready(main);

function main () {

  controlPliegueDespliegueOferta();

  controlPliegueTodasOfertas();

  controlDespliegueTodasOfertas();

  controlRedimensionPantalla();

}

function controlPliegueDespliegueOferta () {

  var oferta;

  // Al clickar en una cabecera de la oferta
  $(".cabecera-oferta").on("click", function(){

    // Obtenemos la oferta a la que pertenece
    oferta = $(this).parent(".oferta");

    // La oferta esta plegada
    if (oferta.hasClass("plegada")) {

      // La desplegamos
      desplegarOferta(oferta);

    }
    // La oferta esta desplegada
    else {

      // La plegamos
      plegarOferta(oferta);

    }

  });

}

function controlPliegueTodasOfertas () {

  // Al clickar en el boton
  $("#plegar-ofertas").on("click", function(){

    // Nos recorremos todas las ofertas para plegarlas
    $(".oferta").each(function(){

      plegarOferta($(this));

    });

  });

}

function controlDespliegueTodasOfertas () {

  // Al clickar en el boton
  $("#desplegar-ofertas").on("click", function(){

    // Nos recorremos todas las ofertas para desplegarlas
    $(".oferta").each(function(){

      desplegarOferta($(this));

    });

  });

}

function desplegarOferta (oferta) {

  // Obtenemos cual es el alto de la tabla de ofertas, para hacer la transicion correctamente
  var altoTabla = oferta.find("table").height();

  // La desplegamos
  oferta.children(".lineas-oferta").css("height", altoTabla+"px");

  // Le asignamos la clase correspondiente
  oferta.addClass("desplegada");
  oferta.removeClass("plegada");

  // Cambiamos el icono de la flecha
  oferta.find(".icono-flecha").addClass("fa-angle-up");
  oferta.find(".icono-flecha").removeClass("fa-angle-down");

}

function plegarOferta (oferta) {

  // La plegamos
  oferta.children(".lineas-oferta").css("height", "0px");

  // Le asignamos la clase correspondiente
  oferta.addClass("plegada");
  oferta.removeClass("desplegada");

  // Cambiamos el icono de la flecha
  oferta.find(".icono-flecha").addClass("fa-angle-down");
  oferta.find(".icono-flecha").removeClass("fa-angle-up");

}

function controlRedimensionPantalla () {

  // Al redimensionar la pantalla
  $(window).resize(function() {

		// Recalculamos el alto que deberian tener las ofertas desplegadas
    $(".oferta.desplegada").each(function(){

      // Obtenemos cual es el alto de la tabla de ofertas, para hacer la transicion correctamente
      var altoTabla = $(this).find("table").height();

      // Redefinimos el alto de las lineas
      $(this).children(".lineas-oferta").css("height", altoTabla+"px");

    });

	});

}
