$(document).ready(main);

var datos = {};

function main() {
	
	// Recogemos los datos iniciales sobre los registros del panel
	datos.registrosTotalesIniciales = $(".full-total").html();
	datos.registrosTotales = datos.registrosTotalesIniciales;
	datos.paginaActual = parseInt($(".page-number-input").val());
	datos.agrupacionActual = $(".per_page").val();
	datos.paginasTotales = Math.ceil(datos.registrosTotales / datos.agrupacionActual);
	
	controlRefrescarTabla();
	controlBotonesPaginacion();
	controlCambioAgrupacion();
	controlFiltros();
	controlOrdenacion();
	controlLimpiarFiltro();
	
}

function cargarNuevosRegistros () {
	
	leerFiltros();
	obtenerDatosOrdenacion();
	
	var parametros = {
			"cliente" : $("#codigoCliente").val(),
            "agrupacion" : datos.agrupacionActual,
            "pagina" : datos.paginaActual,
            "filtros" : JSON.stringify(datos.filtros),
            "tipo_ordenacion" : datos.tipoOrdenacion,
            "columna_ordenacion" : datos.columnaOrdenacion
   	};

	// Este es el beforeSend
	$(".div-cargando").removeClass("hidden");
	
	setTimeout(function(){

		$.ajax({
				data:  parametros,
				url:   base_url()+'index.php/clientes/obtener_otros_pendientes_entrega',
				type:  'post',
				async: true,
				complete: function () {$(".div-cargando").addClass("hidden");},
				success:  function (response) {
					
					response = $.parseJSON(response);

					if (response.resuelto == "OK") {

						// Incluimos el nuevo contenido
						$(".listado-registros").html(response.registros);
						
						// Cambiamos los indicadores de los registros que se están mostrando
						datos.registrosTotales = response.registrosTotales;
						response.registroInicial = response.registrosTotales > 0 ? response.registroInicial : 0;
						datos.paginasTotales = Math.ceil(datos.registrosTotales / datos.agrupacionActual);
						activacionBotonesPaginacion();
						$(".paging-starts").html(response.registroInicial);
						$(".paging-ends").html(response.registroFinal);
						$(".current-total-results").html(response.registrosTotales);
						
					} else {
						if (response.sesion_expirada == true) {
							alert ("Lo sentimos, su sesión ha expirado.");
							window.location.replace(base_url()+"index.php/login");
						}
						else {
							alert("Ha ocurrido un error");
						}
					}
				}
		});
	
	}, 10);
	
}

function controlRefrescarTabla () {
	
	$(".gc-refresh").click(function(e){
		
		e.preventDefault();
		
		cargarNuevosRegistros();
		
	});
	
}

function controlBotonesPaginacion () {
	
	activacionBotonesPaginacion();
	
	// Cambiamos el valor del input de la página en el caso de que toquemos los botones de paginación
	$(".paging-first").click(function(e){
		
		e.preventDefault();
		
		if (!($(this).hasClass("disabled")))
			$(".page-number-input").changeVal(1);
			
	});
	$(".paging-previous").click(function(e){
		
		e.preventDefault();
		
		if (!($(this).hasClass("disabled")))
			$(".page-number-input").changeVal(datos.paginaActual - 1);
			
	});
	$(".paging-next").click(function(e){
		
		e.preventDefault();
		
		if (!($(this).hasClass("disabled")))
			$(".page-number-input").changeVal(datos.paginaActual + 1);
			
	});
	$(".paging-last").click(function(e){
		
		e.preventDefault();
		
		if (!($(this).hasClass("disabled")))
			$(".page-number-input").changeVal(datos.paginasTotales);
			
	});
	
	// Si cambia el valor del input de la paginación, se cargarán nuevos resultados
	$(".page-number-input").on("change", function(){
		
		var pagina = parseInt($(this).val());
		
		// Comprobamos que no sea mayor o menor al límite
		if (pagina < 1) $(this).val(1);
		else if (pagina > datos.paginasTotales) $(this).val(datos.paginasTotales);
		
		datos.paginaActual = parseInt($(this).val());
		activacionBotonesPaginacion();
		cargarNuevosRegistros();
		
	});
	
}

function activacionBotonesPaginacion () {
	
	// Controlamos los botones de página anterior y primera página
	if (datos.paginaActual > 1) {
		$(".paging-first").removeClass("disabled");
		$(".paging-previous").removeClass("disabled");
	}
	else {
		$(".paging-first").addClass("disabled");
		$(".paging-previous").addClass("disabled");
	}
	
	// Controlamos los botones de página siguiente y última página
	if (datos.paginaActual < datos.paginasTotales) {
		$(".paging-next").removeClass("disabled");
		$(".paging-last").removeClass("disabled");		
	}
	else {
		$(".paging-next").addClass("disabled");
		$(".paging-last").addClass("disabled");		
	}
	
}

function controlCambioAgrupacion() {
	
	$(".per_page").on("change", function() {

		datos.agrupacionActual = $(".per_page").val();
		
		limpiarPaginacion();
		
		cargarNuevosRegistros();
		
	});
	
}

function controlFiltros () {
	
	$(".datepicker-input").datepicker({
		language: 'es',
		weekStart: 1,
		format: 'dd/mm/yyyy',
		autoclose: true
	});
	
	// Al presionar una tecla y que esta sea enter dentro de un cuadro de filtro, iniciaremos el filtrado
	$(".searchable-input, .datepicker-input").on('keyup',function(event){

		// Si es intro lo que pulsamos, buscamos
		if(event.keyCode == 13){
			
			// Quitamos el focus del filtro
			document.activeElement.blur();
			
			limpiarPaginacion();
	      	cargarNuevosRegistros();
	      	
			// Nos recorremos los filtros para ver cuales hay que marcarlos y cuales no
			$(".searchable-input").each(function(){
				
				var input = $(this);
				
				if (input.val() != "") {
					input.addClass("value-not-empty");
					input.siblings(".clear-search").removeClass("hidden");
				}
				else {
					input.removeClass("value-not-empty");
					input.siblings(".clear-search").addClass("hidden");
				}
				
			});
			
		}
		
	});
	
	// Para las fechas, el evento será también on change, sin retardo
	$(".datepicker-input").on('change',function(){

		var input = $(this);
			
      	limpiarPaginacion();
      
      	cargarNuevosRegistros();
      	
      	if (input.val() != "") {
	      	input.addClass("value-not-empty");
	      	input.siblings(".clear-search").removeClass("hidden");
      	}
      	else {
      		input.removeClass("value-not-empty");
	      	input.siblings(".clear-search").addClass("hidden");
      	}
		
	});
	
	// Para los select, el evento será sólo on change, pero sin el control para eliminar el interior del input, sin retardo
	$(".select-checkbox").on('change',function(){

		var input = $(this);
		
		var retardoFiltrado = 750;
			
      	limpiarPaginacion();
      
      	cargarNuevosRegistros();
		
	});
	
	// Cuando se clicka en la x del filtro, se borrará y se rafrescará la tabla
	$(".clear-search").on("click", function() {

		$(this).siblings(".searchable-input").removeClass("value-not-empty");
		$(this).siblings(".searchable-input").val("");
		$(this).addClass("hidden");
		
		limpiarPaginacion();
		
		cargarNuevosRegistros();
		
	});
	
}

function leerFiltros () {
	
	datos.filtros = [];
	
	$(".searchable-input").each(function (index) {
		
		if ($(this).val() != "") {
			
			var filtro = {
				"nombre":$(this).attr("name"), 
				"valor":$(this).val()
			};
			
			datos.filtros.push(filtro);
			
		} 
		
	});
	
}

function controlOrdenacion () {
	
	$(".column-with-ordering").click(function(){
		
		// Si ya tiene orden desc, hay que cambiarselo a asc
		var orderAsc = $(this).hasClass("ordering-asc") ? true : false;
		
		// Borramos la ordenación anterior
		limpiarOrdenacion();
		
		// Aplicamos la nueva ordenación
		if (orderAsc == true) {
			
			$(this).addClass("ordering-desc");
			$(this).append("<i class='fa fa-chevron-up'></i>");
			
		}
		else {
			
			$(this).addClass("ordering-asc");
			$(this).append("<i class='fa fa-chevron-down'></i>");
			
		}
		
		limpiarPaginacion();
		
		cargarNuevosRegistros();
		
	});
	
}

function obtenerDatosOrdenacion () {
	
	// Comprobamos si existe una columna con orden descencente
	if ($(".ordering-desc").length){
		
		datos.tipoOrdenacion = "DESC";
		datos.columnaOrdenacion = $(".ordering-desc").attr("data-order-by");
		
	}
	
	// Comprobamos si existe una columna con orden ascendente
	else if ($(".ordering-asc").length){
		
		datos.tipoOrdenacion = "ASC";
		datos.columnaOrdenacion = $(".ordering-asc").attr("data-order-by");
		
	}
	
	// Si no existe ninguna columna de ordenación, envíamos los datos vacíos
	else {
		
		datos.tipoOrdenacion = false;
		datos.columnaOrdenacion = "";
		
	}
	
}

function controlLimpiarFiltro () {
	
	$("#clear-filtering").click(function(){

		limpiarOrdenacion();
		limpiarFiltros();
		limpiarPaginacion();
		
		cargarNuevosRegistros();
		
	});
	
}

function limpiarOrdenacion () {
	
	$(".column-with-ordering").removeClass("ordering-desc");
	$(".column-with-ordering").removeClass("ordering-asc");
	$(".column-with-ordering").children("i").remove();
	
}

function limpiarFiltros () {
	
	$(".searchable-input").removeClass("value-not-empty");
	$(".searchable-input").val("");
	$(".searchable-input").siblings(".clear-search").addClass("hidden");
	
}

function limpiarPaginacion () {
	
	datos.paginaActual = 1;
	$(".page-number-input").val(1);
	
}

// Función que hace que un evento se dispare después de un tiempo
var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

// Función que fuerza al evento change además de cambiar el value. Por defecto al usar val() no se dispara el change
$.fn.changeVal = function (v) {
    return $(this).val(v).trigger("change");
};
