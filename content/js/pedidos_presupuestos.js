$(document).ready(main);

function main() {

	$("body").removeClass("hidden");

	if('localStorage' in window && window['localStorage'] !== null) {
		controlContinuarUltimo();
	    cargarBorradores();
	}
	controlBotones();
	if('localStorage' in window && window['localStorage'] !== null) {
	    controlEliminarBorrador();
	}

}

function controlContinuarUltimo() {

	var codigo_usuario = $("#codigo_usuario").val();
	var tipo_usuario = $("#tipo_usuario").val();
	var nombre_autoguardado = 'autoguardado_'+tipo_usuario+'_'+codigo_usuario;

	if (localStorage.getItem(nombre_autoguardado) != null) {

		var autoguardado = $.parseJSON(localStorage.getItem(nombre_autoguardado));

		var html = 	"<div class='col-sm-4 col-xs-6 elemento-pedidos-presupuestos'>"+
		  					"<a href='"+base_url()+"index.php/pedidos_presupuestos/nuevo/continuar'>"+
								"<div id='continuar-ultimo' class='panel panel-primary' data-borrador='"+autoguardado.borrador+"'>"+
									"<div class='panel-heading'>"+
										"<h4>Continuar último</h4>"+
									"</div>"+
									"<div class='panel-body'>"+
										"<div class='row'>"+
											"<div class='col-xs-12 text-center padding-icono'>"+
												"<i class='fa fa-refresh icono-pedidos'></i>"+
											"</div>"+
										"</div>"+
									"</div>"+
									"<div class='panel-footer'>"+autoguardado.fecha+"</div>"+
								"</div>"+
							"</a>"+
						"</div>";

		$(".pedidos-presupuestos").append(html);

	}

}

function cargarBorradores() {

	var codigo_usuario = $("#codigo_usuario").val();
	var tipo_usuario = $("#tipo_usuario").val();
	var borradores = localStorage.getItem('borradores_'+tipo_usuario) != null ? $.parseJSON(localStorage.getItem('borradores_'+tipo_usuario)) : [];

	// Nos recorremos todos los borradores guardados en el navegador
	$.each( borradores, function(index, borrador) {

		// Si un borrador coincide con el usuario actual, lo mostramos
		if ((borradores[index].comisionista == codigo_usuario && tipo_usuario == "comisionista") || (borradores[index].codigo_cliente == codigo_usuario && tipo_usuario == "cliente")) {

		  	var html = 	"<div class='col-sm-4 col-xs-6 elemento-pedidos-presupuestos'>"+
		  					"<a class='enlace-borrador' href='"+base_url()+"index.php/pedidos_presupuestos/nuevo/borrador/"+index+"'>"+
								"<div class='panel panel-primary borrador' data-borrador='"+index+"'>"+
									"<div class='panel-heading'>"+
										"<h4>Borrador "+borrador.numero+"</h4>"+
									"</div>"+
									"<div class='panel-body'>"+
										"<div class='row info-borrador'>"+
											"<div class='col-xs-12'>"+borrador.cliente+"</div>"+
											"<div class='col-xs-12'>Líneas: "+borrador.lineas.length+" ("+borrador.total+" €)</div>"+
										"</div>"+
									"</div>"+
									"<div class='panel-footer'>"+borrador.fecha+"</div>"+
								"</div>"+
							"</a>"+
							"<div class='eliminar-borrador'>"+
								"<i class='fa fa-trash'></i>"+
							"</div>"+
						"</div>";

			$(".pedidos-presupuestos").append(html);

		}

	});

}

function controlBotones() {

	$(".panel").on("mouseenter", function(){

		$(this).addClass("panel-hover");

	});

	$(".panel").on("mouseleave", function(){

		$(this).removeClass("panel-hover");

	});

}

function controlEliminarBorrador() {

	$(".eliminar-borrador").click(function(){

		// Obtenemos el array de borradores y el numero del borrador a eliminar
		var tipo_usuario = $("#tipo_usuario").val();
		var borradores = localStorage.getItem('borradores_'+tipo_usuario) != null ? $.parseJSON(localStorage.getItem('borradores_'+tipo_usuario)) : [];
		var numeroBorrador = $(this).siblings("a").children(".borrador").attr("data-borrador");

		// Eliminamos el borrador del array y lo guardamos en localStorage
		borradores.splice(numeroBorrador, 1);
		localStorage.setItem('borradores_'+tipo_usuario, JSON.stringify(borradores));

		// Borramos el html
		$(this).parent(".elemento-pedidos-presupuestos").remove();

		// Nos recorremos los demas borradores para redefinirles el numero de borrador correctamente
		redefinirIndexBorradores (numeroBorrador);

	});

}

function redefinirIndexBorradores (indexBorrado) {

	$(".borrador").each(function() {

		if ($(this).attr("data-borrador") > indexBorrado) {

			var nuevoIndex = $(this).attr("data-borrador") - 1;
			$(this).attr("data-borrador", nuevoIndex);
			$(this).closest(".enlace-borrador").attr("href", base_url()+"index.php/pedidos_presupuestos/nuevo/borrador/"+nuevoIndex);

		}

	});

}

function mostrarOcultarMensaje (mensaje, accion) {

	if (accion == "mostrar") {
		$("#mensaje-"+mensaje).removeClass("hidden");
		$("#mensaje-"+mensaje).removeClass("transparente");
		$("#mensaje-"+mensaje).addClass("opaco");
		delay(function(){
		  	mostrarOcultarMensaje(mensaje, "ocultar");
		}, 2000 );
	}
	else if (accion == "ocultar") {
		$("#mensaje-"+mensaje).removeClass("opaco");
		$("#mensaje-"+mensaje).addClass("transparente");
		setTimeout(function() {
	        $("#mensaje-"+mensaje).addClass("hidden");
	    },500);
	}

}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();
