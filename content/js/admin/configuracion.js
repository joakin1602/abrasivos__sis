$(document).ready(main);

var global = {};

function main () {

  controlGuardadoConfiguracion();

}

function controlGuardadoConfiguracion () {

  $("#guardar-configuracion").on("click", function() {

    // Comprobamos que todos los campos sean correctos
    if (comprobarCamposCorrectos()) {

          var datos = {
            "emails_envio" : $("#emails-envio").val()
          };

          // Desactivamos el boton de añadir
          deshabilitar($("#guardar-configuracion"));

          setTimeout(function(){

            $.ajax({
                data:  datos,
                url:   base_url()+'index.php/configuracion/guardar_configuracion',
                type:  'post',
                complete: function () {
                  habilitar($("#guardar-configuracion"));
                },
                success:  function (respuesta) {

                  respuesta = $.parseJSON(respuesta);

                  if (respuesta.resuelto == "OK") {

                    alert("Configuración guardada correctamente");

                  }
                  else if (respuesta.sesion_expirada == true) {
        						alert ("Lo sentimos, su sesión ha expirado.");
        						redireccion("/index.php/login");
        					}
        					else {
        						alert(respuesta.mensaje);
        					}

                },
                error: function () {
                  alert("Ha ocurrido un error");
                }
            });

          }, 10);

    }
    // Algun campo es incorrecto
    else alert("Debes rellenar todos los campos obligatorios.");

  });

}

function comprobarCamposCorrectos () {

  var i;
  var camposCorrectos = true;
  var campos = [
    $("#emails-envio")
  ];

  for (i=0; i<campos.length; i++) {

    // Si el campo no existe, lo damos por incorrecto
    if (campos[i].length == 0) camposCorrectos = false;

    // El campo existe
    else {

      // Si el campo es obligatorio y ademas esta vacio, lo damos por incorrecto
      if (campos[i].hasClass("obligatorio") && campos[i].val() == "")  camposCorrectos = false;

    }

  }

  return camposCorrectos;

}
