$(document).ready(main);

var global = {};

function main () {

  recogidaDeUsuariosInicial();

  controlesFormularioNueva();

  controlBorradoKey();

}

function recogidaDeUsuariosInicial () {

  // Recogemos los datos
  global.comisionistas = $.parseJSON($("#comisionistas").val());
  global.clientes = $.parseJSON($("#clientes").val());

  // Borramos el div con los datos
  $(".datos-js").remove();

}

function controlesFormularioNueva () {

  var tipoUsuario;

  // Generar Key
  $("#generar-key").on("click", function(){

    $("#key-login").val(randomString(6));

  });

  // Cambio de tipo de usuario
  $("#tipo-usuario").on("change", function(){

    // Obtenemos el tipo de usuario seleccionado
    tipoUsuario = $(this).val();

    // Definimos las options del usuario
    definirOptionsUsuario(tipoUsuario);

  });

  $("#nueva-key").on("submit", function(e){

    e.preventDefault();

    registrarNuevaKey();

  });

}

function randomString (longitud) {

  var string = "";
  var caracteres = "abcdefghijkmnpqrstuvwxyz123456789";

  for (var i = 0; i < longitud; i++)
    string += caracteres.charAt(Math.floor(Math.random() * caracteres.length));

  return string;

}

function definirOptionsUsuario (tipoUsuario) {

  var htmlOptions;
  var usuarios;
  var i;

  if (tipoUsuario == "admin") {

    // Solo tendra la opcion de que sea admin
    htmlOptions = "<option value='admin'>admin</option>";

    // El select estara deshabilitado
    $("#id_usuario").attr("disabled", true);

  }

  else {

    htmlOptions = "<option value=''></option>";

    // Definimos los usuarios dependiendo del tipo elegido
    if (tipoUsuario == "comisionista") usuarios = global.comisionistas;
    else if (tipoUsuario == "cliente") usuarios = global.clientes;
    else usuarios = []

    // Definimos el html de las options
    for (i=0; i<usuarios.length; i++) {

      htmlOptions += "<option value='"+usuarios[i].codigo+"'>"+usuarios[i].codigo+" - "+usuarios[i].nombre+"</option>";

    }

    // El select estara habilitado
    $("#id_usuario").attr("disabled", false);

  }

  // Asignamos las nuevas options
  $("#id_usuario").html(htmlOptions);

}

function registrarNuevaKey () {

  var datos = {
    "key" : $("#key-login").val(),
    "tipo_usuario" : $("#tipo-usuario").val(),
    "id_usuario" : $("#id_usuario").val(),
    "descripcion" : $("#descripcion").val()
  };

  if (comprobarDatosKeyCorrectos(datos)) {

    // Desactivamos el boton de añadir
    deshabilitar($("#nueva-key").find("button"));

    setTimeout(function(){

      $.ajax({
          data:  datos,
          url:   base_url()+'index.php/keys_login/registrar_key',
          type:  'post',
          complete: function () {
            habilitar($("#nueva-key").find("button"));
          },
          success:  function (respuesta) {

            respuesta = $.parseJSON(respuesta);

            if (respuesta.resuelto == "OK") {

              redireccion("/index.php/keys_login");

            }
            else if (respuesta.sesion_expirada == true) {
  						alert ("Lo sentimos, su sesión ha expirado.");
  						redireccion("/index.php/login");
  					}
  					else {
  						alert(respuesta.mensaje);
  					}

          },
          error: function () {
            alert("Ha ocurrido un error");
          }
      });

    }, 10);

  }

}

function comprobarDatosKeyCorrectos (datos) {

  var correcto = true;

  // Si hay algun campo obligatorio vacio
  if (datos.key == "" || datos.key == undefined || datos.tipo_usuario == "" || datos.tipo_usuario == undefined  || datos.id_usuario == "" || datos.id_usuario == undefined ) {

    correcto = false;
    alert("Debes rellenar todos los campos obligatorios");

  }

  // Si hay algun fallo en el tipo de usuario
  else if (datos.tipo_usuario != "admin" && datos.tipo_usuario != "comisionista" && datos.tipo_usuario != "cliente") {

    correcto = false;
    alert("El tipo de usuario es incorrecto");

  }

  // Si el admin es erroneo
  else if (datos.tipo_usuario == "admin" && datos.id_usuario != "admin") {

    correcto = false;
    alert("El usuario es incorreto");

  }

  return correcto;

}

function controlBorradoKey () {

  var post;

  $(".borrar-key").on("click", function(){

    // Pedimos confirmacion
    if (confirm("¿Estás seguro que quieres borrar la key?")) {

      // Deshabilitamos todos los botones de borrado
      deshabilitar($(".borrar-key"));

      post = {
        "id_key" : $(this).siblings(".id-key").val()
      };

      setTimeout(function(){

        $.ajax({
            data:  post,
            url:   base_url()+'index.php/keys_login/eliminar_key',
            type:  'post',
            complete: function () {
              habilitar($(".borrar-key"));
            },
            success:  function (respuesta) {

              respuesta = $.parseJSON(respuesta);

              if (respuesta.resuelto == "OK") {

                redireccion("/index.php/keys_login");

              }
              else alert(respuesta.mensaje);

            },
            error: function () {
              alert("Ha ocurrido un error");
            }
        });

      }, 10);

    }

  });

}
