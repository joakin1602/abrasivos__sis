$(document).ready(function(){

	controlArribaPagina();
	controlCerrarEmergente();
	analytics();

});

function base_url () {

	return "https://sis.abrasivos.net:8443/";

}

function redireccion (url) {

	window.location.href = url;

}

function deshabilitar (elemento) {

	elemento.attr("disabled", true);

}

function habilitar (elemento) {

	elemento.attr("disabled", false);

}

function controlArribaPagina () {

	$(".boton-arriba-pagina").click(function(){
		$("html, body").animate({scrollTop:0}, 200);
	});

}

function controlCerrarEmergente () {

	// Control del cierre de las modales a partir del boton
	$(".boton-cerrar-emergente").on("click",function(){
		var modal = $(this).closest(".pantalla-emergente");
		modal.addClass("hidden");
	});

	// Si clickamos fuera de la modal, ésta se cerrará
	$(".pantalla-emergente").mouseup(function (e) {
	    var container = $(".modal-dialog");
		if (!container.is(e.target) // if the target of the click isn't the container...
		    && container.has(e.target).length === 0) // ... nor a descendant of the container
		{
	    	modal = container.parent();
			$(this).addClass("hidden");
	    }

	});

}

function analytics () {

	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-44581662-1', 'auto');
	ga('send', 'pageview');

}
