
var global = {};

// Cuando todo se cargue, ponemos el body como visible y tambien llamamos al main
$(window).load(function(){ $("body").removeClass("hidden"); });
$(document).ready(main);

function main() {

	global.articulosCargados = 0;
	global.empresaSeleccionada = false;
	global.numeroBorrador = false;
	global.numeroPedido = $("#numero").val();

	// Comprobamos que el navegador soporta localStorage
	if('localStorage' in window && window['localStorage'] !== null) {
	    controlBorrador();
	    controlAutoguardado();
	}

	controlModales();
	controlFiltros();
	controlSeleccionCliente();
	controlSeleccionArticulo();
	controlSeleccionLineaHistorico();
	controlSeleccionLineaPendiente();
	controlCalculosLinea();
	controlAgregarBorrarLinea();
	controlEnviarFormulario();
	controlInformacionPlegable();
	controlContadorCaracteresObservaciones();
	controlBorradoInputFocus();
	controlBotonBorrarFormulario();
	controlBotonesBorrarInput();
	controlCambioTipoFormulario();

}

function controlBorrador() {

	$("#boton-borrador").removeClass("hidden");

	var numeroBorrador = $("#boton-borrador").attr("data-borrador");

	// Comprobamos si tenemos que obtener los borradores de comisionistas o clientes
	var tipo_usuario = $("#tipo_usuario").val();
	if (tipo_usuario == "comisionista") {
		var codigo_usuario = $("#comisionista").val();
	}
	else if (tipo_usuario == "cliente") {
		var codigo_usuario = $("#codigo_cliente").val();
	}
	var borradores = localStorage.getItem('borradores_'+tipo_usuario) != null ? $.parseJSON(localStorage.getItem('borradores_'+tipo_usuario)) : [];

	// Si estamos accediendo a un borrador, el numeroBorrador no sera vacio
	if (numeroBorrador != "") {

		// Si existe un borrador con ese numero, y el codigo del usuario coincide con el comisionista o el cliente del borrador
		if (borradores[numeroBorrador] != undefined && (borradores[numeroBorrador].comisionista == codigo_usuario || borradores[numeroBorrador].codigo_cliente == codigo_usuario)) {

			// Definimos el numero de borrador como variable global
			global.numeroBorrador = numeroBorrador;

			var borrador = borradores[numeroBorrador];

			// Rellenamos el formulario con los datos del borrador
			$("#tipo").val(borrador.tipo);
			$("#codigo_cliente").val(borrador.codigo_cliente);
			$("#nombre_cliente").val(borrador.nombre_cliente);
			$("#tarifa_cliente").val(borrador.tarifa_cliente);
			$("#codigo_nombre_cliente").val(borrador.cliente);
			$("#subpedido_web").val(borrador.subPedido);
			$("#emails_envio").val(borrador.emails_envio);
			$("#observaciones").val(borrador.observaciones);
			$("#total-pedido-html").html(borrador.total);
			$("#total-pedido").val(borrador.total);

			// Si el tipo del borrador es presupuesto, el numero del formulario sera p
			if (borrador.tipo == "Presupuesto") $("#numero").val("p");

			// Si existen lineas en el borrador, las incluimos
			if (borrador.lineas.length > 0) {

				$("#titulos-columnas-lineas-abajo").removeClass("hidden");

				for (var i=0; i<borrador.lineas.length; i++) {

					agregarLinea(borrador.lineas[i]);

				}

			}

			// Si el borrador tiene empresa seleccionada, se habilitan los botones del cliente
			if (borrador.empresaSeleccionada == true) {
				global.empresaSeleccionada = true;
				$("#boton-historico").removeAttr("disabled");
				$("#boton-pendientes").removeAttr("disabled");
			}

		}
		// Si intentamos acceder a un borrador erroneo, redirigimos al formulario en blanco
		else {

			window.location.replace(base_url()+"index.php/pedidos_presupuestos/nuevo/");

		}
	}
	// Si es un formulario sin borrador, asignamos el numero de borrador siguiente
	else {

		numeroBorrador = borradores.length;

	}

	$("#numero-borrador").html($("#numero").val());

	$("#boton-borrador").click(function(){

		borradores = localStorage.getItem('borradores_'+tipo_usuario) != null ? $.parseJSON(localStorage.getItem('borradores_'+tipo_usuario)) : [];

		var datosFormulario = recogerDatosFormulario();

		borradores[numeroBorrador] = datosFormulario;

		localStorage.setItem('borradores_'+tipo_usuario, JSON.stringify(borradores));

		mostrarOcultarMensaje("borrador", "mostrar");

	});

	$("#mensaje-borrador").click(function(){
		mostrarOcultarMensaje("borrador", "ocultar");
	});

}

function controlAutoguardado() {

	$(document.body).on('change', "#codigo_nombre_cliente, #tipo, #subpedido_web, #emails_envio, #observaciones, #codigo_nombre_articulo, .cantidad_articulo, .descuento_articulo", autoguardar);
	$(document.body).on('click', ".seleccionar-cliente, .seleccionar-articulo, .incluir-linea-pendiente, #add-linea, .prioridad-linea, .borrar-linea", autoguardar);

	var url = window.location.pathname.split("/");
	var accesoAutoguardado = (url[url.length-1] == "continuar") || ((url[url.length-1] == "") && (url[url.length-2] == "continuar")) ? true : false;

	// Obtenemos los datos del usuario dependiendo de su tipo
	var tipo_usuario = $("#tipo_usuario").val();
	if (tipo_usuario == "comisionista") {
		var codigo_usuario = $("#comisionista").val();
	}
	else if (tipo_usuario == "cliente") {
		var codigo_usuario = $("#codigo_cliente").val();
	}

	// Comprobamos que se este intentando acceder al autoguardado
	if (accesoAutoguardado == true) {

		// Comprobamos que exista autoguardado
		if (localStorage.getItem('autoguardado_'+tipo_usuario+"_"+codigo_usuario) != null) {

			var autoguardado = $.parseJSON(localStorage.getItem('autoguardado_'+tipo_usuario+"_"+codigo_usuario));

			// Rellenamos los camopos con los datos del autoguardado
			$("#tipo").val(autoguardado.tipo);
			$("#codigo_cliente").val(autoguardado.codigo_cliente);
			$("#nombre_cliente").val(autoguardado.nombre_cliente);
			$("#tarifa_cliente").val(autoguardado.tarifa_cliente);
			$("#codigo_nombre_cliente").val(autoguardado.cliente);
			$("#subpedido_web").val(autoguardado.subPedido);
			$("#emails_envio").val(autoguardado.emails_envio);
			$("#observaciones").val(autoguardado.observaciones);
			$("#total-pedido-html").html(autoguardado.total);
			$("#total-pedido").val(autoguardado.total);

			// Si hay lineas, las imprimimos
			if (autoguardado.lineas.length > 0) {

				$("#titulos-columnas-lineas-abajo").removeClass("hidden");

				for (var i=0; i<autoguardado.lineas.length; i++) {

					agregarLinea(autoguardado.lineas[i]);

				}

			}

			if (autoguardado.empresaSeleccionada == true) {
				global.empresaSeleccionada = true;
				$("#boton-historico").removeAttr("disabled");
				$("#boton-pendientes").removeAttr("disabled");
			}

		}

		// Si no existe autoguardado, lo mandamos a la url de pedido nuevo
		else {

			window.location.replace(base_url()+"index.php/pedidos_presupuestos/nuevo");

		}

	}

}

function autoguardar() {

 	// Guardamos después de un pequeño retardo para que antes se realicen todos los cambios y cálgulos del formulario
	delay(function(){

		// Obtenemos los datos del usuario dependiendo de su tipo
		var tipo_usuario = $("#tipo_usuario").val();
		if (tipo_usuario == "comisionista") {
			var codigo_usuario = $("#comisionista").val();
		}
		else if (tipo_usuario == "cliente") {
			var codigo_usuario = $("#codigo_cliente").val();
		}

		datos = recogerDatosFormulario();
		$.parseJSON(localStorage.setItem('autoguardado_'+tipo_usuario+"_"+codigo_usuario, JSON.stringify(datos)));

	}, 500 );

}

function mostrarOcultarMensaje (mensaje, accion) {

	if (accion == "mostrar") {
		$("#mensaje-"+mensaje).removeClass("hidden");
		$("#mensaje-"+mensaje).removeClass("transparente");
		$("#mensaje-"+mensaje).addClass("opaco");
		delay(function(){
		  	mostrarOcultarMensaje(mensaje, "ocultar");
		}, 2000 );
	}
	else if (accion == "ocultar") {
		$("#mensaje-"+mensaje).removeClass("opaco");
		$("#mensaje-"+mensaje).addClass("transparente");
		setTimeout(function() {
	        $("#mensaje-"+mensaje).addClass("hidden");
	    },500);
	}

}

function controlModales() {

	// Control del cierre de las modales a partir del boton
	$(".boton-cerrar-emergente").on("click",function(){
		var modal = $(this).closest(".pantalla-emergente");
		modal.addClass("hidden");
		limpiarFiltros(modal);
	});

	// Si clickamos fuera de la modal, ésta se cerrará
	$(".pantalla-emergente").mouseup(function (e) {
	    var container = $(".modal-dialog");
		if (!container.is(e.target) // if the target of the click isn't the container...
		    && container.has(e.target).length === 0) // ... nor a descendant of the container
		{
	    	modal = container.parent();
			$(this).addClass("hidden");
			limpiarFiltros(modal);
	    }

	});

	// Pulsar en la lupa para buscar clientes, o pulsar enter con el focus en el input
	$("#buscar-cliente").on("click",function(){
		$("#modal-clientes").removeClass("hidden");
		cargarClientes();
	});
	$('#codigo_nombre_cliente').keypress(function(event){
	  	if(event.keyCode == 13){
	  		document.activeElement.blur();
	    	$("#modal-clientes").removeClass("hidden");
			cargarClientes();
	  	}
	});

	// Mostramos el histórico de la empresa seleccionada en una ventana emergente
	$("#boton-historico").on("click",function(){

		var cliente = $("#codigo_cliente").val();

		if (cliente != "") {
			$("#modal-historico").removeClass("hidden");
			cargarHistorico(cliente);
		}
		else {
			alert("Debes seleccionar un cliente correcto antes");
		}

	});

	// Mostramos los pedidos pendientes de la empresa seleccionada en una ventana emergente
	$("#boton-pendientes").on("click",function(){
		var cliente = $("#codigo_cliente").val();

		if (cliente != "") {
			$("#modal-pendientes").removeClass("hidden");
			cargarPendientes(cliente);
		}
		else {
			alert("Debes seleccionar un cliente correcto antes");
		}

	});

	// Pulsar en la lupa para buscar artículos, o pulsar enter con el focus en el input
	$("#buscar-articulos").on("click",function(){
		cargarArticulos();
	});
	$('#codigo_articulo, #nombre_articulo').keypress(function(event){
	  	if(event.keyCode == 13){
	  		document.activeElement.blur();
				cargarArticulos();
	  	}
	});

	// Abajo de la ventana emergente de artículos, podemos buscar más, ya que se limita a buscar de 500 en 500
	$("#boton-cargar-mas-articulos").click(function(){
		filtros = obtenerValorFiltros("modal-articulos");
		cargarArticulos(filtros, true);
	});

	// Cuando pulsemos en el boton del stock de los articulos en el listado de articulos, mostraremos la modal de stock
	$(document).on("click", ".boton-stock-articulo", function(){

		// Recogemos el estado del stock
		var bolitaEstado;
		var textoEstado;
		if ($(this).hasClass("btn-success")) {
			textoEstado = "Disponible";
			bolitaEstado = "estado-disponible";
		}
		else if ($(this).hasClass("btn-warning")) {
			textoEstado = "Disponible parcialmente";
			bolitaEstado = "estado-proximamente";
		}
		else {
			textoEstado = "Agotado";
			bolitaEstado = "estado-agotado";
		}

		var td = $(this).parent("td");

		// Recogemos los datos de stock
		var datosStock = {
			"codigoArticulo" : td.siblings(".codigo-articulo").val(),
			"stockVirtual" : td.siblings(".stock-virtual-articulo").val(),
			"stockDisponible" : td.siblings(".stock-disponible-articulo").val(),
			"pendienteServir" : td.siblings(".pendiente-servir-articulo").val(),
			"pendienteRecibir" : td.siblings(".pendiente-recibir-articulo").val(),
			"pendienteFabricar" : td.siblings(".pendiente-fabricar-articulo").val(),
			"bolitaEstado" : bolitaEstado,
			"textoEstado" : textoEstado
		};

		// Rellenamos la modal de stock
		rellenarModalStock(datosStock);

		// Ocultamos la modal
		$("#modal-stock-articulo").removeClass("hidden");

	});

	// Cuando pulsemos en el boton del stock de los articulos en la linea de edicion, mostraremos la modal de stock
	$(document).on("click", ".boton_stock_articulo", function(){

		// Recogemos el estado del stock
		var bolitaEstado;
		var textoEstado;
		if ($(this).hasClass("btn-success")) {
			textoEstado = "Disponible";
			bolitaEstado = "estado-disponible";
		}
		else if ($(this).hasClass("btn-warning")) {
			textoEstado = "Próximamente";
			bolitaEstado = "estado-proximamente";
		}
		else {
			textoEstado = "Agotado";
			bolitaEstado = "estado-agotado";
		}

		// Recogemos los datos de stock
		var datosStock = {
			"codigoArticulo" : $(this).siblings(".codigo_articulo_seleccionado").val(),
			"stockVirtual" : $(this).siblings(".stock_virtual_articulo").val(),
			"stockDisponible" : $(this).siblings(".stock_disponible_articulo").val(),
			"pendienteServir" : $(this).siblings(".pendiente_servir_articulo").val(),
			"pendienteRecibir" : $(this).siblings(".pendiente_recibir_articulo").val(),
			"pendienteFabricar" : $(this).siblings(".pendiente_fabricar_articulo").val(),
			"bolitaEstado" : bolitaEstado,
			"textoEstado" : textoEstado
		};

		// Rellenamos la modal de stock
		rellenarModalStock(datosStock);

		// Ocultamos la modal
		$("#modal-stock-articulo").removeClass("hidden");

	});

	// Si pulsamos en el boton de arriba, haremos scroll hacia arriba de la modal
	$(".boton-arriba-modal").click(function(){
		$(this).parent().animate({scrollTop:0}, 200);
	});

}

function controlFiltros() {

	$(".datepicker-input").datepicker({
		language: 'es',
		weekStart: 1,
		format: 'dd/mm/yyyy',
		autoclose: true
	});

	// Al presionar una tecla y que esta sea enter dentro de un cuadro de filtro, iniciaremos el filtrado
	$(".searchable-input").on('keyup',function(event){

		// Si es intro lo que pulsamos, no esperamos los 750 ms para filtrar
		if(event.keyCode == 13){

			// Quitamos el focus del filtro
			document.activeElement.blur();

			var modal = $(this).closest(".pantalla-emergente").attr("id");

			filtrarModal(modal);

			// Nos recorremos los filtros para ver cuales hay que marcarlos y cuales no
			$(this).closest(".pantalla-emergente").find(".searchable-input").each(function(){

				var input = $(this);

				if (input.val() != "") {
					input.addClass("value-not-empty");
					input.siblings(".clear-search").removeClass("hidden");
				}
				else {
					input.removeClass("value-not-empty");
					input.siblings(".clear-search").addClass("hidden");
				}

			});

		}

	});

	// Para las fechas, el evento tambien sera on change
	$(".datepicker-input").on('change',function(){

		var modal = $(this).closest(".pantalla-emergente").attr("id");
		var input = $(this);

		var retardoFiltrado = 750;

		delay(function(){

	      	filtrarModal(modal);

	      	if (input.val() != "") {
		      	input.addClass("value-not-empty");
		      	input.siblings(".clear-search").removeClass("hidden");
	      	}
	      	else {
	      		input.removeClass("value-not-empty");
		      	input.siblings(".clear-search").addClass("hidden");
	      	}

	    }, retardoFiltrado );

	});

	// Cuando se clicka en la x del filtro, se borrará y se rafrescará la tabla
	$(".clear-search").on("click", function() {

		var modal = $(this).closest(".pantalla-emergente").attr("id");

		$(this).siblings(".searchable-input").removeClass("value-not-empty");
		$(this).siblings(".searchable-input").val("");
		$(this).addClass("hidden");

		filtrarModal(modal);

	});

}

function limpiarFiltros(modal) {

	modal.find(".searchable-input").removeClass("value-not-empty");
	modal.find(".searchable-input").val("");
	modal.find(".clear-search").addClass("hidden");

}

// Funcion que al modificar un filtro, vuelve a cargar los registros de una modal
function filtrarModal (modal) {

	switch (modal) {

		case "modal-clientes":
			filtros = obtenerValorFiltros("modal-clientes");
			cargarClientes(filtros);
			break;

		case "modal-historico":
			var cliente = $("#codigo_cliente").val();
			if (cliente != "") {
				filtros = obtenerValorFiltros("modal-historico");
				cargarHistorico(cliente, filtros);
			}
			break;

		case "modal-pendientes":
			var cliente = $("#codigo_cliente").val();
			if (cliente != "") {
				filtros = obtenerValorFiltros("modal-pendientes");
				cargarPendientes(cliente, filtros);
			}
			break;

		case "modal-articulos":
			filtros = obtenerValorFiltros("modal-articulos");
			cargarArticulos(filtros);
			break;

	}

}

function obtenerValorFiltros(modal) {

	// Definimos los filtros a enviar. Si estan definidos, sera un array en blanco
	var filtros = [];
	$("#"+modal).find(".searchable-input").each(function(){
		if ($(this).val() != "") {
			var filtro = {
				"nombre" : $(this).attr("name"),
				"valor" : $(this).val()
			};
			filtros.push(filtro);
		}

	});

	return filtros;

}

function cargarClientes(filtros) {

	// Asignamos un array vacio a filtros si el parametro viene vacio
	filtros = filtros || [];

	var parametros = {
		"nombre_cliente" : $("#codigo_nombre_cliente").val(), // Finalmente el campo codigo_nombre_cliente solo filtrara por el nombre
		"codigo_cliente" : $("#codigo_cliente").val(),
		"filtros" : JSON.stringify(filtros)
	};

	// Este es el beforeSend
	$(".div-cargando").removeClass("hidden");
	$("#tabla-usuarios").find(".listado-registros").html("");
	$("#total_clientes").html("-");

	setTimeout(function(){

		$.ajax({
				data:  parametros,
				url:   base_url()+'index.php/pedidos_presupuestos/obtener_clientes',
				type:  'post',
				async: true,
				complete: function () {$(".div-cargando").addClass("hidden");},
				success:  function (response) {
					response = $.parseJSON(response);
					if (response.resuelto == "OK") {
						rellenarClientes(response.clientes);
					} else {
						if (response.sesion_expirada == true) {
							alert ("Lo sentimos, su sesión ha expirado.");
							redirigirAlLogin();
						}
						else {
							alert("Ha ocurrido un error");
						}
					}
				},
				error: function () {alert("Ha ocurrido un error"); }
		});

	}, 10);

}

function rellenarClientes(clientes) {

	var html = "";
	var odd_even;
	var tarifa2;

	// Recorremos los clientes para mostrarlos
	for (var i=0; i<clientes.length; i++) {

		odd_even = i % 2 == 0 ? "odd" : "even";

		tarifa2 = clientes[i].tarifa == 2 ? "cliente-tarifa2" : "";

		html += "<tr class='"+odd_even+" "+tarifa2+"'><td><button type='button' class='btn btn-primary seleccionar-cliente'>Seleccionar</button></td>"+
				"<td>"+clientes[i].codigo+"</td>"+"<td>"+clientes[i].nombre+"</td>"+
				"<td>"+clientes[i].municipio+"</td>"+
				"<input type='hidden' class='codigo-cliente' value='"+clientes[i].codigo+"' />"+
				"<input type='hidden' class='nombre-cliente' value='"+clientes[i].nombre+"' />"+
				"<input type='hidden' class='email-cliente' value='"+clientes[i].email+"' />"+
				"<input type='hidden' class='tarifa-cliente' value='"+clientes[i].tarifa+"' /></tr>";

	}

	$("#total_clientes").html(clientes.length);

	$("#tabla-usuarios").find(".listado-registros").html(html);

}

function controlSeleccionCliente() {

	// Cuando seleccionamos un cliente, llevamos sus datos a los inputs del formulario
	$(document.body).on('click', ".seleccionar-cliente", function(){

		// Limpiamos los filtros
		modal = $("#modal-clientes");
		limpiarFiltros(modal);

		// Recogemos los datos del cliente
		var codigo = $.trim($(this).parent().siblings('.codigo-cliente').val());
		var nombre = $.trim($(this).parent().siblings('.nombre-cliente').val());
		var email = $.trim($(this).parent().siblings('.email-cliente').val()) != "null" ? $.trim($(this).parent().siblings('.email-cliente').val()) : "";
		var nuevaTarifa = $.trim($(this).parent().siblings('.tarifa-cliente').val());
		var antiguaTarifa = $("#tarifa_cliente").val();

		if (antiguaTarifa != nuevaTarifa) limpiarLineasFormulario();

		// Asignamos los datos del cliente a los inputs
		$("#codigo_nombre_cliente").val(codigo+" - "+nombre);
		$("#codigo_cliente").val(codigo);
		$("#nombre_cliente").val(nombre);
		$("#emails_envio").val(email);
		$("#tarifa_cliente").val(nuevaTarifa);

		// Habilitamos los botones de pendientes e historico
		$("#boton-historico").removeAttr("disabled");
		$("#boton-pendientes").removeAttr("disabled");
		global.empresaSeleccionada = true;

		$("#modal-clientes").addClass("hidden");

	});

	// Cuando se escribe en el input del cliente, se deshabilita el histórico y los pendientes
	$("#codigo_nombre_cliente").on("keyup", function(){

		var tipo_usuario = $("#tipo_usuario").val();

		// Si el tipo_usuario es cliente, no borramos nada
		if (tipo_usuario != "cliente") {
			$("#boton-historico").attr("disabled","disabled");
			$("#boton-pendientes").attr("disabled","disabled");
			global.empresaSeleccionada = false;
		}

	});

}

function cargarHistorico(cliente, filtros) {

	// Asignamos un array vacio a filtros si el parametro viene vacio
	filtros = filtros || [];

	var parametros = {
		"cliente" : cliente,
		"filtros" : JSON.stringify(filtros)
	};

	// Este es el beforeSend
	$(".div-cargando").removeClass("hidden");
	$("#tabla-historico").find(".listado-registros").html("");
	$("#total_historico").html("-");

	setTimeout(function(){

		$.ajax({
				data:  parametros,
				url:   base_url()+'index.php/pedidos_presupuestos/obtener_historico',
				type:  'post',
				async: true,
				complete: function () {$(".div-cargando").addClass("hidden");},
				success:  function (response) {
					response = $.parseJSON(response);
					if (response.resuelto == "OK") {
						rellenarHistorico(response.historico);
					} else {
						if (response.sesion_expirada == true) {
							alert ("Lo sentimos, su sesión ha expirado.");
							redirigirAlLogin();
						}
						else {
							alert("Ha ocurrido un error");
						}
					}
				},
				error: function () {alert("Ha ocurrido un error"); }
		});

	}, 10);

}

function rellenarHistorico(historico) {

	var html = "";
	var odd_even;

	// Recorremos los registros para mostrarlos
	for (var i=0; i<historico.length; i++) {

		odd_even = i % 2 == 0 ? "odd" : "even";

		if (historico[i].descripcion == "" || historico[i].descripcion == null) historico[i].descripcion = "-";
		if (isNaN(parseInt(historico[i].unidadesUltimo))) historico[i].unidadesUltimo = 0;

		html += "<tr class='"+odd_even+"'><td><button type='button' class='btn btn-primary incluir-linea-historico'>Añadir</td>"+
				"</td><td>"+historico[i].codigo+"</td><td>"+historico[i].descripcion+"</td>"+
				"<td>"+fechaToString(historico[i].fechaUltimo)+"</td><td>"+parseInt(historico[i].unidadesUltimo)+"</td>"+
				"<td>"+toFloat(historico[i].precioUltimo, 3)+"€</td><td>"+toFloat(historico[i].descuentoUltimo, 2)+"%</td>"+
				"<input type='hidden' class='codigo-articulo' value='"+historico[i].codigo+"' />"+
				"<input type='hidden' class='cantidad-articulo' value='"+parseInt(historico[i].unidadesUltimo)+"' />"+
				"<input type='hidden' class='descuento-articulo' value='"+toFloat(historico[i].descuentoUltimo, 2)+"' /></tr>";

	}

	$("#total_historico").html(historico.length);

	$("#tabla-historico").find(".listado-registros").html(html);

}

function controlSeleccionLineaHistorico() {

	$(document.body).on('click', ".incluir-linea-historico", function(){

		// Limpiamos los filtros
		modal = $("#modal-historico");
		limpiarFiltros(modal);

		// Tomamos los datos de la linea
		var lineaHistorico = {
			"codigoArticulo" : $(this).parent().siblings(".codigo-articulo").val(),
			"cantidadArticulo" : $(this).parent().siblings(".cantidad-articulo").val(),
			"descuentoArticulo" : $(this).parent().siblings(".descuento-articulo").val()
		}

		// Definimos el codigo del articulo, para hallar los datos sobre el
		var parametros = {
			"codigo_articulo" : $(this).parent().siblings(".codigo-articulo").val(),
			"codigo_cliente" : $("#codigo_cliente").val()
		};

		// BeforeSend
		$(".div-cargando").removeClass("hidden");

		setTimeout(function(){

			$.ajax({
					data:  parametros,
					url:   base_url()+'index.php/pedidos_presupuestos/obtener_datos_articulo',
					type:  'post',
					async: true,
					complete: function () {$(".div-cargando").addClass("hidden");},
					success:  function (response) {
						response = $.parseJSON(response);
						if (response.resuelto == "OK") {
							// Recogemos los datos del articulo
							articulo = response.articulo;
							lineaHistorico.descripcionArticulo = articulo.descripcion;
							lineaHistorico.stockVirtualArticulo = articulo.stockVirtual;
							lineaHistorico.stockDisponibleArticulo = articulo.stockDisponible;
							lineaHistorico.pendienteServirArticulo = articulo.pendienteServir;
							lineaHistorico.pendienteRecibirArticulo = articulo.pendienteRecibir;
							lineaHistorico.pendienteFabricarArticulo = articulo.pendienteFabricar;
							lineaHistorico.precioArticulo = articulo.precio;
							// Rellenamos la linea a partir de los datos de la linea del historico
							rellenarLineaInicial(lineaHistorico);
							//Cerramos el cuadro modal del historico
							$("#modal-historico").addClass("hidden");
							/*// Ponemos el focus en la descripcion del producto
							focusDescripcionArticulo();*/

						} else {
							if (response.sesion_expirada == true) {
								alert ("Lo sentimos, su sesión ha expirado.");
								redirigirAlLogin();
							}
							else {
								alert("Ha ocurrido un error");
							}
						}
					},
					error: function () {alert("Ha ocurrido un error"); }
			});

		}, 10);

	});

}

function cargarPendientes(cliente, filtros) {

	// Asignamos un array vacio a filtros si el parametro viene vacio
	filtros = filtros || [];

	var parametros = {
		"cliente" : cliente,
		"filtros" : JSON.stringify(filtros)
	};

	// Este es el beforeSend
	$(".div-cargando").removeClass("hidden");
	$("#tabla-pendientes").find(".listado-registros").html("");
	$("#total_pendientes").html("-");

	setTimeout(function(){

		$.ajax({
				data:  parametros,
				url:   base_url()+'index.php/pedidos_presupuestos/obtener_pendientes',
				type:  'post',
				async: true,
				complete: function () {$(".div-cargando").addClass("hidden");},
				success:  function (response) {
					response = $.parseJSON(response);
					if (response.resuelto == "OK") {
						rellenarPendientes(response.pendientes);
					} else {
						if (response.sesion_expirada == true) {
							alert ("Lo sentimos, su sesión ha expirado.");
							redirigirAlLogin();
						}
						else {
							alert("Ha ocurrido un error");
						}
					}
				},
				error: function () {alert("Ha ocurrido un error"); }
		});

	}, 10);

}

function rellenarPendientes(pendientes) {

	var html = "";
	var odd_even;

	// Recorremos los registros para mostrarlos
	for (var i=0; i<pendientes.length; i++) {

		odd_even = i % 2 == 0 ? "odd" : "even";

		if (isNaN(parseInt(pendientes[i].descuentoArticulo))) pendientes[i].descuentoArticulo = 0;

		html += "<tr class='"+odd_even+"'><td><button type='button' class='btn btn-primary incluir-linea-pendiente'>Añadir</td>"+
				"<td class='serie-pedido-pendiente'>"+pendientes[i].seriePedido+"/"+pendientes[i].numeroPedido+"</td>"+
				"<td class='fecha_pedido_pendiente'>"+fechaToString(pendientes[i].fechaPedido)+"</td>"+
				"<td>"+pendientes[i].descripcionArticulo+"</td><td>"+toFloat(pendientes[i].cantidadArticulo, 2)+"</td>"+
				"<td>"+parseInt(pendientes[i].descuentoArticulo)+"%</td>"+
				"<input type='hidden' class='unidades_pendiente' value='"+toFloat(pendientes[i].cantidadArticulo, 2)+"' />"+
				"<input type='hidden' class='codigo_articulo_pendiente' value='"+pendientes[i].codigoArticulo+"' />"+
				"<input type='hidden' class='nombre_articulo_pendiente' value='"+pendientes[i].descripcionArticulo+"' />"+
				"<input type='hidden' class='precio_articulo_pendiente' value='"+pendientes[i].precioArticulo+"' />"+
				"<input type='hidden' class='descuento_articulo_pendiente' value='"+parseInt(pendientes[i].descuentoArticulo)+"' />"+
				"<input type='hidden' class='stock_virtual_articulo_pendiente' value='"+pendientes[i].stockVirtual+"' />"+
				"<input type='hidden' class='stock_disponible_articulo_pendiente' value='"+pendientes[i].stockDisponible+"' />"+
				"<input type='hidden' class='pendiente_servir_articulo_pendiente' value='"+pendientes[i].pendienteServir+"' />"+
				"<input type='hidden' class='pendiente_recibir_articulo_pendiente' value='"+pendientes[i].pendienteRecibir+"' />"+
				"<input type='hidden' class='pendiente_fabricar_articulo_pendiente' value='"+pendientes[i].pendienteFabricar+"' /></tr>";

	}

	$("#total_pendientes").html(pendientes.length);

	$("#tabla-pendientes").find(".listado-registros").html(html);

}

function controlSeleccionLineaPendiente () {

	$(document.body).on('click', ".incluir-linea-pendiente", function(){

		// Limpiamos los filtros
		modal = $("#modal-pendientes");
		limpiarFiltros(modal);

		// Recogemos los datos de la linea
		var linea = {
			"codigoArticulo" : $(this).parent().siblings(".codigo_articulo_pendiente").val(),
			"descripcionArticulo" : $(this).parent().siblings(".nombre_articulo_pendiente").val(),
			"stockVirtualArticulo" : toFloat($(this).parent().siblings(".stock_virtual_articulo_pendiente").val(), 2),
			"stockDisponibleArticulo" : toFloat($(this).parent().siblings(".stock_disponible_articulo_pendiente").val(), 2),
			"pendienteServirArticulo" : toFloat($(this).parent().siblings(".pendiente_servir_articulo_pendiente").val(), 2),
			"pendienteRecibirArticulo" : toFloat($(this).parent().siblings(".pendiente_recibir_articulo_pendiente").val(), 2),
			"pendienteFabricarArticulo" : toFloat($(this).parent().siblings(".pendiente_fabricar_articulo_pendiente").val(), 2),
			"cantidadArticulo" : parseInt($(this).parent().siblings(".unidades_pendiente").val()),
			"precioArticulo" : toFloat($(this).parent().siblings(".precio_articulo_pendiente").val(), 3),
			"descuentoArticulo" : $(this).parent().siblings(".descuento_articulo_pendiente").val()
		};

		// Rellenamos la linea a partir de los datos de la linea
		rellenarLineaInicial(linea);

		//Cerramos el cuadro modal de los pendientes
		$("#modal-pendientes").addClass("hidden");

	});

}

function cargarArticulos (filtros, cargar_mas) {

	// Asignamos un array vacio a filtros si el parametro viene vacio
	filtros = filtros || [];

	// La siguiente variable define si se estan cargando mas articulos, o se esta realizando una busqueda nueva.
	// Lo ponemos a false si viene vacio
	cargar_mas = cargar_mas || false;

	var codigoCliente = $("#codigo_cliente").val();

	// Si no hemos seleccionado cliente
	if (!codigoCliente) alert("Debes seleccionar un cliente correcto antes");

	// Si hemos seleccionado cliente
	else {

		// Mostramos la modal
		$("#modal-articulos").removeClass("hidden");

		// Definimos el limit
		if (cargar_mas == true) {
			var limit = global.articulosCargados;
		}
		else {
			global.articulosCargados = 0;
			var limit = 0;
		}

		var parametros = {
			"codigo_cliente" : codigoCliente,
			"nombre_articulo" : $("#nombre_articulo").val(),
			"codigo_articulo" : $("#codigo_articulo").val(),
			"filtros" : JSON.stringify(filtros),
			"limit" : limit
		};

		// Este es el beforeSend
		$(".div-cargando").removeClass("hidden");
		if (cargar_mas == false) $("#tabla-articulos").find(".listado-registros").html("");
		$("#articulos_cargados").html("-");
		$("#total_articulos").html("-");

		setTimeout(function(){

			$.ajax({
					data:  parametros,
					url:   base_url()+'index.php/pedidos_presupuestos/obtener_articulos',
					type:  'post',
					async: true,
					complete: function () {$(".div-cargando").addClass("hidden");},
					success:  function (response) {
						response = $.parseJSON(response);
						if (response.resuelto == "OK") {
							rellenarArticulos(response.articulos, response.cantidadArticulosTotal);

						} else {
							if (response.sesion_expirada == true) {
								alert ("Lo sentimos, su sesión ha expirado.");
								redirigirAlLogin();
							}
							else {
								alert("Ha ocurrido un error");
							}
						}
					},
					error: function () {alert("Ha ocurrido un error."); }
			});

	    }, 10);

	}

}

function rellenarArticulos(articulos, cantidadArticulosTotal) {

	var html = "";
	var odd_even;
	var botonStock;
	var stockDisponible;
	var pendienteServir;
	var pendienteRecibir;
	var pendienteFabricar;

	global.articulosCargados += articulos.length;

	// Controlamos que el boton mas aparezca, segun haya mas articulos
	if (global.articulosCargados < cantidadArticulosTotal) $("#boton-cargar-mas-articulos").removeClass("hidden");
	else $("#boton-cargar-mas-articulos").addClass("hidden");

	// Recorremos los artículos para mostrarlos
	for (var i=0; i<articulos.length; i++) {

		odd_even = i % 2 == 0 ? "odd" : "even";

		if (articulos[i].descripcion == "" || articulos[i].descripcion == null) articulos[i].descripcion = "-";

		stockVirtual = toFloat(articulos[i].stockVirtual, 2);
		stockDisponible = toFloat(articulos[i].stockDisponible, 2);
		pendienteServir = toFloat(articulos[i].pendienteServir, 2);
		pendienteRecibir = toFloat(articulos[i].pendienteRecibir, 2);
		pendienteFabricar = toFloat(articulos[i].pendienteFabricar, 2);

		botonStock = obtenerBotonStock(stockVirtual, stockDisponible, pendienteServir, pendienteRecibir, pendienteFabricar);

		html += "<tr class='"+odd_even+"'><td><button type='button' class='btn btn-primary seleccionar-articulo'>Seleccionar</button></td>"+
				"<td>"+articulos[i].codigo+"</td><td>"+articulos[i].descripcion+"</td><td>"+toFloat(articulos[i].precio, 3)+"€</td>"+
				"<td><button class='btn "+botonStock+" boton-stock-articulo'>"+toFloat(articulos[i].stockVirtual, 2)+"</button></td>"+
				"<input type='hidden' class='codigo-articulo' value='"+articulos[i].codigo+"' />"+
				"<input type='hidden' class='descripcion-articulo' value='"+articulos[i].descripcion+"' />"+
				"<input type='hidden' class='precio-articulo' value='"+toFloat(articulos[i].precio, 3)+"' />"+
				"<input type='hidden' class='descuento-articulo' value='"+toFloat(articulos[i].descuento, 3)+"' />"+
				"<input type='hidden' class='stock-virtual-articulo' value='"+toFloat(articulos[i].stockVirtual, 2)+"' />"+
				"<input type='hidden' class='stock-disponible-articulo' value='"+toFloat(articulos[i].stockDisponible, 2)+"' />"+
				"<input type='hidden' class='pendiente-servir-articulo' value='"+toFloat(articulos[i].pendienteServir, 2)+"' />"+
				"<input type='hidden' class='pendiente-recibir-articulo' value='"+toFloat(articulos[i].pendienteRecibir, 2)+"' />"+
				"<input type='hidden' class='pendiente-fabricar-articulo' value='"+toFloat(articulos[i].pendienteFabricar, 2)+"' /></tr>";

	}

	// Actualizamos la informacion sobre articulos cargados y total
	$("#articulos_cargados").html(global.articulosCargados);
	$("#total_articulos").html(cantidadArticulosTotal);

	// Añadimos los articulos al listado
	$("#tabla-articulos").find(".listado-registros").append(html);

}

function controlSeleccionArticulo() {

	// Cuando seleccionamos un cliente, lo llevamos al input
	$(document.body).on('click', ".seleccionar-articulo", function(){

		// Limpiamos los filtros
		modal = $("#modal-articulos");
		limpiarFiltros(modal);

		// Recogemos los datos del articulo
		var datos = {
			codigoArticulo : $(this).parent().siblings('.codigo-articulo').val(),
			descripcionArticulo : $(this).parent().siblings('.descripcion-articulo').val(),
			precioArticulo : $(this).parent().siblings('.precio-articulo').val(),
			descuentoArticulo : parseFloat($(this).parent().siblings('.descuento-articulo').val()),
			stockVirtualArticulo : $(this).parent().siblings('.stock-virtual-articulo').val(),
			stockDisponibleArticulo : $(this).parent().siblings('.stock-disponible-articulo').val(),
			pendienteServirArticulo : $(this).parent().siblings('.pendiente-servir-articulo').val(),
			pendienteRecibirArticulo : $(this).parent().siblings('.pendiente-recibir-articulo').val(),
			pendienteFabricarArticulo : $(this).parent().siblings('.pendiente-fabricar-articulo').val(),
			cantidadArticulo : 0
		}

		// Rellenamos la linea inicial con los datos del articulo
		rellenarLineaInicial (datos);

		// Ocultamos la seleccion de productos
		$("#modal-articulos").addClass("hidden");

	});

}

function obtenerBotonStock (stockVirtual, stockDisponible, pendienteServir, pendienteRecibir, pendienteFabricar)  {

	var botonStock;

	// Si hay stock disponible del articulo, y no hay salidas previstas
	if (stockDisponible > 0 && (pendienteRecibir + pendienteFabricar) >= pendienteServir) botonStock = "btn-success";
	// Si hay stock disponible del articulo, pero hay salidas previstas
	else if (stockDisponible > 0 && (pendienteRecibir + pendienteFabricar) < pendienteServir) botonStock = "btn-warning";
	// Si no hay stock disponible del articulo pero hay pendiente de recibir o fabricar
	else if (stockDisponible <= 0 && ((pendienteRecibir + pendienteFabricar) > 0)) botonStock = "btn-warning";
	else botonStock = "btn-danger";

	return botonStock;

}

function focusDescripcionArticulo () {

	// Se pone el focus en la descripcion
	$("#nombre_articulo").focus();

	// Se reescribe la descripcion del articulo para que el cursor quede al final del input
	var nombreArticulo = $("#nombre_articulo").val();
	$("#nombre_articulo").val("");
	$("#nombre_articulo").val(nombreArticulo);

}

function controlCalculosLinea () {

	$(document.body).on("keyup change focus", ".cantidad_articulo, .descuento_articulo", function(event){

		var change = event.type == "change" ? true : false;

		calcularTotalLinea($(this), change);

	});

	$(document.body).on("blur", ".cantidad_articulo, .descuento_articulo", function(){

		if (!($(this).val() > 0)) {
			$(this).val(0);
		}

		calcularTotalPedido();

	});

}

// Elemento es el input que hemos modificado para disparar el cálculo. Change es un booleano, dependiendo si el evento que
// llama a la funcion es change o no
function calcularTotalLinea (elemento, change) {

	var linea = elemento.closest(".lineas-pedido");

	if (linea.find(".cantidad_articulo").val() > 0) {
		var cantidad = linea.find(".cantidad_articulo").val();
	}
	else {
		var cantidad = parseFloat(0);
		if ((elemento.hasClass("cantidad_articulo")) && (change == true)) linea.find(".cantidad_articulo").val(cantidad);
	}

	if (linea.find(".descuento_articulo").val() > 0) {
		var descuento = linea.find(".descuento_articulo").val();
	}
	else {
		var descuento = parseFloat(0);
		if ((elemento.hasClass("descuento_articulo")) && (change == true)) linea.find(".descuento_articulo").val(descuento);
	}

	var cantidad = linea.find(".cantidad_articulo").val() > 0 ? linea.find(".cantidad_articulo").val() : 0;
	var descuento = linea.find(".descuento_articulo").val() > 0 ? linea.find(".descuento_articulo").val() : 0;

	var precio = linea.find(".precio_articulo").val();

	linea.find(".neto_articulo").val(toFloat(precio - precio * descuento / 100, 3));

	var neto = linea.find(".neto_articulo").val();

	linea.find(".total_articulo").val(toFloat(neto * cantidad, 3));

	calcularTotalPedido();

}

function calcularTotalPedido() {

	var total = 0;

	$(".linea-terminada").find(".total_articulo").each(function(){
		total += parseFloat($(this).val());
	});

	$("#total-pedido-html").html(toFloat(total, 2));
	$("#total-pedido").val(toFloat(total, 2));

}

// Funcion que rellena la linea inicial, que desde la que incluimos lineas, segun los datos que reciba
function rellenarLineaInicial (datos) {

	var nuevaLinea = $("#nueva-linea");
	var botonStock;

	// Si es un pedido con cantidad indefinida, la ponemos a 0
	datos.cantidadArticulo = $.trim(datos.cantidadArticulo);
	if (datos.cantidadArticulo == undefined) {
		datos.cantidadArticulo = 0;
	}

	// Definimos el string que une el codigo con la descripcion. Si alguno de los dos es '-' o '.', no lo incluimos
	datos.codigoArticulo = $.trim(datos.codigoArticulo);
	datos.descripcionArticulo = $.trim(datos.descripcionArticulo);
	if (datos.codigoArticulo == "-" || datos.codigoArticulo == ".") {
		datos.codigoDescripcionArticulo = datos.descripcionArticulo;
	}
	else if (datos.descripcionArticulo == "-" || datos.descripcionArticulo == ".") {
		datos.codigoDescripcionArticulo = datos.codigoArticulo;
	}
	else {
		datos.codigoDescripcionArticulo = datos.codigoArticulo+" - "+datos.descripcionArticulo;
	}

	nuevaLinea.find($("#codigo_articulo")).val($.trim(datos.codigoArticulo));
	nuevaLinea.find($("#nombre_articulo")).val($.trim(datos.descripcionArticulo));
	nuevaLinea.find($("#codigo_nombre_articulo")).val(datos.codigoDescripcionArticulo);
	nuevaLinea.find($(".boton_stock_articulo")).children("span").html($.trim(toFloat(datos.stockVirtualArticulo, 2)));
	nuevaLinea.find($(".codigo_articulo_seleccionado")).val($.trim(datos.codigoArticulo));
	nuevaLinea.find($(".stock_virtual_articulo")).val($.trim(toFloat(datos.stockVirtualArticulo, 2)));
	nuevaLinea.find($(".stock_disponible_articulo")).val($.trim(toFloat(datos.stockDisponibleArticulo, 2)));
	nuevaLinea.find($(".pendiente_servir_articulo")).val($.trim(toFloat(datos.pendienteServirArticulo, 2)));
	nuevaLinea.find($(".pendiente_recibir_articulo")).val($.trim(toFloat(datos.pendienteRecibirArticulo, 2)));
	nuevaLinea.find($(".pendiente_fabricar_articulo")).val($.trim(toFloat(datos.pendienteFabricarArticulo, 2)));
	nuevaLinea.find($(".precio_articulo")).val($.trim(toFloat(datos.precioArticulo, 3)));
	nuevaLinea.find($(".descuento_articulo")).val($.trim(datos.descuentoArticulo));
	nuevaLinea.find($(".cantidad_articulo")).val(datos.cantidadArticulo);

	// Deshabilitamos la edicion del precio de la linea inicial
	nuevaLinea.find($(".precio_articulo")).attr("disabled",true);

	botonStock = obtenerBotonStock(datos.stockVirtualArticulo, datos.stockDisponibleArticulo,  datos.pendienteServirArticulo, datos.pendienteRecibirArticulo, datos.pendienteFabricarArticulo);

	// Colocamos la clase del boton de stock
	nuevaLinea.find($(".boton_stock_articulo")).removeClass("btn-success");
	nuevaLinea.find($(".boton_stock_articulo")).removeClass("btn-warning");
	nuevaLinea.find($(".boton_stock_articulo")).removeClass("btn-danger");
	nuevaLinea.find($(".boton_stock_articulo")).addClass(botonStock);

	// Habilitamos el boton del stock
	nuevaLinea.find($(".boton_stock_articulo")).attr("disabled",false);

	// Si es un articulo con codigo '-' o '.', ponemos solo el nombre del articulo en el input
	if (datos.codigoArticulo == "-" || datos.codigoArticulo == ".") {
		nuevaLinea.find($("#codigo_nombre_articulo")).val($.trim(datos.descripcionArticulo));
	}
	else if (datos.descripcionArticulo == "-" || datos.descripcionArticulo == ".") {
		nuevaLinea.find($("#codigo_nombre_articulo")).val($.trim(datos.codigoArticulo));
	}
	else {
		nuevaLinea.find($("#codigo_nombre_articulo")).val($.trim(datos.codigoArticulo+" - "+datos.descripcionArticulo));
	}

	calcularTotalLinea(nuevaLinea.find($(".cantidad_articulo")), false);

}

function controlAgregarBorrarLinea() {

	$("#add-linea").click(function(){

		var linea = {};

		linea.codigo = $("#codigo_articulo").val() != "" ? $("#codigo_articulo").val() : ".";
		linea.nombre = $("#codigo_articulo").val() != "" ? $("#nombre_articulo").val() : $("#nombre_articulo").val();
		linea.codigo_nombre = $("#codigo_nombre_articulo").val() != "" ? $("#codigo_nombre_articulo").val() : "-";
		linea.stock = $("#nueva-linea").find(".stock_virtual_articulo").val() > 0 ? $("#nueva-linea").find(".stock_virtual_articulo").val() : 0;
		linea.cantidad = $("#nueva-linea").find(".cantidad_articulo").val() > 0 ? $("#nueva-linea").find(".cantidad_articulo").val() : 0;
		linea.precio = $("#nueva-linea").find(".precio_articulo").val() > 0 ? $("#nueva-linea").find(".precio_articulo").val() : toFloat(0, 3);
		linea.descuento = $("#nueva-linea").find(".descuento_articulo").val() > 0 ? $("#nueva-linea").find(".descuento_articulo").val() : 0;
		linea.neto = $("#nueva-linea").find(".neto_articulo").val() > 0 ? $("#nueva-linea").find(".neto_articulo").val() : toFloat(0, 3);
		linea.total = $("#nueva-linea").find(".total_articulo").val() > 0 ? $("#nueva-linea").find(".total_articulo").val() : toFloat(0, 3);
		linea.prioridad = "0";

		// El articulo ha sido elegido del listado de articulos
		if ($("#codigo_articulo").val() != "") linea.nombre = $("#nombre_articulo").val();
		// El articulo ha sido introducido a mano
		else linea.nombre = $("#nombre_articulo").val();

		agregarLinea(linea);

		limpiarLineaInicial();

	});

	$(document.body).on('click', ".borrar-linea", function(){

		// Borramos la línea
		$(this).closest(".linea-terminada").remove();

		// Si es la única línea, ocultamos la cabecera de las líneas de abajo
		if (!($(".linea-terminada").length > 0)) {
			$("#titulos-columnas-lineas-abajo").addClass("hidden");
		}

		calcularTotalPedido();

	});

	$(document.body).on('click', ".prioridad-linea", function(){
		switch ($(this).val()) {
			case "0":
				$(this).removeClass("btn-success");
				$(this).addClass("btn-warning");
				$(this).html(1);
				$(this).val(1);
				break;
			case "1":
				$(this).removeClass("btn-warning");
				$(this).addClass("btn-danger");
				$(this).html(2);
				$(this).val(2);
				break;
			case "2":
				$(this).removeClass("btn-danger");
				$(this).addClass("btn-success");
				$(this).html(0);
				$(this).val(0);
				break;
		}
	});
}

function limpiarLineaInicial () {

	// Borramos los datos del articulo seleccionado
	$("#codigo_nombre_articulo").val("");
	$("#codigo_articulo").val("");
	$("#nombre_articulo").val("");

	// Borramos los datos de la linea
	$("#nueva-linea").find(".cantidad_articulo").val("");
	$("#nueva-linea").find(".precio_articulo").val("");
	$("#nueva-linea").find(".descuento_articulo").val("");
	$("#nueva-linea").find(".total_articulo").val("");
	$("#nueva-linea").find(".neto_articulo").val("");

	// Borramos los datos del stock
	$("#nueva-linea").find(".codigo_articulo_seleccionado").val("");
	$("#nueva-linea").find(".stock_virtual_articulo").val("");
	$("#nueva-linea").find(".stock_disponible_articulo").val("");
	$("#nueva-linea").find(".pendiente_servir_articulo").val("");
	$("#nueva-linea").find(".pendiente_recibir_articulo").val("");
	$("#nueva-linea").find(".pendiente_fabricar_articulo").val("");

	// Limpiamos el boton del stock
	$("#nueva-linea").find(".boton_stock_articulo").children("span").html("");
	$("#nueva-linea").find(".boton_stock_articulo").attr("disabled", true);
	$("#nueva-linea").find(".boton_stock_articulo").removeClass("btn-danger");
	$("#nueva-linea").find(".boton_stock_articulo").removeClass("btn-warning");
	$("#nueva-linea").find(".boton_stock_articulo").removeClass("btn-success");

	// Habilitamos la edicion del precio de la linea inicial
	$("#nueva-linea").find($(".precio_articulo")).attr("disabled",false);

}

function agregarLinea(linea) {

	var descuento_disabled;

	// Quitamos el hidden a la cabecera de las líneas de abajo
	$("#titulos-columnas-lineas-abajo").removeClass("hidden");

	switch (linea.prioridad) {
		case "0": var botonPrioridad = "btn-success"; break;
		case "1": var botonPrioridad = "btn-warning"; break;
		case "2": var botonPrioridad = "btn-danger"; break;
	}

	// Comprobamos si es un comisionista o un cliente
	var tipo_usuario = $("#tipo_usuario").val();

	// Si es comisionista
	if (tipo_usuario == "comisionista") {

		descuento_disabled = "";

	}
	// Si es cliente
	else if (tipo_usuario == "cliente") {

		descuento_disabled = "disabled";

	}

	var html = "<div class='row cols-menos-padding lineas-pedido linea-terminada'>"+

					"<div class='form-group columna columna-6 padding-left'>"+
				    	"<button type='button' class='btn "+botonPrioridad+" prioridad-linea' value='"+linea.prioridad+"'>"+linea.prioridad+"</button>"+
				  	"</div>"+

					"<div class='form-group columna columna-66'>"+
						  "<input type='text' class='form-control codigo_nombre_articulo' value='"+linea.codigo_nombre+"' disabled>"+
				  	"</div>"+

				  	"<div class='form-group columna columna-6'>"+
				    	"<input type='text' class='form-control cantidad_articulo' value='"+linea.cantidad+"'>"+
				  	"</div>"+

				  	"<div class='form-group columna columna-10'>"+
				    	"<div class='input-group'>"+
						  "<input type='text' class='form-control precio_articulo' value='"+linea.precio+"' disabled>"+
						  "<span class='input-group-addon simbolo-input'>€</span>"+
						"</div>"+
				  	"</div>"+

				  	"<div class='form-group columna columna-6'>"+
						"<div class='input-group'>"+
						  "<input type='text' class='form-control descuento_articulo' value='"+linea.descuento+"' "+descuento_disabled+">"+
						  "<span class='input-group-addon simbolo-input'>%</span>"+
						"</div>"+
					"</div>"+

				  	"<input type='hidden' class='codigo_articulo' value='"+linea.codigo+"'>"+
				  	"<input type='hidden' class='nombre_articulo' value='"+linea.nombre+"'>"+
				  	"<input type='hidden' class='total_articulo' value='"+linea.total+"'>"+
				  	"<input type='hidden' class='neto_articulo' value='"+linea.neto+"'>"+
				  	"<input type='hidden' class='stock_articulo' value='"+linea.stock+"'>"+

				  	"<div class='form-group columna columna-6 padding-right'>"+
				    	"<button type='button' class='btn btn-danger borrar-linea'><i class='fa fa-trash'></i></button>"+
				  	"</div>"+
				"</div>";

	$("#ultima-fila").before(html);

	calcularTotalPedido();

}

function controlEnviarFormulario () {

	$("#enviar-pedido-presupuesto").on("click", function(){

		if (comprobarRequeridos() == true) {
			$("#modal-confirmar-envio").removeClass("hidden");
		}
		else {
			$("#modal-campos-obligatorios").removeClass("hidden");
		}

	});

	$("#confirmar-envio").on("click", function(){

		if (comprobarRequeridos() == true) {

			datos = recogerDatosFormulario();
			datos.lineas = JSON.stringify(datos.lineas);

			// Si se ha desmarcado la opcion de enviar emails al cliente, borramos los emails de los datos
			if (!($("#checkbox-email").is(":checked"))) datos.emails_envio = "";

			// Este es el beforeSend para que funcione correctamente en iPad
			$("#modal-confirmar-envio").addClass("hidden");
			$(".div-cargando").removeClass("hidden");
			$( "#enviar-pedido-presupuesto" ).prop( "disabled", true );

			setTimeout(function(){
				$.ajax({
		            data:  datos,
		            url:   base_url()+'index.php/pedidos_presupuestos/enviar_pedido_presupuesto',
		            type:  'post',
		            async: true,
					complete: function () {
						$(".div-cargando").addClass("hidden");
						$("#enviar-pedido-presupuesto").prop( "disabled", false );
					},
		            success:  function (response) {

		            	response = $.parseJSON(response);

						// Si se recibe OK
		            	if (response.resuelto == "OK") {

		            		// Comprobamos que se puede trabajar con localStorage
							if('localStorage' in window && window['localStorage'] !== null) {

								// Comprobamos si es un comisionista o un cliente
								var tipo_usuario = $("#tipo_usuario").val();

								if (tipo_usuario == "comisionista") var codigo_usuario = $("#comisionista").val();
								else if (tipo_usuario == "cliente") var codigo_usuario = $("#codigo_cliente").val();

								// Borramos el autoguardado si existe, para que no se vuelva a reenviar el pedido
								if (localStorage.getItem("autoguardado_"+tipo_usuario+"_"+codigo_usuario) != null) {

									localStorage.removeItem("autoguardado_"+tipo_usuario+"_"+codigo_usuario);

								}

								// Borramos el borrador si existe y estamos en uno
								if (global.numeroBorrador !== false) {

									var borradores = localStorage.getItem('borradores_'+tipo_usuario) != null ? $.parseJSON(localStorage.getItem('borradores_'+tipo_usuario)) : [];
									borradores.splice(global.numeroBorrador, 1);
									localStorage.setItem('borradores_'+tipo_usuario, JSON.stringify(borradores));

								}

							}
							alert ("Éxito! Se ha enviado el pedido/presupuesto correctamente.");
						}
						// Si no se recibe OK
		            	else {

							// Si la sesion esta expirada, lo indicamos y redirigimos al login
							if (response.sesion_expirada == true) {

								alert ("Lo sentimos, su sesión ha expirado.");
								redirigirAlLogin();

							}
							// Si no, mostramos es por alert que ha habido un error
							else {
								alert (response.resuelto);
							}
		            	}
						window.location.replace(base_url()+"index.php/pedidos_presupuestos");
		            }
				});
		    }, 10);

		}
		else {
			$("#modal-campos-obligatorios").removeClass("hidden");
		}

	});

}

function controlInformacionPlegable() {

	// Hover de la cabecera
	$(".cabecera-informacion-plegable").on("mouseenter", function(){
		$(this).addClass("cabecera-informacion-plegable-hover");
		$(this).parent().addClass("informacion-plegable-hover");
	});
	$(".cabecera-informacion-plegable").on("mouseleave", function(){
		$(this).removeClass("cabecera-informacion-plegable-hover");
		$(this).parent().removeClass("informacion-plegable-hover");
	});

	// Plegado y desplegado al clickar en la cabecera
	$(".cabecera-informacion-plegable").on("click", function(){
		if ($(".cuerpo-informacion-plegable").hasClass("informacion-plegada")) {
			$(".cuerpo-informacion-plegable").removeClass("informacion-plegada");
			$(".icono-plegable").addClass("fa-angle-up");
			$(".icono-plegable").removeClass("fa-angle-down");
		}
		else {
			$(".cuerpo-informacion-plegable").addClass("informacion-plegada");
			$(".icono-plegable").addClass("fa-angle-down");
			$(".icono-plegable").removeClass("fa-angle-up");
		}
	});

	// Plegar al poner el foco en el buscador de artículos
	$("#nombre_articulo, #codigo_articulo, #buscar-articulos").on("focus click", function(){
		$(".cuerpo-informacion-plegable").addClass("informacion-plegada");
		$(".icono-plegable").addClass("fa-angle-down");
		$(".icono-plegable").removeClass("fa-angle-up");
	});

}

function controlContadorCaracteresObservaciones(){

	$("#caracteres-observaciones").html($("#observaciones").val().length);

	$("#observaciones").on("keyup change", function(){
		$("#caracteres-observaciones").html($("#observaciones").val().length);
	});

}

function controlBorradoInputFocus() {

	// Cuando se hace foco en determinados inputs, se borra el texto
	$(document.body).on('focus', ".cantidad_articulo, .descuento_articulo", function(){
		$(this).val("");
		calcularTotalLinea($(this), false);
	});

	// Cuando toquemos una pulsemos dentro del codigo_nombre del cliente
	$(document.body).on('keyup', "#codigo_nombre_cliente", function(){

		var tipo_usuario = $("#tipo_usuario").val();

		// Si el tipo_usuario es cliente, no borramos nada
		if (tipo_usuario != "cliente") {
			$("#codigo_cliente").val("");
			$("#nombre_cliente").val("");
		}
	});

	// Cuando pulsemos una tecla dentro del nombre del articulo
	$(document.body).on('keyup', "#nombre_articulo", function(){

		// Definimos el codigo en blanco
		$("#codigo_articulo").val("");

		// Definimos el codigo_nombre como el nombre unicamente
		var nombre_articulo = $("#nombre_articulo").val();
		$("#codigo_nombre_articulo").val(nombre_articulo);

		// Habilitamos la edicion del precio de la linea inicial
		$("#nueva-linea").find($(".precio_articulo")).attr("disabled",false);

	});

}

function controlBotonesBorrarInput() {

	// Limpiador del input del cliente
	$("#limpiar-input-cliente").click(function (){
		// Borramos los datos del cliente seleccionado
		$(this).siblings("input").val("");
		$("#codigo_nombre_cliente").val("");
		$("#codigo_cliente").val("");
		$("#nombre_cliente").val("");
		$("#emails_envio").val("");

		// Deshabilitamos los botones de consulta de pendientes e historico
		$("#boton-historico").attr("disabled","disabled");
		$("#boton-pendientes").attr("disabled","disabled");
		global.empresaSeleccionada = false;
	});

	// Limpiador del input del articulo
	$("#limpiar-input-articulo").click(limpiarLineaInicial);

}

function comprobarRequeridos() {

	var respuesta = true;

	if ($("#numero").val() == "" || $("#fecha").val() == "" || $("#codigo_cliente").val() == "" || $("#nombre_cliente").val() == "") {
		respuesta = false;
	}

	return respuesta;

}

function recogerDatosFormulario() {

	var datosFormulario = {};

	datosFormulario.tipo_usuario = $("#tipo_usuario").val();
	datosFormulario.comisionista = $("#comisionista").val();
	datosFormulario.numero = $("#numero").val();
	datosFormulario.fecha = $("#fecha").val();
	datosFormulario.tipo = $("#tipo").val();
	datosFormulario.codigo_cliente = $("#codigo_cliente").val();
	datosFormulario.nombre_cliente = $("#nombre_cliente").val();
	datosFormulario.tarifa_cliente = $("#tarifa_cliente").val();
	datosFormulario.cliente = $("#codigo_nombre_cliente").val();
	datosFormulario.empresaSeleccionada = global.empresaSeleccionada;
	datosFormulario.subPedido = $("#subpedido_web").val();
	datosFormulario.emails_envio = $("#emails_envio").val();
	datosFormulario.observaciones = $("#observaciones").val();
	datosFormulario.total = $("#total-pedido").val();
	datosFormulario.lineas = [];

	$(".linea-terminada").each(function(){

		var linea = {};

		linea.prioridad = $(this).find($(".prioridad-linea")).val();
		linea.codigo = $(this).find($(".codigo_articulo")).val();
		linea.nombre = $(this).find($(".nombre_articulo")).val();
		linea.codigo_nombre = $(this).find($(".codigo_nombre_articulo")).val();
		linea.cantidad = $(this).find($(".cantidad_articulo")).val();
		linea.stock = $(this).find($(".stock_articulo")).val();
		linea.precio = $(this).find($(".precio_articulo")).val();
		linea.descuento = $(this).find($(".descuento_articulo")).val();
		linea.neto = $(this).find($(".neto_articulo")).val();
		linea.total = $(this).find($(".total_articulo")).val();

		datosFormulario.lineas.push(linea);

	});

	return datosFormulario;

}

function controlBotonBorrarFormulario() {

	$("#limpiar-formulario").click(function(){

		var tipo_usuario = $("#tipo_usuario").val();

		// Si el tipo_usuario es de cliente, no borramos los datos de la empresa
		if (tipo_usuario != "cliente") {

			global.empresaSeleccionada = false;
			$("#codigo_nombre_cliente").val("");
			$("#codigo_cliente").val("");
			$("#nombre_cliente").val("");
			$("#tarifa_cliente").val("");
			$("#boton-historico").attr("disabled","disabled");
			$("#boton-pendientes").attr("disabled","disabled");

		}

		$("#subpedido_web").val("");
		$("#emails_envio").val("");
		$("#observaciones").val("");

		limpiarLineasFormulario();

	});

}

function limpiarLineasFormulario () {

	limpiarLineaInicial();

	// Limpiamos las lineas terminadas
	$(".linea-terminada").remove();
	$("#titulos-columnas-lineas-abajo").addClass("hidden");
	$("#total-pedido").val(0);
	$("#total-pedido-html").html("0");

}

function controlCambioTipoFormulario() {

	// Cuando se cambia el select de tipo
	$("#tipo").on("change", function(){

		// Cuando se cambia a pedido, el numero sera el numero de pedido
		if ($(this).val() == "Pedido") $("#numero").val(global.numeroPedido);

		// Cuando se cambia a presupuesto, el numero sera p
		else if ($(this).val() == "Presupuesto") $("#numero").val("p");


	});

}

function rellenarModalStock (datosStock) {

	// Rellenamos los datos de stock
	$("#modal-stock-codigo-articulo").html(datosStock.codigoArticulo);
	$("#modal-stock-virtual").html(datosStock.stockVirtual);
	$("#modal-stock-disponible").html(datosStock.stockDisponible);
	$("#modal-stock-servir").html(datosStock.pendienteServir);
	$("#modal-stock-recibir").html(datosStock.pendienteRecibir);
	$("#modal-stock-fabricar").html(datosStock.pendienteFabricar);

	// Asignamos el color a la bolita del estado
	$("#modal-stock-bolita-estado").removeClass();
	$("#modal-stock-bolita-estado").addClass(datosStock.bolitaEstado);

	// Asignamos el texto del estado
	$("#modal-stock-texto-estado").html(datosStock.textoEstado);

}

function fechaToString (fechaSQL) {

	var fechaString;

	if (fechaSQL == null || fechaSQL == undefined) {
		fechaString = "-";
	}
	else {
		fechaSQL = fechaSQL.split(" ");
		fechaSQL = fechaSQL[0].split("-");
		fechaString = fechaSQL[2]+"/"+fechaSQL[1]+"/"+fechaSQL[0];
	}

	return fechaString;

}

function toFloat(numero, redondeo) {

	return parseFloat(numero).toFixed(redondeo);

}

function redirigirAlLogin () {

	window.location.replace(base_url()+"index.php/login");

}

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();
