﻿$(document).ready(main);

function main () {

	controlLogin();

	controlRegistroKeys();

}

function obtenerKeysLocales () {

	var keys = [];

	// LocalStorage activado
	if('localStorage' in window && window['localStorage'] !== null) {
		if (localStorage.getItem("keys") != null) {
			keys = $.parseJSON(localStorage.getItem("keys"));
		}
	}
	// LocalStorage desactivado
	else {
		alert("Para poder iniciar sesión, debes tener un navegador que permita el uso de localStorage y tenerlo activo.");
	}

	return keys;

}

function mensajeError (mensaje) {
	var campoMensaje = $("#mensaje");
	campoMensaje.html(mensaje);
	campoMensaje.css('opacity','1');
	campoMensaje.delay(1500).fadeTo("slow",0);
}

function controlLogin () {

	var usuario;
	var clave;
	var keys;
	var keyUsuario;
	var i;
	var parametros = {
  	"usuario" : "",
  	"clave" : ""
  };

	$("#login").submit(function(e) {

		e.preventDefault();

		usuario = $("#form-usuario").val();
		clave = $("#form-clave").val();

		if (usuario.length >= 3) {

			if (clave != "" && clave != null){

				// Obtenemos las keys locales
				keys = obtenerKeysLocales();

				// Comprobamos si existe key para ese usuario, poniendo la key como vacia por defecto
				keyUsuario = "";
				if (keys.length > 0) {

					// Nos recorremos las keys
					for (i=0; i<keys.length; i++) {

						if (keys[i].usuario == usuario) keyUsuario = keys[i].key;

					}

				}

				parametros = {
		    	"usuario" : usuario,
		    	"clave" : $.md5(clave),
					"key" : keyUsuario
		    };

				// Esto junto al setTimeout simulan el beforeSend
				$("#boton-acceso").attr("disabled", true);

				setTimeout(function(){

					$.ajax({
							data:  parametros,
							url:   base_url()+'index.php/login/validar_login',
							type:  'post',
							complete: function () {
								$("#boton-acceso").attr("disabled", false);
							},
							success:  function (respuesta) {

								respuesta = $.parseJSON(respuesta);

								// Login correcto
								if (respuesta.resuelto == "OK") {

									// Si la key es correcta, redirigimos al inicio
									if (respuesta.key == 1) window.location.replace(base_url());

									// Si la key es incorrecta
									else {

										// Asignamos los datos de la modal de la key
										$("#usuario-key").val(parametros.usuario);
										$("#clave-key").val(clave);

										// Mostramos la modal de la key, limpiando antes la key y haciendo focus en el input
										$("#key").val("");
										$("#introducir-key-login").removeClass("hidden");
										$("#key").focus();

									}

								}
								// Login incorrecto
								else {

									$("#form-usuario").focus();
									mensajeError("Login incorrecto");
								}
							},
							error: function () {
								alert("Ha ocurrido un error");
							}
					});

				}, 10);

			} else {
				mensajeError("Introduzca contraseña");
				$("#form-clave").focus();
			}

		} else {
			mensajeError("Introduzca usuario correcto");
			$("#form-usuario").focus();
		}

	});

}

function controlRegistroKeys () {

	var usuario;
	var clave;
	var key;
	var parametros;

	$("#registro-key").submit(function(e) {

		e.preventDefault();

		key = $("#key").val();
		usuario = $("#usuario-key").val();
		clave = $("#clave-key").val();

		if (key != "" && key != undefined && usuario != "" && usuario != undefined && clave != "" && clave != undefined) {

			parametros = {
	    	"usuario" : usuario,
	    	"clave" : $.md5(clave),
				"key" : key
	    };

			// Esto junto al setTimeout simulan el beforeSend
			$("#boton-registrar-key").attr("disabled", true);

			setTimeout(function(){

				$.ajax({
						data:  parametros,
						url:   base_url()+'index.php/login/registrar_key',
						type:  'post',
						complete: function () {
							$("#boton-registrar-key").attr("disabled", false);
						},
						success:  function (respuesta) {

							respuesta = $.parseJSON(respuesta);

							// Key correcta
							if (respuesta.resuelto == "OK") {

								// Registramos la key
								registrarKey(usuario, key);

								// Forzamos el inicio de sesion
								$("#form-usuario").val(usuario);
								$("#form-clave").val(clave);
								$("#login").submit();

							}
							// Error
							else {
								alert(respuesta.mensaje);
							}
						},
						error: function () {
							alert("Ha ocurrido un error");
						}
				});

			}, 10);

    }
		else {
  		mensajeError("Ha ocurrido un error");
  	}

	});

}

function registrarKey (usuario, key) {

	var keys = obtenerKeysLocales();
	var registradaAntes = false;
	var i;

	for (i=0; i<keys.length; i++) {

		// Si el usuario ya tenia una key registrada en el navegador, la reemplazamos por la nueva
		if (keys[i].usuario == usuario) {

			keys[i].key = key;
			registradaAntes = true;

		}

	}

	// Si no estaba registrada, la incluimos
	if (!registradaAntes) {

		keys.push({"usuario":usuario, "key":key});

	}

	// Guardamos los nuevos datos en localStorage
	localStorage.setItem('keys', JSON.stringify(keys));

}
