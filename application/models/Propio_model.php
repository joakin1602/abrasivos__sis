<?php
	class Propio_model extends CI_Model {

		function comprobarUsuarioComisionistaCorrecto($usuario, $clave){

			$datos = new stdClass();
			$datos->correcto = false;

			$empresa = $usuario[0];
			$comisionista = substr($usuario, 1);

			// Comprobamos que no se haya introducido pr, ya que daria error al ser string
			if ($comisionista != "pr") {

				$this->db->select("CodigoEmpresa AS empresa,CodigoComisionista AS codigo, Comisionista AS nombre, EMail1 AS email, contrasena AS clave");
				$this->db->where("CodigoEmpresa", intVal($empresa));
				$this->db->where("CodigoComisionista", intVal($comisionista));
				$query = $this->db->get("Comisionistas_cic");
				$row = $query->row();

				if ($query->num_rows() > 0) {

					if (md5(strtolower($row->clave)) == $clave) {

						$datos->correcto = true;
						$datos->empresa = $row->empresa;
						$datos->codigo = $row->codigo;
						$datos->nombre = $row->nombre;
						$datos->email = isset($row->email) ? trim($row->email) : "";

					}
				}
			}

			return $datos;

		}

		function comprobarUsuarioClienteCorrecto($usuario, $clave){

			$datos = new stdClass();
			$datos->correcto = false;

			$empresa = $usuario[0];
			$cliente = substr($usuario, 1);

			$this->db->select("CodigoEmpresa AS empresa,CodigoCliente AS codigo, RazonSocial AS nombre, EMail1 AS email, contrasena AS clave");
			$this->db->where("CodigoEmpresa", intVal($empresa));
			$this->db->where("CodigoCliente", $cliente);
			$this->db->where("contrasena !=", 'null');
			$query = $this->db->get("Clientes_cic");
			$row = $query->row();

			if ($query->num_rows() > 0) {

				if (md5(strtolower($row->clave)) == $clave) {

					$datos->correcto = true;
					$datos->empresa = $row->empresa;
					$datos->codigo = $row->codigo;
					$datos->nombre = $row->nombre;
					$datos->email = isset($row->email) ? trim($row->email) : "";

				}

			}

			return $datos;

		}

		function crearLogInicioSesion ($tipo_usuario) {

			if ($tipo_usuario == "admin") $id_usuario = 0;
			elseif ($tipo_usuario == "comisionista") $id_usuario = $this->session->comisionista;
			elseif ($tipo_usuario == "cliente") $id_usuario = $this->session->cliente;

			$datos_log = Array(
				"id_usuario" => $id_usuario,
				"tipo_usuario" => $tipo_usuario,
				"ip" => $_SERVER["REMOTE_ADDR"],
				"fecha" => date("Y-m-d")."T".date("H:i:s")
			);

			$this->db->insert("LogWeb_cic", $datos_log);

		}

		function obtenerClientesIniciales ($comisionista=false) {

			$this->db->select('codigoCliente AS codigo, RazonSocial AS nombre, Telefono AS telefono, Municipio AS municipio, TarifaPrecio AS tarifa');
			if ($comisionista) $this->db->where('CodigoComisionista',$comisionista);
			$query = $this->db->get('Clientes_cic', 25);
			$clientes = $query->result();

			return $clientes;

		}

		function obtenerCantidadClientesTotales($comisionista=false) {

			$this->db->select('COUNT(*) AS cantidad');
			if ($comisionista) $this->db->where('CodigoComisionista',$comisionista);
			$query = $this->db->get('Clientes_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerOtrosClientes($comisionista, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion){

			$this->db->select('codigoCliente AS codigo, RazonSocial AS nombre, Telefono AS telefono, Municipio AS municipio, TarifaPrecio AS tarifa');
			if ($comisionista) $this->db->where('CodigoComisionista',$comisionista);

			foreach ($filtros as $filtro) {

				$this->db->like($filtro->nombre, $filtro->valor);

			}

			if ($columnaOrdenacion != "") {
				$this->db->order_by($columnaOrdenacion, $tipoOrdenacion);
			}
			else {
				$this->db->order_by('CodigoCliente', 'ASC');
			}

			$query = $this->db->get('Clientes_cic', $agrupacion, $inicioLimit);
			$clientes = $query->result();

			return $clientes;

		}

		function obtenerCantidadOtrosClientes($comisionista, $filtros) {

			$this->db->select('COUNT(*) AS cantidad');
			if ($comisionista) $this->db->where('CodigoComisionista',$comisionista);

			foreach ($filtros as $filtro) {

				$this->db->like($filtro->nombre, $filtro->valor);

			}

			$query = $this->db->get('Clientes_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		// Esta funcion obtendra el email del comisionista asignado al cliente, en el caso de que el comisionista este asignado para recibir los emails de sus clientes
		function obtenerEmailReenvioClienteComisionista($cliente) {

			$emailComisionista = false;

			$this->db->select('COM.EMail1 AS email');
			$this->db->from('Comisionistas_cic AS COM');
			$this->db->join('Clientes_cic AS CLI', 'COM.CodigoComisionista = CLI.CodigoComisionista');
			$this->db->join('ComisionistasEmailCliente_cic AS CEC', 'COM.CodigoComisionista = CEC.CodigoComisionista');
			$this->db->where('CLI.CodigoCliente', $cliente);
			$query = $this->db->get();

			if ($resultado = $query->row()) $emailComisionista = $resultado->email;

			return $emailComisionista;

		}

		function comprobarClienteComisionista($cliente, $comisionista) {

			$this->db->select('COUNT(*) AS cantidad');
			$this->db->where('CodigoCliente',$cliente);
			$this->db->where('CodigoComisionista',$comisionista);
			$query = $this->db->get('Clientes_cic');
			$resultado = $query->row();

			$clienteComisionista = $resultado->cantidad > 0 ? true : false;

			return $clienteComisionista;

		}

		function obtenerDatosCliente ($cliente, $comisionista=false) {

			$datosCliente = false;

			$this->db->select('codigoCliente AS codigo, RazonSocial AS nombre, Domicilio AS domicilio, CodigoPostal AS cp, Municipio AS municipio, Provincia AS provincia, Telefono AS telefono,
							   EMail1 AS email, BloqueoPedido, TarifaPrecio AS tarifa, TipoCliente AS cliente_anterior');
			if ($comisionista) $this->db->where('CodigoComisionista',$comisionista);
			$this->db->where('codigoCliente',$cliente);
			$query = $this->db->get('Clientes_cic');

			if ($query->num_rows()) $datosCliente = $query->row();

			return $datosCliente;

		}

		function obtenerHistoricoClienteInicial ($cliente) {

			$this->db->select('ac.CodigoArticulo AS codigo, a.DescripcionArticulo AS descripcion, ac.FechaPrimerAlbaran as fechaPrimer, ac.FechaUltimoAlbaran AS fechaUltimo,
							   ac.UnidadesUltimoAlbaran as unidadesUltimo');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'a.CodigoArticulo = ac.CodigoArticulo');

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(ac.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" ac.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('ac.codigoCliente', $cliente->codigo);

			}

			$this->db->where('a.CodigoArticulo != ', '.');
			$this->db->order_by('ac.FechaUltimoAlbaran', 'desc');
			$this->db->limit(25);
			$query = $this->db->get();
			$datosHistorico = $query->result();

			return $datosHistorico;

		}

		function obtenerCantidadHistoricoCliente($cliente) {

			$this->db->select('COUNT(*) AS cantidad');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'a.CodigoArticulo = ac.CodigoArticulo');

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(ac.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" ac.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('ac.codigoCliente', $cliente->codigo);

			}

			$this->db->where('a.CodigoArticulo != ', '.');

			$query = $this->db->get();
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerOtrosHistoricoCliente($cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion){

			$this->db->select('ac.CodigoArticulo AS codigo, a.DescripcionArticulo AS descripcion, ac.FechaPrimerAlbaran as fechaPrimer, ac.FechaUltimoAlbaran AS fechaUltimo,
							   ac.UnidadesUltimoAlbaran as unidadesUltimo');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'a.CodigoArticulo = ac.CodigoArticulo');

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(ac.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" ac.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('ac.codigoCliente', $cliente->codigo);

			}

			$this->db->where('a.CodigoArticulo != ', '.');
			$this->db->limit($agrupacion, $inicioLimit);

			foreach ($filtros as $filtro) {

				if ($filtro->nombre == "FechaPrimerAlbaran" || $filtro->nombre == "FechaUltimoAlbaran") {
					$this->db->where($filtro->nombre, $filtro->valor);
				}
				else {
					$this->db->like($filtro->nombre, $filtro->valor);
				}

			}

			if ($columnaOrdenacion != "") {
				$this->db->order_by($columnaOrdenacion, $tipoOrdenacion);
			}

			// Este if es para evitar que se ordene dos veces por FechaUltimoAlbaran, si se clicka en dicha columna
			if ($columnaOrdenacion != "FechaUltimoAlbaran") {
				$this->db->order_by('ac.FechaUltimoAlbaran', 'desc');
			}

			$query = $this->db->get();
			$datosHistorico = $query->result();

			return $datosHistorico;

		}

		function obtenerCantidadOtrosHistoricoCliente($cliente, $filtros) {

			$this->db->select('COUNT(*) AS cantidad');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'a.CodigoArticulo = ac.CodigoArticulo');

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(ac.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" ac.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('ac.codigoCliente', $cliente->codigo);

			}

			$this->db->where('a.CodigoArticulo != ', '.');

			foreach ($filtros as $filtro) {

				if ($filtro->nombre == "FechaPrimerAlbaran" || $filtro->nombre == "FechaUltimoAlbaran") {
					$this->db->where($filtro->nombre, $filtro->valor);
				}
				else {
					$this->db->like($filtro->nombre, $filtro->valor);
				}

			}

			$query = $this->db->get();
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerPendientesEntregaClienteInicial ($cliente) {

			$this->db->select("{fn CONCAT({fn CONCAT(CAST(SeriePedido AS VARCHAR(10)),'/')},CAST(NumeroPedido AS VARCHAR(10)))} AS pedido, FechaPedido AS fecha, CodigoArticulo AS codigoArticulo, DescripcionArticulo AS descripcionArticulo,
							   UnidadesPedidas as unidadesPedidas, UnidadesPendientes AS unidadesPendientes, EstadoFabricacion AS estado");

			// Hay que hayar tambien los del cliente anterior
 			if ($cliente->cliente_anterior != '') {

 				$this->db->where("(codigoCliente = '".$cliente->codigo."'");
 				$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

 			}
 			// No tiene cliente anterior
 			else {

 				$this->db->where('codigoCliente', $cliente->codigo);

 			}

			$this->db->where_in('EstadoFabricacion', array('P','T','E','I'));
			$this->db->where('NumeroPedido !=', 0);
			$this->db->order_by ("SeriePedido, NumeroPedido", "asc");
			$query = $this->db->get('LineasPedidoCliente_cic'/*, 25*/);
			$pendientesEntrega = $query->result();

			return $pendientesEntrega;

		}

		function obtenerCantidadPendientesEntregaCliente($cliente) {

			$this->db->select('COUNT(*) AS cantidad');

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('codigoCliente', $cliente->codigo);

			}

			$this->db->where_in('EstadoFabricacion', array('P','T','E','I'));
			$this->db->where('NumeroPedido !=', 0);
			$query = $this->db->get('LineasPedidoCliente_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerOtrosPendientesEntregaCliente($cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion){

			$this->db->select("{fn CONCAT({fn CONCAT(CAST(SeriePedido AS VARCHAR(10)),'/')},CAST(NumeroPedido AS VARCHAR(10)))} AS pedido, FechaPedido AS fecha, CodigoArticulo AS codigoArticulo, DescripcionArticulo AS descripcionArticulo,
							   UnidadesPedidas as unidadesPedidas, UnidadesPendientes AS unidadesPendientes, EstadoFabricacion AS estado");

		 	// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('codigoCliente', $cliente->codigo);

			}

			$this->db->where_in('EstadoFabricacion', array('P','T','E','I'));
			$this->db->where('NumeroPedido !=', 0);

			foreach ($filtros as $filtro) {

				if ($filtro->nombre == "FechaPedido") {
					$this->db->where($filtro->nombre, $filtro->valor);
				}
				elseif ($filtro->nombre == "pedido") {
					$this->db->like("{fn CONCAT({fn CONCAT(CAST(SeriePedido AS VARCHAR(10)),'/')},CAST(NumeroPedido AS VARCHAR(10)))}", $filtro->valor);
				}
				else {
					$this->db->like($filtro->nombre, $filtro->valor);
				}

			}

			if ($columnaOrdenacion != "") {
				if ($columnaOrdenacion == "pedido") {
					$this->db->order_by("SeriePedido, NumeroPedido", $tipoOrdenacion);
				}
				else {
					$this->db->order_by($columnaOrdenacion, $tipoOrdenacion);
				}
			}

			else {
				$this->db->order_by ("SeriePedido, NumeroPedido", "asc");
			}

			/*$this->db->limit($agrupacion, $inicioLimit);*/
			$query = $this->db->get('LineasPedidoCliente_cic');
			$datosHistorico = $query->result();

			return $datosHistorico;

		}

		function obtenerCantidadOtrosPendientesEntregaCliente($cliente, $filtros) {

			$this->db->select('COUNT(*) AS cantidad');

			// Hay que hayar tambien los del cliente anterior
 			if ($cliente->cliente_anterior != '') {

 				$this->db->where("(codigoCliente = '".$cliente->codigo."'");
 				$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

 			}
 			// No tiene cliente anterior
 			else {

 				$this->db->where('codigoCliente', $cliente->codigo);

 			}

			$this->db->where_in('EstadoFabricacion', array('P','T','E','I'));
			$this->db->where('NumeroPedido !=', 0);

			foreach ($filtros as $filtro) {

				if ($filtro->nombre == "FechaPedido") {
					$this->db->where($filtro->nombre, $filtro->valor);
				}
				elseif ($filtro->nombre == "pedido") {
					$this->db->like("{fn CONCAT({fn CONCAT(CAST(SeriePedido AS VARCHAR(10)),'/')},CAST(NumeroPedido AS VARCHAR(10)))}", $filtro->valor);
				}
				else {
					$this->db->like($filtro->nombre, $filtro->valor);
				}

			}

			$query = $this->db->get('LineasPedidoCliente_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerPedidosPendientesEntrega ($comisionista=false) {

			$this->db->select("{fn CONCAT({fn CONCAT(CAST(p.SeriePedido AS VARCHAR(10)),'/')},CAST(p.NumeroPedido AS VARCHAR(10)))} AS pedido, p.FechaPedido AS fecha, p.CodigoArticulo AS codigoArticulo, p.DescripcionArticulo AS descripcionArticulo,
							   p.UnidadesPedidas as unidadesPedidas, p.UnidadesPendientes AS unidadesPendientes, p.EstadoFabricacion AS estado,
							   c.CodigoCliente as codigoCliente, c.RazonSocial AS nombreCliente");
			$this->db->from("Clientes_cic AS c");
			$this->db->join('LineasPedidoCliente_cic AS p', 'c.CodigoCliente = p.CodigoCliente');
			$this->db->where_in('p.EstadoFabricacion', array('P','T','E','I'));
			if ($comisionista) $this->db->where('c.CodigoComisionista', $comisionista);
			$this->db->where('p.NumeroPedido !=', 0);
			$this->db->order_by('c.RazonSocial, p.SeriePedido, p.NumeroPedido','asc');
			$query = $this->db->get();
			$pendientesEntrega = $query->result();

			return $pendientesEntrega;

		}

		function obtenerSerieNumeroAlbaranesReservaClienteInicial ($cliente, $comisionista) {

			$this->db->select("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))} AS albaran");
			if ($comisionista) $this->db->where('CodigoComisionista', $comisionista);

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('codigoCliente', $cliente->codigo);

			}

			$this->db->where('CodigoArticulo', "R");
			$query = $this->db->get('LineasAlbaranCliente_cic');

			$serieNumeroAlbaranesReserva = array();

			foreach ($query->result() AS $row) {

				$serieNumeroAlbaranesReserva[] = $row->albaran;

			}

			return $serieNumeroAlbaranesReserva;

		}

		function obtenerLineasAlbaranesReservaClienteInicial ($cliente, $comisionista, $serieNumeroAlbaranesReserva) {

			if (count($serieNumeroAlbaranesReserva) > 0) {

				$this->db->select("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))} AS albaran, FechaAlbaran AS fecha, CodigoArticulo AS codigoArticulo, DescripcionArticulo as descripcionArticulo,
							   	   Unidades AS unidades");
				if ($comisionista) $this->db->where('CodigoComisionista', $comisionista);

				// Hay que hayar tambien los del cliente anterior
				if ($cliente->cliente_anterior != '') {

					$this->db->where("(codigoCliente = '".$cliente->codigo."'");
					$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

				}
				// No tiene cliente anterior
				else {

					$this->db->where('codigoCliente', $cliente->codigo);

				}

				$this->db->where('CodigoArticulo !=', "R");
				$this->db->where_in("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $serieNumeroAlbaranesReserva);
				$this->db->order_by ("SerieAlbaran, NumeroAlbaran", "asc");
				$query = $this->db->get('LineasAlbaranCliente_cic'/*, 25*/);

				$albaranesReserva = $query->result();

			}
			else {
				$albaranesReserva = array();
			}

			return $albaranesReserva;

		}

		function obtenerCantidadLineasAlbaranesReservaClienteInicial($cliente, $comisionista, $serieNumeroAlbaranesReserva) {

			if (count($serieNumeroAlbaranesReserva) > 0) {

				$this->db->select('COUNT(*) AS cantidad');
				if ($comisionista) $this->db->where('CodigoComisionista', $comisionista);

				// Hay que hayar tambien los del cliente anterior
				if ($cliente->cliente_anterior != '') {

					$this->db->where("(codigoCliente = '".$cliente->codigo."'");
					$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

				}
				// No tiene cliente anterior
				else {

					$this->db->where('codigoCliente', $cliente->codigo);

				}

				$this->db->where('CodigoArticulo !=', "R");
				$this->db->where_in("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $serieNumeroAlbaranesReserva);
				$query = $this->db->get('LineasAlbaranCliente_cic');
				$resultado = $query->row();

				$cantidad = $resultado->cantidad;

			}
			else {

				$cantidad = 0;

			}

			return $cantidad;

		}

		function obtenerOtrosAlbaranesReservaCliente($cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion, $comisionista, $serieNumeroAlbaranesReserva){

			if (count($serieNumeroAlbaranesReserva) > 0) {

				$this->db->select("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))} AS albaran, FechaAlbaran AS fecha, CodigoArticulo AS codigoArticulo, DescripcionArticulo as descripcionArticulo,
								   	   Unidades AS unidades");
				if ($comisionista) $this->db->where('CodigoComisionista', $comisionista);

				// Hay que hayar tambien los del cliente anterior
				if ($cliente->cliente_anterior != '') {

					$this->db->where("(codigoCliente = '".$cliente->codigo."'");
					$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

				}
				// No tiene cliente anterior
				else {

					$this->db->where('codigoCliente', $cliente->codigo);

				}

				$this->db->where('CodigoArticulo !=', "R");
				$this->db->where_in("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $serieNumeroAlbaranesReserva);
				/*$this->db->limit($agrupacion, $inicioLimit);*/

				foreach ($filtros as $filtro) {

					if ($filtro->nombre == "FechaAlbaran") {
						$this->db->where($filtro->nombre, $filtro->valor);
					}
					elseif ($filtro->nombre == "pedido") {
						$this->db->like("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $filtro->valor);
					}
					else {
						$this->db->like($filtro->nombre, $filtro->valor);
					}

				}

				if ($columnaOrdenacion != "") {
					if ($columnaOrdenacion == "pedido") {
						$this->db->order_by("SerieAlbaran, NumeroAlbaran", $tipoOrdenacion);
					}
					else {
						$this->db->order_by($columnaOrdenacion, $tipoOrdenacion);
					}
				}

				else {
					$this->db->order_by ("SerieAlbaran, NumeroAlbaran", "asc");
				}

				$query = $this->db->get('LineasAlbaranCliente_cic');
				$albaranesReserva = $query->result();

			}
			else {
				$albaranesReserva = array();
			}

			return $albaranesReserva;

		}

		function obtenerCantidadOtrosAlbaranesReservaCliente($cliente, $filtros, $comisionista, $serieNumeroAlbaranesReserva) {

			if (count($serieNumeroAlbaranesReserva) > 0) {

				$this->db->select('COUNT(*) AS cantidad');
				if ($comisionista) $this->db->where('CodigoComisionista', $comisionista);

				// Hay que hayar tambien los del cliente anterior
				if ($cliente->cliente_anterior != '') {

					$this->db->where("(codigoCliente = '".$cliente->codigo."'");
					$this->db->or_where(" codigoCliente = '".$cliente->cliente_anterior."')");

				}
				// No tiene cliente anterior
				else {

					$this->db->where('codigoCliente', $cliente->codigo);

				}

				$this->db->where('CodigoArticulo !=', "R");
				$this->db->where_in("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $serieNumeroAlbaranesReserva);

				foreach ($filtros as $filtro) {

					if ($filtro->nombre == "FechaAlbaran") {
						$this->db->where($filtro->nombre, $filtro->valor);
					}
					elseif ($filtro->nombre == "pedido") {
						$this->db->like("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $filtro->valor);
					}
					else {
						$this->db->like($filtro->nombre, $filtro->valor);
					}

				}

				$query = $this->db->get('LineasAlbaranCliente_cic');
				$resultado = $query->row();

				$cantidad = $resultado->cantidad;

			}
			else {
				$cantidad = 0;
			}

			return $cantidad;

		}

		function obtenerSerieNumeroAlbaranesReserva ($comisionista) {

			$this->db->select("{fn CONCAT({fn CONCAT(CAST(a.SerieAlbaran AS VARCHAR(10)),'/')},CAST(a.NumeroAlbaran AS VARCHAR(10)))} AS albaran");
			$this->db->from('LineasAlbaranCliente_cic AS a');
			$this->db->join('Clientes_cic AS c', 'a.CodigoCliente = c.CodigoCliente');
			$this->db->where('a.CodigoComisionista', $comisionista);
			$this->db->where('a.CodigoArticulo', "R");
			$this->db->order_by('c.RazonSocial, a.SerieAlbaran, a.NumeroAlbaran');
			$query = $this->db->get();

			$serieNumeroAlbaranesReserva = array();

			foreach ($query->result() AS $row) {

				$serieNumeroAlbaranesReserva[] = $row->albaran;

			}

			return $serieNumeroAlbaranesReserva;

		}

		function obtenerAlbaranesReserva ($comisionista, $serieNumeroAlbaranesReserva) {

			if (count($serieNumeroAlbaranesReserva) > 0) {

				$this->db->select("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))} AS pedido, a.FechaAlbaran AS fecha, a.CodigoArticulo AS codigoArticulo, a.DescripcionArticulo as descripcionArticulo,
							   	   a.Unidades AS unidades, c.CodigoCliente AS codigoCliente, c.RazonSocial AS nombreCliente");
				$this->db->from("LineasAlbaranCliente_cic AS a");
				$this->db->join('Clientes_cic AS c', 'c.CodigoCliente = a.CodigoCliente');
				$this->db->where('a.CodigoComisionista', $comisionista);
				$this->db->where('a.CodigoArticulo !=', "R");
				$this->db->where_in("{fn CONCAT({fn CONCAT(CAST(SerieAlbaran AS VARCHAR(10)),'/')},CAST(NumeroAlbaran AS VARCHAR(10)))}", $serieNumeroAlbaranesReserva);
				$this->db->order_by ("a.SerieAlbaran, a.NumeroAlbaran", "asc");
				$query = $this->db->get('');

				$albaranesReserva = $query->result();

			}
			else {
				$albaranesReserva = array();
			}

			return $albaranesReserva;

		}

		function obtenerClientesPendientesEntrega ($comisionista=false) {

			$this->db->select("DISTINCT(c.CodigoCliente) as codigo, c.RazonSocial AS nombre");
			$this->db->from("Clientes_cic AS c");
			$this->db->join('LineasPedidoCliente_cic AS p', 'c.CodigoCliente = p.CodigoCliente');
			$this->db->where_in('p.EstadoFabricacion', array('P','T','E','I'));
			if ($comisionista) $this->db->where('c.CodigoComisionista', $comisionista);
			$this->db->where('p.NumeroPedido !=', 0);
			$this->db->order_by('c.RazonSocial','asc');
			$query = $this->db->get();
			$clientesConPendientesEntrega = $query->result();

			return $clientesConPendientesEntrega;

		}

		function obtenerClientesAlbaranesReserva ($comisionista=false) {

			$this->db->select("DISTINCT(c.CodigoCliente) as codigo, c.RazonSocial AS nombre");
			$this->db->from("Clientes_cic AS c");
			$this->db->join('LineasAlbaranCliente_cic AS a', 'c.CodigoCliente = a.CodigoCliente');
			if ($comisionista) $this->db->where('c.CodigoComisionista', $comisionista);
			$this->db->where('a.CodigoArticulo', "R");
			$this->db->order_by('c.RazonSocial','asc');
			$query = $this->db->get();

			$clientesConAlbaranesReserva = $query->result();

			return $clientesConAlbaranesReserva;

		}

		function obtenerClientesFormularioPedidoSinFiltros ($comisionista, $nombre_cliente, $codigo_cliente) {

			$this->db->select('codigoCliente AS codigo, RazonSocial AS nombre, EMail1 AS email, Municipio AS municipio, TarifaPrecio AS tarifa');
			$this->db->where('CodigoComisionista',$comisionista);
			$this->db->where('BloqueoPedido !=','-1');

			if ($codigo_cliente != "") {
				$this->db->like("CodigoCliente", $codigo_cliente);
			}
			elseif ($nombre_cliente != "") {
				$palabrasFiltro = explode(" ", $nombre_cliente);
				foreach($palabrasFiltro as $palabraFiltro){
					$this->db->like("RazonSocial", $palabraFiltro);
				}
			}

			$this->db->order_by("RazonSocial");
			$query = $this->db->get('Clientes_cic');
			$clientes = $query->result();

			return $clientes;

		}

		function obtenerClientesFormularioPedidoConFiltros ($comisionista, $nombre_cliente, $codigo_cliente, $filtros) {

			// Obtenemos los codigos de los clientes del filtro inicial en un array
			$this->db->select('CodigoCliente AS codigo');
			$this->db->where('CodigoComisionista',$comisionista);
			$this->db->where('BloqueoPedido !=','-1');

			if ($codigo_cliente != "") {
				$this->db->like("CodigoCliente", $codigo_cliente);
			}
			elseif ($nombre_cliente != "") {
				$palabrasFiltro = explode(" ", $nombre_cliente);
				foreach($palabrasFiltro as $palabraFiltro){
					$this->db->like("RazonSocial", $palabraFiltro);
				}
			}

			$query = $this->db->get('Clientes_cic');
			$clientes = $query->result();

			$codigos_clientes = Array();

			foreach ($clientes as $cliente) $codigos_clientes[] = $cliente->codigo;

			// Ejecutamos el filtro a dichos clientes, sabiendo su codigo
			$this->db->select('codigoCliente AS codigo, RazonSocial AS nombre, EMail1 AS email, Municipio AS municipio, TarifaPrecio AS tarifa');
			$this->db->where_in('CodigoCliente', $codigos_clientes);

			foreach ($filtros as $filtro) {
				$palabrasFiltro = explode(" ", $filtro->valor);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like($filtro->nombre, $palabraFiltro);
				}
			}

			$this->db->order_by("RazonSocial");
			$query = $this->db->get('Clientes_cic');
			$clientes = $query->result();

			return $clientes;

		}

		function obtenerHistoricoFormularioPedido ($cliente, $filtros) {

			$this->db->select('ac.CodigoArticulo AS codigo, a.DescripcionArticulo AS descripcion,
							   ac.FechaUltimoAlbaran AS fechaUltimo, ac.UnidadesUltimoAlbaran as unidadesUltimo,
							   ac.%Descuento AS descuentoUltimo, ac.PrecioUltimoAlbaran AS precioUltimo');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'a.CodigoArticulo = ac.CodigoArticulo');
			if (count($filtros)) {
				foreach($filtros as $filtro) {
					if ($filtro->nombre == "ac.FechaUltimoAlbaran") {
						$this->db->where($filtro->nombre, $filtro->valor);
					}
					else {
						$palabrasFiltro = explode(" ", $filtro->valor);
						foreach($palabrasFiltro as $palabraFiltro){
							$this->db->like($filtro->nombre, $palabraFiltro);
						}

					}
				}
			}

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(ac.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" ac.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('ac.codigoCliente', $cliente->codigo);

			}

			$this->db->order_by("fechaUltimo", "desc");
			$query = $this->db->get();
			$historico = $query->result();

			return $historico;

		}

		function obtenerPendientesFormularioPedido($cliente, $filtros) {

			$columnaPrecio = 'a.PrecioVentasinIVA'.$cliente->tarifa;

			$this->db->select("lp.SeriePedido AS seriePedido, lp.NumeroPedido AS numeroPedido,
							   lp.FechaPedido AS fechaPedido, a.CodigoArticulo AS codigoArticulo,
							   a.DescripcionArticulo AS descripcionArticulo, lp.UnidadesPedidas AS cantidadArticulo,
							   ".$columnaPrecio." AS precioArticulo, a.StockVirtual AS stockVirtual,
							   a.StockDisponible AS stockDisponible, a.PendienteServir AS pendienteServir,
							   a.PendienteRecibir AS pendienteRecibir, a.PendienteFabricar AS pendienteFabricar,
							   lp.%Descuento AS descuentoArticulo, lp.CodigoArticulo AS codigoArticuloPedido,
							   lp.DescripcionArticulo AS descripcionArticuloPedido, lp.Precio AS precioArticuloPedido");
			$this->db->from("LineasPedidoCliente_cic AS lp");
			$this->db->join("Articulos_cic AS a", "a.CodigoArticulo = lp.CodigoArticulo");

			// Hay que hayar tambien los del cliente anterior
			if ($cliente->cliente_anterior != '') {

				$this->db->where("(lp.codigoCliente = '".$cliente->codigo."'");
				$this->db->or_where(" lp.codigoCliente = '".$cliente->cliente_anterior."')");

			}
			// No tiene cliente anterior
			else {

				$this->db->where('lp.codigoCliente', $cliente->codigo);

			}

			$this->db->where_in('lp.EstadoFabricacion', array('P','T','E','I'));
			$this->db->where('lp.NumeroPedido !=', 0);

			if (count($filtros)) {
				foreach($filtros as $filtro) {
					if ($filtro->nombre == "lp.FechaPedido") {
						$this->db->where($filtro->nombre, $filtro->valor);
					}
					else {
						$palabrasFiltro = explode(" ", $filtro->valor);
						foreach($palabrasFiltro as $palabraFiltro){
							$this->db->like($filtro->nombre, $palabraFiltro);
						}

					}
				}
			}

			$this->db->order_by('lp.SeriePedido, lp.NumeroPedido','desc');
			$query = $this->db->get();
			$pedidos = $query->result();

			foreach ($pedidos as $index => $pedido) {

				// Comprobamos uno a uno que no haya sido un articulo introducido a mano, en vez de uno de la BD
				if ($pedido->codigoArticuloPedido == ".") {

					$pedidos[$index]->codigoArticulo = ".";
					$pedidos[$index]->descripcionArticulo = $pedido->descripcionArticuloPedido;
					$pedidos[$index]->precioArticulo = $pedido->precioArticuloPedido;

				}

			}

			return $pedidos;

		}

		function obtenerArticulosFormularioPedidoSinFiltros ($nombre_articulo, $codigo_articulo, $limit, $articulos_cliente, $tarifa) {

			$columnaPrecio = 'PrecioVentasinIVA'.$tarifa;

			$this->db->select('codigoArticulo AS codigo, DescripcionArticulo AS descripcion,
							   '.$columnaPrecio.' AS precio, StockVirtual AS stockVirtual, StockDisponible
							   AS stockDisponible, PendienteServir AS pendienteServir, PendienteRecibir AS
							   pendienteRecibir, PendienteFabricar AS pendienteFabricar');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			if ($codigo_articulo != "") {
				$this->db->like("codigoArticulo", $codigo_articulo);
			}
			elseif ($nombre_articulo != "") {
				$palabrasFiltro = explode(" ", $nombre_articulo);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like("DescripcionArticulo", $palabraFiltro);
				}
			}

			$this->db->limit(100, $limit);
			$this->db->order_by("DescripcionArticulo, CodigoArticulo");
			$query = $this->db->get('Articulos_cic');
			$articulos = $query->result();

			return $articulos;

		}

		function obtenerArticulosFormularioPedidoConFiltros ($nombre_articulo, $codigo_articulo, $filtros, $limit, $articulos_cliente, $tarifa) {

			$columnaPrecio = 'PrecioVentasinIVA'.$tarifa;

			$this->db->select('codigoArticulo AS codigo, DescripcionArticulo AS descripcion,
							   '.$columnaPrecio.' AS precio, StockVirtual AS stockVirtual, StockDisponible
							   AS stockDisponible, PendienteServir AS pendienteServir, PendienteRecibir AS
							   pendienteRecibir, PendienteFabricar AS pendienteFabricar');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			if ($codigo_articulo != "") {
				$this->db->like("codigoArticulo", $codigo_articulo);
			}
			elseif ($nombre_articulo != "") {
				$palabrasFiltro = explode(" ", $nombre_articulo);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like("DescripcionArticulo", $palabraFiltro);
				}
			}

			foreach ($filtros as $filtro) {
				$palabrasFiltro = explode(" ", $filtro->valor);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like($filtro->nombre, $palabraFiltro);
				}
			}

			$this->db->limit(100, $limit);
			$this->db->order_by("DescripcionArticulo, CodigoArticulo");
			$query = $this->db->get('Articulos_cic');
			$articulos = $query->result();

			return $articulos;

		}

		function obtenerCantidadArticulosTotalesSinFiltros($nombre_articulo, $codigo_articulo, $articulos_cliente) {

			$this->db->select('COUNT(*) AS cantidad');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			if ($codigo_articulo != "") {
				$this->db->like("codigoArticulo", $codigo_articulo);
			}
			elseif ($nombre_articulo != "") {
				// Finalmente el campo codigo_nombre solo filtrara por el nombre
				$palabrasFiltro = explode(" ", $nombre_articulo);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like("DescripcionArticulo", $palabraFiltro);
				}
			}

			$query = $this->db->get('Articulos_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerCantidadArticulosTotalesConFiltros($nombre_articulo, $codigo_articulo, $filtros, $articulos_cliente) {

			$this->db->select('COUNT(*) AS cantidad');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			if ($codigo_articulo != "") {
				$this->db->like("codigoArticulo", $codigo_articulo);
			}
			elseif ($nombre_articulo != "") {
				// Finalmente el campo codigo_nombre solo filtrara por el nombre
				$palabrasFiltro = explode(" ", $nombre_articulo);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like("DescripcionArticulo", $palabraFiltro);
				}
			}

			foreach ($filtros as $filtro) {
				$palabrasFiltro = explode(" ", $filtro->valor);
				foreach ($palabrasFiltro as $palabraFiltro) {
					$this->db->like($filtro->nombre, $palabraFiltro);
				}
			}

			$query = $this->db->get('Articulos_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerArticulosInicial ($articulos_cliente, $tarifa) {

			$columnaPrecio = 'PrecioVentasinIVA'.$tarifa;

			$this->db->select('codigoArticulo AS codigo, DescripcionArticulo AS descripcion,
							   '.$columnaPrecio.' AS precio, StockVirtual AS stockVirtual, StockDisponible
							   AS stockDisponible, PendienteServir AS pendienteServir, PendienteRecibir AS
							   pendienteRecibir, PendienteFabricar AS pendienteFabricar');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			$query = $this->db->get('Articulos_cic', 25);
			$articulos = $query->result();

			return $articulos;

		}

		function obtenerCantidadArticulosTotales($articulos_cliente) {

			$this->db->select('COUNT(*) AS cantidad');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			$query = $this->db->get('Articulos_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerOtrosArticulos($inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion, $articulos_cliente, $tarifa){

			$columnaPrecio = 'PrecioVentasinIVA'.$tarifa;

			$this->db->select('codigoArticulo AS codigo, DescripcionArticulo AS descripcion,
							   '.$columnaPrecio.' AS precio, StockVirtual AS stockVirtual, StockDisponible
							   AS stockDisponible, PendienteServir AS pendienteServir, PendienteRecibir AS
							   pendienteRecibir, PendienteFabricar AS pendienteFabricar');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			foreach ($filtros as $filtro) {

				$this->db->like($filtro->nombre, $filtro->valor);

			}

			if ($columnaOrdenacion != "") {
				$this->db->order_by($columnaOrdenacion, $tipoOrdenacion);
			}
			else {
				$this->db->order_by('CodigoArticulo', 'ASC');
			}

			$this->db->limit($agrupacion, $inicioLimit);
			$query = $this->db->get('Articulos_cic');
			$articulos = $query->result();

			return $articulos;

		}

		function obtenerCantidadOtrosArticulos($filtros, $articulos_cliente) {

			$this->db->select('COUNT(*) AS cantidad');

			// Si es cliente, solo se obtendran los articulos para los clientes
			if ($articulos_cliente) $this->db->like('tarifa', '*T');

			foreach ($filtros as $filtro) {

				$this->db->like($filtro->nombre, $filtro->valor);

			}

			$query = $this->db->get('Articulos_cic');
			$resultado = $query->row();

			return $resultado->cantidad;

		}

		function obtenerNombreComisionista ($comisionista_id) {

			$this->db->select('Comisionista AS nombre');
			$this->db->where('CodigoComisionista', $comisionista_id);
			$query = $this->db->get('Comisionistas_cic');

			if ($query->num_rows() > 0) {
				$resultado = $query->row();
				$nombreComisionista = $resultado->nombre;
			}
			else {
				$nombreComisionista = false;
			}

			return $nombreComisionista;

		}

		/*** Esta funcion es la que se tendra que usar en un futuro. Esta comentada porque es para base de datos ***/
		/*** local, y mientras este en pruebas es remota ***/
		/*function insercionPedido($datos) {

			$orden = 5;
			$correcto = true;

			foreach ($datos->lineas as $linea) {

				// Definimos los datos que va a tener la insercion
				$insercion = Array(
								"CodigoEmpresa" => $datos->empresaComisionista,
								"CodigoCliente" => $datos->codigoCliente,
								"RazonSocial" => $datos->nombreCliente,
								"EjercicioPedido" => date("Y"),
								"SeriePedido" => "ZA",
								"NumeroPedido" => $datos->numeroPedido,
								"FechaPedido" => date("Y-m-d")."T".date("H:i:s"),
								"SuPedido" => $datos->subpedido,
								"VPedidoWeb" => $datos->vpedidoweb,
								"CodigoArticulo" => $linea->codigo,
								"DescripcionArticulo" => $linea->nombre,
								"UnidadesPedidas" => $linea->cantidad,
								"Precio" => $linea->precio,
								"%Descuento" => $linea->descuento,
								"Orden" => $orden,
								"StatusProcesado" => 0,
								"NumeroDocumento" => 0
							);

				// Incrementamos el orden, que va de 5 en 5
				$orden = $orden + 5;

				// Realizamos la insercion de cada linea
				if (!$this->db->insert('VE_TemporalCrearPedido', $insercion)) $correcto = false;

			}

			// Realizamos una ultima insercion para añadir las observaciones, si existen
			if ($datos->observaciones != "") {

				$insercion = Array(
									"CodigoEmpresa" => intval($datos->empresaComisionista),
									"CodigoCliente" => $datos->codigoCliente,
									"RazonSocial" => $datos->nombreCliente,
									"EjercicioPedido" => intval(date("Y")),
									"SeriePedido" => "ZA",
									"NumeroPedido" => intval($datos->numeroPedido),
									"FechaPedido" => date("Y-m-d")."T".date("H:i:s"),
									"SuPedido" => $datos->subpedido,
									"VPedidoWeb" => $datos->vpedidoweb,
									"CodigoArticulo" => "*",
									"DescripcionArticulo" => $datos->observaciones,
									"UnidadesPedidas" => 0.00,
									"Precio" => 0.00,
									"%Descuento" => 0.00,
									"Orden" => intval($orden),
									"StatusProcesado" => 0,
									"NumeroDocumento" => 0
								);

				if (!$this->db->insert('VE_TemporalCrearPedido', $insercion)) $correcto = false;

			}

			return $correcto;

		}*/

		// Esta funcion se tendra que reemplazar en un futuro por la de arriba del mismo nombre, que esta comentada
		function insercionPedido($datos) {

			$db_cic = $this->load->database("sage", true);

			$orden = 5;
			$correcto = true;

			foreach ($datos->lineas as $linea) {

				// Definimos los datos que va a tener la insercion
				$insercion = Array(
								"CodigoEmpresa" => $datos->empresaComisionista,
								"CodigoCliente" => $datos->codigoCliente,
								"RazonSocial" => $datos->nombreCliente,
								"EjercicioPedido" => date("Y"),
								"SeriePedido" => "ZA",
								"NumeroPedido" => $datos->numeroPedido,
								"FechaPedido" => date("Y-m-d")."T".date("H:i:s"),
								"SuPedido" => $datos->subpedido,
								"VPedidoWeb" => $datos->vpedidoweb,
								"CodigoArticulo" => $linea->codigo,
								"DescripcionArticulo" => $linea->nombre,
								"UnidadesPedidas" => $linea->cantidad,
								"Precio" => $linea->precio,
								"%Descuento" => $linea->descuento,
								"Orden" => $orden,
								"StatusProcesado" => 0,
								"NumeroDocumento" => 0
							);

				// Incrementamos el orden, que va de 5 en 5
				$orden = $orden + 5;

				// Realizamos la insercion de cada linea
				if (!$db_cic->insert('VE_TemporalCrearPedido', $insercion)) $correcto = false;

			}

			// Realizamos una ultima insercion para añadir las observaciones, si existen
			if ($datos->observaciones != "") {

				$insercion = Array(
									"CodigoEmpresa" => intval($datos->empresaComisionista),
									"CodigoCliente" => $datos->codigoCliente,
									"RazonSocial" => $datos->nombreCliente,
									"EjercicioPedido" => intval(date("Y")),
									"SeriePedido" => "ZA",
									"NumeroPedido" => intval($datos->numeroPedido),
									"FechaPedido" => date("Y-m-d")."T".date("H:i:s"),
									"SuPedido" => $datos->subpedido,
									"VPedidoWeb" => $datos->vpedidoweb,
									"CodigoArticulo" => "*",
									"DescripcionArticulo" => $datos->observaciones,
									"UnidadesPedidas" => 0.00,
									"Precio" => 0.00,
									"%Descuento" => 0.00,
									"Orden" => intval($orden),
									"StatusProcesado" => 0,
									"NumeroDocumento" => 0
								);

				if (!$db_cic->insert('VE_TemporalCrearPedido', $insercion)) $correcto = false;

			}

			return $correcto;

		}

		function actualizarRegistroPedido ($cliente, $comisionista, $numero) {

			$data = array(
						"codigocliente" => $cliente,
						"codigocomisionista" => $comisionista
			);

			$this->db->where('NumeroPedidoWeb', $numero);
			$this->db->update('RegistroWeb_cic', $data);

		}

		function obtenerDatosArticulo ($codigo_articulo, $tarifa) {

			$columnaPrecio = 'PrecioVentasinIVA'.$tarifa;

			$this->db->select('DescripcionArticulo AS descripcion, StockVirtual AS stockVirtual,
			                   StockDisponible AS stockDisponible, PendienteServir AS pendienteServir,
							           PendienteRecibir AS pendienteRecibir, pendienteFabricar AS pendienteFabricar,
							           '.$columnaPrecio.' AS precio');
			$this->db->where('CodigoArticulo', $codigo_articulo);
			$query = $this->db->get('Articulos_cic');

			if ($query->num_rows() > 0) {
				$articulo = $query->row();
			}
			else {
				$articulo = false;
			}

			return $articulo;

		}

		function obtenerCodigoComisionistaCliente ($cliente_id) {

			$this->db->select('CodigoComisionista AS codigo');
			$this->db->where('CodigoCliente', $cliente_id);
			$query = $this->db->get('Clientes_cic');

			if ($query->num_rows() > 0) {
				$resultado = $query->row();
				$codigo_comisionista = $resultado->codigo;
			}
			else {
				$codigo_comisionista = false;
			}

			return $codigo_comisionista;

		}

		function obtenerEmpresaCliente ($cliente_id) {

			$this->db->select('CodigoEmpresa AS empresa');
			$this->db->where('CodigoCliente', $cliente_id);
			$query = $this->db->get('Clientes_cic');

			if ($query->num_rows() > 0) {
				$resultado = $query->row();
				$codigo_empresa = $resultado->empresa;
			}
			else {
				$codigo_empresa = false;
			}

			return $codigo_empresa;

		}

		function obtenerDescuentosArticuloCliente ($cliente_id) {

			$this->db->select('ac.CodigoArticulo, ac.%Descuento AS descuento');
			$this->db->from('ArticuloCliente_cic AS ac');
			$this->db->join('Articulos_cic AS a', 'ac.CodigoArticulo = a.CodigoArticulo');
			$this->db->where('ac.CodigoCliente', $cliente_id);
			$this->db->where('ac.%Descuento >', 0);
			$this->db->where('ac.CodigoArticulo !=', '.');
			$this->db->order_by("a.DescripcionArticulo, a.CodigoArticulo");

			$query = $this->db->get();

			$descuentos_articulos = $query->result();

			return $descuentos_articulos;

		}

		function obtener_todos_comisionistas_con_acceso () {

			$this->db->select('CodigoComisionista AS codigo, Comisionista AS nombre');
			$this->db->where('contrasena != ', '');
			$query = $this->db->get('Comisionistas_cic');

			return $query->result();

		}

		function obtener_todos_clientes_con_acceso () {

			$this->db->select('CodigoCliente AS codigo, RazonSocial AS nombre');
			$this->db->where('contrasena != ', '');
			$query = $this->db->get('Clientes_cic');

			return $query->result();

		}

		function comprobar_comisionista_existe_con_acceso ($id_comisionista) {

			$existe = false;

			$this->db->select('CodigoComisionista');
			$this->db->where('CodigoComisionista', $id_comisionista);
			$this->db->where('contrasena != ', '');
			$query = $this->db->get('Comisionistas_cic');

			if ($query->num_rows()) $existe = true;

			return $existe;

		}

		function comprobar_cliente_existe_con_acceso ($id_cliente) {

			$existe = false;

			$this->db->select('CodigoCliente');
			$this->db->where('CodigoCliente', $id_cliente);
			$this->db->where('contrasena != ', '');
			$query = $this->db->get('Clientes_cic');

			if ($query->num_rows()) $existe = true;

			return $existe;

		}

		function insertar_key_login ($datos) {

			$this->db->insert("KeysLogin_cic", $datos);

		}

		function obtener_keys_login () {

			$this->db->order_by("tipo_usuario, id_usuario, descripcion");
			$query = $this->db->get("KeysLogin_cic");

			return $query->result();

		}

		function comprobar_key_login_existe ($id_key) {

			$existe = false;

			$this->db->where("id", $id_key);
			$query = $this->db->get("KeysLogin_cic");

			if ($query->num_rows()) $existe = true;

			return $existe;

		}

		function eliminar_key ($id_key) {

			$this->db->where("id", $id_key);
			$query = $this->db->delete("KeysLogin_cic");

		}

		function obtener_key_login ($tipo_usuario, $usuario, $key, $registrada) {

			$datos_key = false;

			$this->db->where("tipo_usuario", $tipo_usuario);
			$this->db->where("id_usuario", $usuario);
			$this->db->where("key", $key);
			$this->db->where("registrada", $registrada);
			$query = $this->db->get("KeysLogin_cic");

			if ($query->num_rows()) $datos_key = $query->row();

			return $datos_key;

		}

		function update_key_login ($id, $datos) {

			$this->db->where("id", $id);
			$this->db->update("KeysLogin_cic", $datos);

		}

		function obtener_keys_login_usuario ($tipo_usuario, $usuario) {

			$this->db->where("tipo_usuario", $tipo_usuario);
			$this->db->where("id_usuario", $usuario);
			$query = $this->db->get("KeysLogin_cic");

			return $query->result();

		}

		function obtener_emails_configuracion () {

			$emails = Array();

			$this->db->select("valor");
			$this->db->where("configuracion", "emails_envio_pedido");
			$query = $this->db->get("ConfiguracionWeb_cic");

			if ($query->num_rows()) {

				$row = $query->row();

				// Los emails estan separados por coma
				$emails = explode(",", $row->valor);

			}

			return $emails;

		}

		function obtener_valor_configuracion ($configuracion, $tipo_usuario = false, $id_usuario = false, $formato = "row") {

			$valor = false;

			$this->db->select("valor");
			$this->db->where("configuracion", $configuracion);
			if ($tipo_usuario) $this->db->where("tipo_usuario", $tipo_usuario);
			if ($id_usuario) $this->db->where("id_usuario", $id_usuario);
			$query = $this->db->get("ConfiguracionWeb_cic");

			if ($query->num_rows()) {

				$row = $query->row();

				$valor = $row->valor;

			}

			return $valor;

		}

		function update_configuracion ($configuracion, $datos) {

			$this->db->where("configuracion", $configuracion);
			$this->db->update("ConfiguracionWeb_cic", $datos);

		}

		function obtener_codigo_articulo_cliente ($codigo_cliente, $codigo_articulo_cliente) {

			$codigo_articulo = false;

			$this->db->select("CodigoArticulo AS codigo");
			$this->db->where("CodigoCliente", $codigo_cliente);
			$this->db->where("CodigodelCliente", $codigo_articulo_cliente);
			$query = $this->db->get("ArticuloCliente_cic");

			if ($query->num_rows()) {

				$row = $query->row();
				$codigo_articulo = $row->codigo;

			}

			return $codigo_articulo;

		}

		function obtener_datos_articulo ($codigo_articulo) {

			$articulo = false;

			$this->db->select("CodigoArticulo AS codigo, DescripcionArticulo AS nombre,
			                   PrecioVentasinIVA1 AS precio_1, PrecioVentasinIVA2 AS precio_2,
												 StockVirtual AS stockVirtual, StockDisponible AS stockDisponible,
												 PendienteServir AS pendienteServir, PendienteRecibir AS pendienteRecibir,
												 PendienteFabricar AS pendienteFabricar");
			$this->db->where("CodigoArticulo", $codigo_articulo);
			$query = $this->db->get("Articulos_cic");

			if ($query->num_rows()) $articulo = $query->row();

			return $articulo;

		}

		function obtener_ofertas_cliente ($codigo_cliente) {

			$this->db->select("EjercicioOferta AS ejercicio, SerieOferta AS serie,
			                   NumeroOferta AS numero, MIN(FechaOferta) AS fecha");
			$this->db->where("CodigoCliente", $codigo_cliente);
			$this->db->group_by("EjercicioOferta, SerieOferta, NumeroOferta");
			$this->db->order_by("fecha", "DESC");
			$query = $this->db->get("LineasOfertaCliente_cic");

			return $query->result();

		}

		function obtener_lineas_oferta ($linea) {

			$this->db->select("codigoArticulo, descripcionArticulo, unidadesPedidas,
			                   precio, %Descuento AS descuento");
			$this->db->where("EjercicioOferta", $linea->ejercicio);
			$this->db->where("SerieOferta", $linea->serie);
			$this->db->where("NumeroOferta", $linea->numero);
			$query = $this->db->get("LineasOfertaCliente_cic");

			return $query->result();

		}

	}

?>
