<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container container-propio marg-top-15'>

			<form id="__vtigerWebForm" name="PRECONTACTO desde SIS" action="https://crm.abrastar.com:8443/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
				<div class="panel panel-primary">

						<div class="panel-heading clearfix">
							<div class="row">
								<div class="col-xs-12">
									<input class="btn btn-default text-primary floatR" type="submit" value="Enviar">
								</div>
							</div>
						</div>

						<div id="cuerpo-formularios" class="panel-body">
							<div class="row">
								<input type="hidden" name="__vtrftk" value="sid:c790729ed201f8dbe8697015cdfe35a215a14aea,1498236467">
								<input type="hidden" name="publicid" value="6e76a6014ebf6cfaa52219d3b5dde3cc">
								<input type="hidden" name="urlencodeenable" value="1">
								<input type="hidden" name="name" value="PRECONTACTO desde SIS">
								<div class="form-group col-xs-12 col-sm-6">
									<label>Nombre</label>
									<input type="text" name="firstname" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Apellido<span class="required">*</span></label>
									<input type="text" name="lastname" data-label="" value="" required="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Empresas</label>
									<input type="text" name="company" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Teléfono Principal</label>
									<input type="text" name="phone" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Móvil</label>
									<input type="text" name="mobile" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Código Postal</label>
									<input type="text" name="code" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12">
									<label>Dirección</label>
									<textarea name="lane" class="form-control"></textarea>
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Población</label>
									<input type="text" name="city" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Provincia</label>
									<input type="text" name="state" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Origen de Pre-Contacto</label>
									<select name="leadsource" data-label="leadsource" class="form-control">
										<option value="">--Selecciona Valor--</option>
										<option value="Cold Call">Llamada fria</option>
										<option value="Existing Customer">Cliente</option>
										<option value="Self Generated">Autogenerada</option>
										<option value="Employee">Empleado</option>
										<option value="Partner">Socio</option>
										<option value="Public Relations">Relaciones Públicas</option>
										<option value="Direct Mail">Mailing</option>
										<option value="Conference">Conferencia</option>
										<option value="Trade Show">Feria</option>
										<option value="Web Site">Sitio Web</option>
										<option value="Word of mouth">Boca a Boca</option>
										<option value="Other">Otro</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>De Correo Electrónico Principal</label>
									<input type="email" name="email" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Correo Electrónico Secundaria</label>
									<input type="email" name="secondaryemail" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Página Web</label>
									<input type="text" name="website" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Facturación Anual</label>
									<input type="text" name="annualrevenue" data-label="" value="" class="form-control">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Estado Pre-Contacto</label>
									<select name="leadstatus" data-label="leadstatus" class="form-control">
										<option value="">--Selecciona Valor--</option>
										<option value="Attempted to Contact">Intentado Contactar</option>
										<option value="Cold">Frío</option>
										<option value="Contact in Future">Contactar más adelante</option>
										<option value="Contacted">Contactado</option>
										<option value="Hot">Caliente</option>
										<option value="Junk Lead">Pre-Contacto Basura</option>
										<option value="Lost Lead">Pre-Contacto Fallido</option>
										<option value="Not Contacted">No Contactado</option>
										<option value="Pre Qualified">Pre Calificado</option>
										<option value="Qualified">Calificado</option>
										<option value="Warm">Tibio</option>
									</select>
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Numero de Empleados</label>
									<input type="number" name="noofemployees" data-label="" value="" class="form-control" min="0">
								</div>
								<div class="form-group col-xs-12 col-sm-6">
									<label>Clasificación</label>
									<select name="rating" data-label="rating" class="form-control">
										<option value="">--Selecciona Valor--</option>
										<option value="Acquired">Adquirido</option>
										<option value="Active">Activo</option>
										<option value="Market Failed">Mercado Inmaduro</option>
										<option value="Project Cancelled">Cancelado</option>
										<option value="Shutdown">Suspendido</option>
									</select>
								</div>
								<div class="form-group col-xs-12">
									<label>Descripción</label>
									<textarea name="description" class="form-control"></textarea>
								</div>
							</div>
						</div>

				</div>
			</form>

		</div>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
