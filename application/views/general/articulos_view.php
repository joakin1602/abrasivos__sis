<html>
	<head>
		
		<?php $this->load->view('head_view'); ?>
		
	</head>
	<body>
		
		<?php $this->load->view('menu_view'); ?>
		
		<!-- Inicio panel -->
		<div class="container gc-container container-propio marg-top-11">
			<div class="row">
				<div class="table-section">
					<div class="table-container">
						<form accept-charset="utf-8" id="gcrud-search-form" autocomplete="off" method="post">
							<div class="header-tools">
								<div class="clear"></div>
							</div>
							<table class="table table-bordered grocery-crud-table table-hover">
								<thead>
									<tr class="titulos-columnas">
										<th data-order-by="CodigoArticulo" class="column-with-ordering celda-titulo-columna">Código</th>
										<th data-order-by="DescripcionArticulo" class="column-with-ordering celda-titulo-columna">Descripción</th>
										<th data-order-by="PreciosVentasinIVA1" class="column-with-ordering celda-titulo-columna">Precio</th>									
										<th data-order-by="StockVirtual" class="column-with-ordering celda-titulo-columna">Stock</th>
									</tr>

									<tr class="filter-row gc-search-row">
										<td>
										<input type="text" name="CodigoArticulo" placeholder="Código" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="DescripcionArticulo" placeholder="Descripción" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="PreciosVentasinIVA1" placeholder="Precio" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="StockVirtual" placeholder="Stock" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
									</tr>

								</thead>
								<tbody class="listado-registros">

									<?php foreach($registros_iniciales as $index => $registro): ?>
										
										<?php $odd_even = $index % 2 == 0 ? "odd" : "even"; ?>
										
										<tr class="<?php echo $odd_even; ?>">
											<td><?php echo $registro->codigo; ?></td>
											<?php if ($registro->descripcion == "") $registro->descripcion = "-"; ?>
											<td><?php echo $registro->descripcion; ?></td>
											<td><?php echo round($registro->precio, 3); ?> €</td>
											<td>
												<button type="button" class="btn <?php echo $registro->boton_stock; ?> boton-stock-articulo"><?php echo round_to_2dp($registro->stockVirtual); ?></button>
												<input type="hidden" class="stock_codigo_articulo" value="<?php echo $registro->codigo;?>">
												<input type="hidden" class="stock_virtual_articulo" value="<?php echo round_to_2dp($registro->stockVirtual);?>">
												<input type="hidden" class="stock_disponible_articulo" value="<?php echo round_to_2dp($registro->stockDisponible);?>">
												<input type="hidden" class="pendiente_servir_articulo" value="<?php echo round_to_2dp($registro->pendienteServir);?>">
												<input type="hidden" class="pendiente_recibir_articulo" value="<?php echo round_to_2dp($registro->pendienteRecibir);?>">
												<input type="hidden" class="pendiente_fabricar_articulo" value="<?php echo round_to_2dp($registro->pendienteFabricar);?>">
											</td>
										</tr>
										
									<?php endforeach; ?>

								</tbody>
							</table>
							<!-- Table Footer -->
							<div class="footer-tools">

								<!-- "Show 10/25/50/100 entries" (dropdown per-page) -->
								<div class="floatL t20 l5">
									<div class="floatL t10">
										Mostrar
									</div>
									<div class="floatL r5 l5 t3">
										<select class="per_page form-control" name="per_page">
											<option value="10"> 10&nbsp;&nbsp; </option>
											<option selected="selected" value="25"> 25&nbsp;&nbsp; </option>
											<option value="50"> 50&nbsp;&nbsp; </option>
											<option value="100"> 100&nbsp;&nbsp; </option>
										</select>
									</div>
									<div class="floatL t10">
										registros
									</div>
									<div class="clear"></div>
								</div>
								<!-- End of "Show 10/25/50/100 entries" (dropdown per-page) -->

								<div class="floatR r5">

									<!-- Buttons - First,Previous,Next,Last Page -->
									<ul class="pagination">
										<li class="disabled paging-first">
											<a href="#"><i class="fa fa-step-backward"></i></a>
										</li>
										<li class="prev disabled paging-previous">
											<a href="#"><i class="fa fa-chevron-left"></i></a>
										</li>
										<li>
											<span class="page-number-input-container">
												<input type="numeric" class="form-control page-number-input text-center" value="1">
											</span>
										</li>
										<li class="disabled next paging-next">
											<a href="#"><i class="fa fa-chevron-right"></i></a>
										</li>
										<li class="disabled paging-last">
											<a href="#"><i class="fa fa-step-forward"></i></a>
										</li>
									</ul>
									<!-- End of Buttons - First,Previous,Next,Last Page -->

									<input type="hidden" value="1" class="page-number-hidden" name="page_number">

									<!-- Start of: Settings button -->
									<button class="btn btn-default floatR t20 l10" type="button" id="clear-filtering">
										<span class="text-info"><i class="fa fa-eraser"></i> Limpiar filtro</span>
									</button>
									<!-- End of: Settings button -->

								</div>

								<!-- "Displaying 1 to 10 of 116 items" -->
								<div class="floatR r10 t30">
									<?php $registro_inicial = $cantidad_registros_iniciales > 1 ? 1 : 0; ?>
									Mostrando <span class="paging-starts"><?php echo $registro_inicial; ?></span> a <span class="paging-ends"><?php echo $cantidad_registros_iniciales; ?></span> de <span class="current-total-results"><?php echo $cantidad_registros_totales; ?></span> registros <span class="full-total-container hidden"> (filtrando de <span class="full-total"><?php echo $cantidad_registros_totales; ?></span> total registros) </span>
								</div>
								<!-- End of "Displaying 1 to 10 of 116 items" -->

								<div class="clear"></div>
							</div>
							<!-- End of: Table Footer -->

						</form>
					</div>
				</div>

			</div>
			
		</div>
		
		<div id='modal-stock-articulo' class='hidden pantalla-emergente'>
			<div class='modal-dialog cuadro-emergente'>
				<div class='modal-content'>
					<div class='modal-body'>
						<div class="row">
							<div class="col-xs-12">
								<table>
									<tr>
										<td>Artículo</td>
										<td id="modal-stock-codigo-articulo"></td>
									</tr>
									<tr>
										<td>Stock Virtual</td>
										<td id="modal-stock-virtual"></td>
									</tr>
									<tr>
										<td>Stock Disponible</td>
										<td id="modal-stock-disponible"></td>
									</tr>
									<tr>
										<td>Pendiente Servir</td>
										<td id="modal-stock-servir"></td>
									</tr>
									<tr>
										<td>Pendiente Recibir</td>
										<td id="modal-stock-recibir"></td>
									</tr>
									<tr>
										<td>Pendiente Fabricar</td>
										<td id="modal-stock-fabricar"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class='modal-footer'>
					  <p class='modal-stock-estado'>
						<span id='modal-stock-bolita-estado'></span>
						<span id='modal-stock-texto-estado'></span>
					  </p>
					  <button type='button' class='btn btn-primary boton-cerrar-emergente'>Aceptar</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="div-cargando hidden">
			Cargando...
			<div class="fondo-cargando"></div>
		</div>
		
		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>
		
		<!-- Fin panel -->
	
		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>