<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body class="hidden">

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container container-propio pedidos-presupuestos'>

			<div class="col-sm-4 col-xs-6 elemento-pedidos-presupuestos">
				<a href="<?php echo base_url(); ?>index.php/pedidos_presupuestos/nuevo">
					<div id="nuevo" class="panel panel-primary" data-borrador="">
						<div class="panel-heading">
							<h4>Nuevo</h4>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-12 col-lg-12 text-center padding-icono">
									<i class="fa fa-plus-circle icono-pedidos"></i>
								</div>
							</div>
						</div>
						<div class="panel-footer"></div>
					</div>
				</a>
			</div>

		</div>

		<?php

		?>
		<input type="hidden" id="codigo_usuario" value="<?php echo $codigo_usuario; ?>">
		<input type="hidden" id="tipo_usuario" value="<?php echo $tipo_usuario; ?>">

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
