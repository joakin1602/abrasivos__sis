<html >
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Abrasivos - Login</title>
		<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>content/images/favicon.ico"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>content/css/bootstrap/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>content/css/main.css?v=1.01">
		<link rel="stylesheet" href="<?php echo base_url(); ?>content/css/login.css?v=1.00">
	</head>

	<body>

		<div class="container">

			<div id="formulario">

				<h3>Inicio de sesión</h3>

				<fieldset>

					<form id="login" action="" method="get">

						<input id="form-usuario" type="text" required placeholder="Usuario">

						<input id="form-clave" type="password" required placeholder="Contraseña">

						<div class="clearfix">
							<p id="mensaje"></p>
							<input id="boton-acceso" type="submit" value="Acceder">
						</div>

					</form>

					<div id="div-focusable"><input id="focusable" type="checkbox" value=""></div>

				</fieldset>

			</div>

		</div>

		<div id="logo">
			<img src="<?php echo base_url(); ?>content/images/logo.png">
		</div>

		<div id="introducir-key-login" class="pantalla-emergente hidden">
			<div class="modal-dialog cuadro-emergente">
				<button type="button" class="btn btn-danger boton-cerrar-emergente boton-x-emergente">X</button>
				<form id="registro-key" action="." method="post">
					<div class="modal-content">
						<div class="modal-body">
							<div class="row">
								<div class="col-xs-12">
									<p>Parece que no tienes una key correcta registrada en el dispositivo. Para poder iniciar sesión, regístrala a continuación, y si no la tienes, pídesela al administrador: </p>
									<input type="text" id="key" class="form-control" required>
									<input type="hidden" id="usuario-key">
									<input type="hidden" id="clave-key">
								</div>
							</div>
						</div>
						<div class="modal-footer">
						  <button type="submit" class="btn btn-primary boton-registrar-key">Registrar</button>
						</div>
					</div>
				</form>
			</div>

		</div>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
