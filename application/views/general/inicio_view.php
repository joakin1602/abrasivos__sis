<html>
	<head>
		
		<?php $this->load->view('head_view'); ?>
		
	</head>
	<body>
		
		<?php $this->load->view('menu_view'); ?>
		
		<!-- Contenido -->
		<div class='container container-propio marg-top-15'>
			
			<?php if ($this->session->tipo_usuario != 'cliente'): ?>
			
			<div class='col-xs-6 no-padd-left'>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4>Empresas con pendientes de entrega</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class=" col-md-12 col-lg-12 ">
								<table class="table table-user-information">
									<tbody>
										<?php foreach($clientesConPendientesEntrega as $cliente): ?>
											<tr>
												<td><?php echo $cliente->codigo." - ".$cliente->nombre; ?></td>
												<td><a class="btn btn-info" href="<?php echo base_url(); ?>index.php/clientes/pendientes_entrega/<?php echo $cliente->codigo; ?>">Ver</a></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class='col-xs-6 no-padd-right'>
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4>Empresas con albaranes reserva</h4>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class=" col-md-12 col-lg-12 ">
								<table class="table table-user-information">
									<tbody>
										<?php foreach($clientesConAlbaranesReserva as $cliente): ?>
											<tr>
												<td><?php echo $cliente->codigo." - ".$cliente->nombre; ?></td>
												<td><a class="btn btn-info" href="<?php echo base_url(); ?>index.php/clientes/albaranes_reserva/<?php echo $cliente->codigo; ?>">Ver</a></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<?php endif; ?>
			
		</div>
		
		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>
		
	</body>
</html>
