<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body class="hidden">

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container container-propio pedidos-presupuestos'>

			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<div class="row">
						<div class="col-xs-8">
							<button id="limpiar-formulario" class="btn btn-danger-2"><i class="fa fa-trash"></i> Limpiar formulario</button>
						</div>
						<div class="col-xs-4">
							<button id="enviar-pedido-presupuesto" type="button" class="btn btn-default text-primary floatR">Enviar</button>
							<button id="boton-borrador" type="button" class="btn btn-warning floatR hidden" data-borrador="<?php echo $borrador; ?>"><i class="fa fa-save"></i> Borrador</button>
						</div>
					</div>
				</div>

				<div id="cuerpo-formulario" class="panel-body">

					<input type="hidden" id="tipo_usuario" value="<?php echo $tipo_usuario; ?>">
					<input type="hidden" id="comisionista" value="<?php echo $codigo_comisionista; ?>">

					<div class="row">
					  	<div class="form-group col-xs-9">
							<?php if ($tipo_usuario == "comisionista"): ?>
								<label for="codigo_nombre_cliente">Cliente<span class="required">*</span></label>
								<div class="input-group">
								  <span class="input-group-addon buscador-input" id="buscar-cliente"><i class="fa fa-search"></i></span>
								  <input type="text" class="form-control" id="codigo_nombre_cliente" value="<?php if ($cliente) echo $codigo_cliente." - ".$nombre_cliente; ?>" autocomplete="off">
								  <span id="limpiar-input-cliente" class="input-group-addon limpiar-input"><i class="fa fa-eraser"></i></span>
								</div>
								<input type="hidden" id="codigo_cliente" value="<?php if ($cliente) echo $codigo_cliente; ?>">
								<input type="hidden" id="nombre_cliente" value="<?php if ($cliente) echo $nombre_cliente; ?>">
								<input type="hidden" id="tarifa_cliente" value="<?php if ($cliente) echo $tarifa_cliente; ?>">
							<?php elseif ($tipo_usuario == "cliente"): ?>
								<label for="codigo_nombre_cliente">Cliente<span class="required">*</span></label>
								<input type="text" class="form-control" id="codigo_nombre_cliente" value="<?php echo $codigo_cliente." - ".$nombre_cliente; ?>" disabled>
								<input type="hidden" id="codigo_cliente" value="<?php echo $codigo_cliente; ?>">
								<input type="hidden" id="nombre_cliente" value="<?php echo $nombre_cliente; ?>">
								<input type="hidden" id="tarifa_cliente" value="<?php echo $tarifa_cliente; ?>">
							<?php endif; ?>
					  	</div>

					  	<div class="form-group col-xs-3">
					    	<label for="tipo">Tipo</label>
					    	<select class="form-control" id="tipo">
					    		<option value="Pedido" selected="selected">Pedido</option>
					    		<option values="Presupuesto">Presupuesto</option>
					    	</select>
					  	</div>
					</div>

					<div class="row">
						<div class="form-group col-xs-12">
					  		<button id="boton-historico" type="button" class="btn btn-info" <?php if (!$cliente) echo "disabled" ?>>Histórico empresa</button>
					  		<button id="boton-pendientes" type="button" class="btn btn-info" <?php if (!$cliente) echo "disabled" ?>>Pedidos pendientes empresa</button>
					  	</div>
					</div>

					<div class="informacion-plegable">
						<div class="cabecera-informacion-plegable">
							<i class="fa fa-angle-down icono-plegable"></i> <span class="noselect">Datos del pedido</span> <i class="fa fa-angle-down icono-plegable"></i>
						</div>

						<div class="cuerpo-informacion-plegable informacion-plegada">
							<div class="row">
								<div class="form-group col-xs-6">
							    	<label for="numero">Número<span class="required">*</span></label>
							    	<input type="numeric" class="form-control" id="numero" value="<?php echo $numero; ?>" disabled>
							  	</div>

							  	<div class="form-group col-xs-6">
							    	<label for="fecha">Fecha<span class="required">*</span></label>
							    	<input type="text" class="form-control" id="fecha" value="<?php echo date("d/m/Y") ?>" disabled>
							  	</div>
							</div>

							<div class="row">
							  	<div class="form-group col-xs-4">
							    	<label for="subpedido_web">SubPedido Web</label>
							    	<input type="text" class="form-control" id="subpedido_web">
							  	</div>

							  	<div class="form-group col-xs-8">
							    	<label for="emails_envio">Emails de envío (separar con comas)</label>
							    	<input type="text" class="form-control" id="emails_envio" value="<?php if ($cliente) echo $email_cliente; ?>">
							  	</div>
							</div>

							<div class="row">
							  	<div class="form-group col-xs-12">
							    	<label for="observaciones">Observaciones (Limitado a 200 caracteres). Llevas <span id="caracteres-observaciones"></span></label>
							    	<textarea class="form-control" id="observaciones" maxlength="200"></textarea>
							  	</div>
							</div>
						</div>
					</div>

					<div class="row cols-menos-padding marg-top-10 titulos-columnas-lineas">
				  		<div class="columna columna-52 padding-left">
				  			Artículo
				  		</div>
				  		<div class="columna columna-6">
				  			Stock
				  		</div>
				  		<div class="columna columna-6">
				  			Uds.
				  		</div>
				  		<div class="columna columna-10">
				  			Precio
				  		</div>
						<div class="columna columna-10">
							Dto.
						</div>
				  		<div class="columna columna-10">
				  			T.Linea
				  		</div>
					</div>

				  	<div id="nueva-linea" class="row cols-menos-padding lineas-pedido">

					  	<div class="form-group columna columna-52 padding-left">
					    	<div class="input-group">
							  <span class="input-group-addon buscador-input" id="buscar-articulos"><i class="fa fa-search"></i></span>
							  <input id="codigo_articulo" type="text" class="form-control" placeholder="Código" autocomplete="off" maxlength="20">
							  <input id="nombre_articulo" type="text" class="form-control" placeholder="Descripción" autocomplete="off" maxlength="70">
							  <span id="limpiar-input-articulo" class="input-group-addon limpiar-input"><i class="fa fa-eraser"></i></span>
							</div>
					  	</div>

					  	<div class="form-group columna columna-6">
							<button class="btn boton_stock_articulo" disabled><span></span></button>
							<input type="hidden" class="form-control codigo_articulo_seleccionado" disabled>
							<input type="hidden" class="form-control stock_virtual_articulo" disabled>
							<input type="hidden" class="form-control stock_disponible_articulo" disabled>
							<input type="hidden" class="form-control pendiente_servir_articulo" disabled>
							<input type="hidden" class="form-control pendiente_recibir_articulo" disabled>
							<input type="hidden" class="form-control pendiente_fabricar_articulo" disabled>
					  	</div>

					  	<div class="form-group columna columna-6">
					    	<input type="text" class="form-control cantidad_articulo">
					  	</div>

					  	<div class="form-group columna columna-10">
					    	<div class="input-group">
							  <input type="text" class="form-control precio_articulo">
							  <span class="input-group-addon simbolo-input">€</span>
							</div>
					  	</div>

						<?php
						if ($tipo_usuario == "comisionista") $descuento_disabled = "";
						elseif ($tipo_usuario == "cliente") $descuento_disabled = "disabled";
						?>

						<div class="form-group columna columna-10">
							<div class="input-group">
							  <input type="text" class="form-control descuento_articulo" <?php echo $descuento_disabled; ?>>
							  <span class="input-group-addon simbolo-input">%</span>
							</div>
						</div>

					  	<div class="form-group columna columna-10">
					    	<div class="input-group">
							  <input type="text" class="form-control total_articulo" disabled>
							  <span class="input-group-addon simbolo-input">€</span>
							</div>
					  	</div>

						<input id="codigo_nombre_articulo" type="hidden">
					  	<input type="hidden" class="form-control neto_articulo" disabled>

					  	<div class="form-group columna columna-6 padding-right">
					    	<button id="add-linea" type="button" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
					  	</div>
				  	</div>

				  	<div id="titulos-columnas-lineas-abajo" class="row cols-menos-padding marg-top-10 titulos-columnas-lineas hidden">
				  		<div class="columna columna-6 padding-left">
				  			Prio.
				  		</div>
				  		<div class="columna columna-66">
				  			Artículo
				  		</div>
				  		<div class="columna columna-6">
				  			Uds.
				  		</div>
				  		<div class="columna columna-10">
				  			Precio
				  		</div>
				  		<div class="columna columna-6">
				  			Dto.
				  		</div>
					</div>

					<div id="ultima-fila" class="row">
						<div class="col-xs-12">
							<div class="recuadro-total-pedido">Total del pedido/presupuesto: <span id="total-pedido-html">0</span> €</div>
							<input type="hidden" id="total-pedido" value="0" />
						</div>
					</div>

				</div>

			</div>
			<button type="button" id="envio_prueba" class="hidden">Prueba</button>
			<div id='modal-clientes' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div id="tabla-usuarios" class="container gc-container container-propio">
						<div class="row">
							<div class="table-section">
								<div class="table-container">
									<table class="table table-bordered grocery-crud-table table-hover">
										<thead>
											<tr class="titulos-columnas">
												<th class="celda-titulo-columna"></th>
												<th class="celda-titulo-columna">Código</th>
												<th class="celda-titulo-columna">Nombre</th>
												<th class="celda-titulo-columna">Municipio</th>
											</tr>
											<tr class="filter-row gc-search-row">
												<td></td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Código" name="CodigoCliente">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Nombre" name="RazonSocial">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Municipio" name="Municipio">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
											</tr>
										</thead>
										<tbody class="listado-registros"></tbody>
									</table>
									<div class="footer-tools">
										<div class="floatR r10 t30">
											Mostrando <span id="total_clientes"></span> registros
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button class="boton-arriba-modal" type="button"><i class="fa fa-angle-double-up"></i></button>
				</div>
				<button type="button" class="btn btn-danger boton-cerrar-emergente boton-x-emergente">X</button>
			</div>

			<div id='modal-historico' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div id="tabla-historico" class="container gc-container container-propio">
						<div class="row">
							<div class="table-section">
								<div class="table-container">
									<table class="table table-bordered grocery-crud-table table-hover">
										<thead>
											<tr class="titulos-columnas">
												<th class="celda-titulo-columna"></th>
												<th class="celda-titulo-columna">Código</th>
												<th class="celda-titulo-columna">Descripción</th>
												<th class="celda-titulo-columna">Último pedido</th>
												<th class="celda-titulo-columna">Uds.</th>
												<th class="celda-titulo-columna">Precio</th>
												<th class="celda-titulo-columna">Dto.</th>
											</tr>
											<tr class="filter-row gc-search-row">
												<td></td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Código" name="a.CodigoArticulo">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Descripción" name="a.DescripcionArticulo">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL datepicker-input" type="text" placeholder="Fecha" name="ac.FechaUltimoAlbaran">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</thead>
										<tbody class="listado-registros"></tbody>
									</table>
									<div class="footer-tools">
										<div class="floatR r10 t30">
											Mostrando <span id="total_historico"></span> registros
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button class="boton-arriba-modal" type="button"><i class="fa fa-angle-double-up"></i></button>
				</div>
				<button type="button" class="btn btn-danger boton-cerrar-emergente boton-x-emergente">X</button>
			</div>

			<div id='modal-pendientes' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div id="tabla-pendientes" class="container gc-container container-propio">
						<div class="row">
							<div class="table-section">
								<div class="table-container">
									<table class="table table-bordered grocery-crud-table table-hover">
										<thead>
											<tr class="titulos-columnas">
												<th class="celda-titulo-columna"></th>
												<th class="celda-titulo-columna">Pedido</th>
												<th class="celda-titulo-columna">Fecha</th>
												<th class="celda-titulo-columna">Artículo</th>
												<th class="celda-titulo-columna">Uds.</th>
												<th class="celda-titulo-columna">Dto.</th>
											</tr>
											<tr class="filter-row gc-search-row">
												<td></td>
												<td></td>
												<td>
													<input class="form-control searchable-input floatL datepicker-input" type="text" placeholder="Fecha" name="lp.FechaPedido">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Artículo" name="a.DescripcionArticulo">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td></td>
												<td></td>
											</tr>
										</thead>
										<tbody class="listado-registros"></tbody>
									</table>
									<div class="footer-tools">
										<div class="floatR r10 t30">
											Mostrando <span id="total_pendientes"></span> registros
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button class="boton-arriba-modal" type="button"><i class="fa fa-angle-double-up"></i></button>
				</div>
				<button type="button" class="btn btn-danger boton-cerrar-emergente boton-x-emergente">X</button>
			</div>

			<div id='modal-articulos' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div id="tabla-articulos" class="container gc-container container-propio">
						<div class="row">
							<div class="table-section">
								<div class="table-container">
									<table class="table table-bordered grocery-crud-table table-hover">
										<thead>
											<tr class="titulos-columnas">
												<th class="celda-titulo-columna"></th>
												<th class="celda-titulo-columna">Código</th>
												<th class="celda-titulo-columna">Descripción</th>
												<th class="celda-titulo-columna">Precio</th>
												<th class="celda-titulo-columna">Stock</th>
											</tr>
											<tr class="filter-row gc-search-row">
												<td></td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Código" name="CodigoArticulo">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td>
													<input class="form-control searchable-input floatL" type="text" placeholder="Descripción" name="DescripcionArticulo">
													<i class="fa fa-times clear-search hidden"></i>
												</td>
												<td></td>
												<td></td>
											</tr>
										</thead>
										<tbody class="listado-registros"></tbody>
									</table>
									<div class="footer-tools">
										<button id="boton-cargar-mas-articulos" type='button' class='btn btn-info hidden'>Cargar más</button>
										<div class="floatR r10 t30">
											Mostrando <span id="articulos_cargados"></span> registros de <span id="total_articulos"></span>
										</div>
										<div class="clear"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<button class="boton-arriba-modal" type="button"><i class="fa fa-angle-double-up"></i></button>
				</div>
				<button type="button" class="btn btn-danger boton-cerrar-emergente boton-x-emergente">X</button>
			</div>

			<div id='modal-confirmar-envio' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div class='modal-content'>
						<div class='modal-header cabecera-emergente'>
							<button  type='button' class='close boton-cerrar-emergente'>&times;</button>
							<h3 class='modal-title titulo-emergente'>Enviar pedido / presupuesto</h3>
						</div>
						<div class='modal-body'>
							<div class="row">
								<div class="col-xs-12">
									<p>¿Estás seguro que deseas enviar el pedido / presupuesto?</p>
								</div>
								<?php
								if ($tipo_usuario == "comisionista") $checkbox_hidden = "";
								else $checkbox_hidden = "hidden";
								?>
								<div class="col-xs-12 checkbox <?php echo $checkbox_hidden; ?>">
									<label class="noselect"><input type="checkbox" id="checkbox-email" checked> Enviar email al cliente</label>
								</div>
							</div>
						</div>
						<div class='modal-footer'>
						  <button type='button' class='btn btn-danger boton-cerrar-emergente'>Cancelar</button>
					      <button type='button' class='btn btn-primary' id='confirmar-envio'>Enviar</button>
					    </div>
					</div>
				</div>
			</div>

			<div id='modal-campos-obligatorios' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div class='modal-content'>
						<div class='modal-header cabecera-emergente'>
							<button  type='button' class='close boton-cerrar-emergente'>&times;</button>
							<h3 class='modal-title titulo-emergente'>Campos obligatorios</h3>
						</div>
						<div class='modal-body'>
							<div class="row">
								<div class="col-xs-12">
									<p>Por favor, rellena todos los campos obligatorios, que tienen un asterísco rojo (<span class="required">*</span>)</p>
								</div>
							</div>
						</div>
						<div class='modal-footer'>
						  <button type='button' class='btn btn-primary boton-cerrar-emergente'>OK</button>
					    </div>
					</div>
				</div>
			</div>

			<div id='modal-stock-articulo' class='hidden pantalla-emergente'>
				<div class='modal-dialog cuadro-emergente'>
					<div class='modal-content'>
						<div class='modal-body'>
							<div class="row">
								<div class="col-xs-12">
									<table>
										<tr>
											<td>Artículo</td>
											<td id="modal-stock-codigo-articulo"></td>
										</tr>
										<tr>
											<td>Stock Virtual</td>
											<td id="modal-stock-virtual"></td>
										</tr>
										<tr>
											<td>Stock Disponible</td>
											<td id="modal-stock-disponible"></td>
										</tr>
										<tr>
											<td>Pendiente Servir</td>
											<td id="modal-stock-servir"></td>
										</tr>
										<tr>
											<td>Pendiente Recibir</td>
											<td id="modal-stock-recibir"></td>
										</tr>
										<tr>
											<td>Pendiente Fabricar</td>
											<td id="modal-stock-fabricar"></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class='modal-footer'>
						  <p class='modal-stock-estado'>
							<span id='modal-stock-bolita-estado'></span>
							<span id='modal-stock-texto-estado'></span>
						  </p>
						  <button type='button' class='btn btn-primary boton-cerrar-emergente'>Aceptar</button>
					    </div>
					</div>
				</div>
			</div>

		</div>

		<div id="mensaje-borrador" class="alert alert-success transparente hidden">
		  	<p>Se ha guardado como borrador <span id="numero-borrador">1</span></p>
		</div>

		<div class="div-cargando hidden">
			Cargando...
			<div class="fondo-cargando"></div>
		</div>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
