<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<!-- Inicio panel -->
		<div class="container gc-container container-propio">
			<div class="success-message hidden"></div>

			<div class="row">
				<div class="table-section">
					<div class="table-container">
						<form accept-charset="utf-8" id="gcrud-search-form" autocomplete="off" method="post" >
							<div class="header-tools">
								<div class="clear"></div>
							</div>
							<table class="table table-bordered grocery-crud-table table-hover">
								<thead>
									<tr class="titulos-columnas">
										<th colspan="2" class="celda-acciones"></th>
										<th data-order-by="codigoCliente" class="column-with-ordering celda-titulo-columna">Código</th>
										<th data-order-by="RazonSocial" class="column-with-ordering celda-titulo-columna">Nombre</th>
										<th data-order-by="Municipio" class="column-with-ordering celda-titulo-columna">Municipio</th>
										<th data-order-by="Telefono" class="column-with-ordering celda-titulo-columna">Teléfono</th>
									</tr>

									<tr class="filter-row gc-search-row">
										<td class="no-border-right "></td>
										<td class="no-border-left ">
										<div class="floatR l5">
											<a class="btn btn-info gc-refresh" href="javascript:void(0);"> <i class="fa fa-refresh"></i> </a>
										</div><div class="clear"></div></td>
										<td>
										<input type="text" name="CodigoCliente" placeholder="Código" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="RazonSocial" placeholder="Nombre" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="Municipio" placeholder="Municipio" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="Telefono" placeholder="Teléfono" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
									</tr>

								</thead>
								<tbody class="listado-registros">

									<?php foreach($registros_iniciales as $index => $registro): ?>

										<?php $odd_even = $index % 2 == 0 ? "odd" : "even"; ?>
										<?php $tarifa2 = $registro->tarifa == 2 ? "cliente-tarifa2" : ""; ?>

										<tr class="<?php echo $odd_even; ?> <?php echo $tarifa2; ?>">
											<td style="border-right: none;"></td>
											<td style="border-left: none;">
												<div style="white-space: nowrap" class="">
													<a href="<?php echo base_url(); ?>index.php/clientes/perfil/<?php echo $registro->codigo; ?>" class="btn btn-primary">Seleccionar</a>
												</div>
											</td>
											<td><?php echo $registro->codigo; ?></td>
											<td><?php echo $registro->nombre; ?></td>
											<?php if ($registro->municipio == "") $registro->municipio = "-"; ?>
											<td><?php echo $registro->municipio; ?></td>
											<?php if ($registro->telefono == "") $registro->telefono = "-"; ?>
											<td><?php echo $registro->telefono; ?></td>
										</tr>

									<?php endforeach; ?>

								</tbody>
							</table>
							<!-- Table Footer -->
							<div class="footer-tools">

								<!-- "Show 10/25/50/100 entries" (dropdown per-page) -->
								<div class="floatL t20 l5">
									<div class="floatL t10">
										Mostrar
									</div>
									<div class="floatL r5 l5 t3">
										<select class="per_page form-control" name="per_page">
											<option value="10"> 10&nbsp;&nbsp; </option>
											<option selected="selected" value="25"> 25&nbsp;&nbsp; </option>
											<option value="50"> 50&nbsp;&nbsp; </option>
											<option value="100"> 100&nbsp;&nbsp; </option>
										</select>
									</div>
									<div class="floatL t10">
										registros
									</div>
									<div class="clear"></div>
								</div>
								<!-- End of "Show 10/25/50/100 entries" (dropdown per-page) -->

								<div class="floatR r5">

									<!-- Buttons - First,Previous,Next,Last Page -->
									<ul class="pagination">
										<li class="disabled paging-first">
											<a href="#"><i class="fa fa-step-backward"></i></a>
										</li>
										<li class="prev disabled paging-previous">
											<a href="#"><i class="fa fa-chevron-left"></i></a>
										</li>
										<li>
											<span class="page-number-input-container">
												<input type="numeric" class="form-control page-number-input text-center" value="1">
											</span>
										</li>
										<li class="disabled next paging-next">
											<a href="#"><i class="fa fa-chevron-right"></i></a>
										</li>
										<li class="disabled paging-last">
											<a href="#"><i class="fa fa-step-forward"></i></a>
										</li>
									</ul>
									<!-- End of Buttons - First,Previous,Next,Last Page -->

									<input type="hidden" value="1" class="page-number-hidden" name="page_number">

									<!-- Start of: Settings button -->
									<button class="btn btn-default floatR t20 l10" type="button" id="clear-filtering">
										<span class="text-info"><i class="fa fa-eraser"></i> Limpiar filtro</span>
									</button>
									<!-- End of: Settings button -->

								</div>

								<!-- "Displaying 1 to 10 of 116 items" -->
								<div class="floatR r10 t30">
									Mostrando <span class="paging-starts">1</span> a <span class="paging-ends"><?php echo $cantidad_registros_iniciales; ?></span> de <span class="current-total-results"><?php echo $cantidad_registros_totales; ?></span> registros <span class="full-total-container hidden"> (filtrando de <span class="full-total"><?php echo $cantidad_registros_totales; ?></span> total registros) </span>
								</div>
								<!-- End of "Displaying 1 to 10 of 116 items" -->

								<div class="clear"></div>
							</div>
							<!-- End of: Table Footer -->

						</form>
					</div>
				</div>

			</div>

		</div>

		<div class="div-cargando hidden">
			Cargando...
			<div class="fondo-cargando"></div>
		</div>

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<!-- Fin panel -->

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
