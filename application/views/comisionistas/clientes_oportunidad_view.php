<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<div class="container container-propio titulo-cliente">
			<h4><?php echo trim($cliente->codigo)." - ".trim($cliente->nombre); ?></h4>
		</div>

		<!-- Inicio panel -->
		<div class="container container-vtiger container-propio">
			<div class="row">
				<div class="col-xs-12 pad0" >
					<?php $this->load->view("menu_cliente_view"); ?>
				</div>
				<div class="col-xs-12 pad0" >
					<div class="panel panel-primary">
						<form id="__vtigerWebForm" name="OPORTUNIDAD desde SIS" action="https://crm.abrastar.com:8443/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<div class="panel-heading"></div>
							<div class="panel-body">
								<div class="row">
									<?php if (!$cliente_vtiger): ?>
										<h3 class="text-center">Ha ocurrido un error.</h3><p class="text-center margin-bottom-15">Probablemente el cliente no esté dado de alta en vTiger o no tenga el mismo nombre exactamente.</p>
									<?php else: ?>
										<input type="hidden" name="__vtrftk" value="sid:1990a601d865d3d500ad30991fa679d7cffa267c,1498235904">
										<input type="hidden" name="publicid" value="d955d623c69c79f6e92b70c5df3807dd">
										<input type="hidden" name="urlencodeenable" value="1">
										<input type="hidden" name="name" value="OPORTUNIDAD desde SIS">
										<div class="form-group col-xs-12 col-sm-12">
											<label>Nombre Oportunidad<span class="required">*</span></label>
											<input type="text" name="potentialname" data-label="" value="" required="" class="form-control">
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Nombre de la organización</label>
											<input type="text" name="related_to_display" data-label="" value="<?php echo $cliente_vtiger->accountname; ?>" class="form-control" disabled>
											<input type="hidden" name="related_to" data-label="" value="11x<?php echo $cliente_vtiger->id; ?>" required="" class="form-control">
										</div>
										<?php /* LOS SELECTORES DINAMICOS SE DESACTIVARON
										<div class="form-group col-xs-12 col-sm-6">
											<label>Nombre de Contacto</label>
											<select name="contact_id" data-label="contact_id" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<?php foreach ($contactos_vtiger as $contacto): ?>
													<option value="12x<?php echo $contacto->id; ?>"><?php echo $contacto->firstname." ".$contacto->lastname; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										*/ ?>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Origen de Pre-Contacto</label>
											<select name="leadsource" data-label="leadsource" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Cold Call">Llamada fria</option>
												<option value="Existing Customer" selected>Cliente</option>
												<option value="Self Generated">Autogenerada</option>
												<option value="Employee">Empleado</option>
												<option value="Partner">Socio</option>
												<option value="Public Relations">Relaciones Públicas</option>
												<option value="Direct Mail">Mailing</option>
												<option value="Conference">Conferencia</option>
												<option value="Trade Show">Feria</option>
												<option value="Web Site">Sitio Web</option>
												<option value="Word of mouth">Boca a Boca</option>
												<option value="Other">Otro</option>
											</select>
										</div>
										<div class="form-group col-xs-12">
											<label>Descripción<span class="required">*</span></label>
											<textarea name="description" required="" class="form-control"></textarea>
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Importe</label>
											<input type="text" name="amount" data-label="" value="" class="form-control">
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Tipo</label>
											<select name="opportunity_type" data-label="opportunity_type" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Existing Business">Negocio Existente</option>
												<option value="New Business">Nuevo Negocio</option>
											</select>
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Fecha Estimada de Cierre<span class="required">*</span> (yyyy-mm-dd)</label>
											<input type="date" name="closingdate" data-label="" value="" required="" class="form-control">
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Fase de Venta<span class="required">*</span></label>
											<select name="sales_stage" data-label="sales_stage" required="" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Prospecting">Investigando</option>
												<option value="Qualification">Calificando</option>
												<option value="Needs Analysis">Necesita Análisis</option>
												<option value="Value Proposition">Propuesta de Evaluación</option>
												<option value="Id. Decision Makers">Identificando Quien Decide</option>
												<option value="Perception Analysis">Análisis</option>
												<option value="Proposal or Price Quote">Proposal or Price Quote</option>
												<option value="Negotiation or Review">Negotiation or Review</option>
												<option value="Closed Won">Cerrada-Ganada</option>
												<option value="Closed Lost">Cerrada-Perdida</option>
											</select>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="panel-footer">
								<?php if ($cliente_vtiger): ?>
									<input class="btn btn-primary floatR" type="submit" value="Enviar">
								<?php endif; ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin panel -->

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
