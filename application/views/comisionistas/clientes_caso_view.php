<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<div class="container container-propio titulo-cliente">
			<h4><?php echo trim($cliente->codigo)." - ".trim($cliente->nombre); ?></h4>
		</div>

		<!-- Inicio panel -->
		<div class="container container-vtiger container-propio">
			<div class="row" >
				<div class="col-xs-12 pad0" >
					<?php $this->load->view("menu_cliente_view"); ?>
				</div>
				<div class="col-xs-12 pad0" >
					<div class="panel panel-primary">
						<form id="__vtigerWebForm" name="CASOS desde SIS" action="https://crm.abrastar.com:8443/modules/Webforms/capture.php" method="post" accept-charset="utf-8" enctype="multipart/form-data">
							<div class="panel-heading"></div>
							<div class="panel-body">
								<div class="row">
									<?php if (!$cliente_vtiger): ?>
										<h3 class="text-center">Ha ocurrido un error.</h3><p class="text-center margin-bottom-15">Probablemente el cliente no esté dado de alta en vTiger o no tenga el mismo nombre exactamente.</p>
									<?php else: ?>
										<input type="hidden" name="__vtrftk" value="sid:8bcc77b8cd9a6d8b7b4b356a8984bd5068ae8bc6,1498459915">
										<input type="hidden" name="publicid" value="9228ac5634f8024d818a1777235d3bb8">
										<input type="hidden" name="urlencodeenable" value="1">
										<input type="hidden" name="name" value="CASOS desde SIS">
										<div class="form-group col-xs-12 col-sm-6">
											<label>Título<span class="required">*</span></label>
											<input type="text" name="ticket_title" data-label="" value="" required="" class="form-control">
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>En relación con</label>
											<input type="text" name="parent_id_display" data-label="" value="<?php echo $cliente_vtiger->accountname; ?>" class="form-control" disabled>
											<input type="hidden" name="parent_id" data-label="" value="11x<?php echo $cliente_vtiger->id; ?>" required="" class="form-control">
										</div>
										<?php /* LOS SELECTORES DINAMICOS SE DESACTIVARON
										<div class="form-group col-xs-12 col-sm-6">
											<label>Nombre de Contacto</label>
											<select name="contact_id" data-label="contact_id" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<?php foreach ($contactos_vtiger as $contacto): ?>
													<option value="12x<?php echo $contacto->id; ?>"><?php echo $contacto->firstname." ".$contacto->lastname; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group col-xs-12 col-sm-6">
											<label>Nombre Artículo</label>
											<select name="product_id" data-label="contact_id" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<?php foreach ($productos_vtiger as $producto): ?>
													<option value="14x<?php echo $producto->id; ?>"><?php echo $producto->productname; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										*/ ?>
										<div class="form-group col-xs-12">
											<label>Descripción</label>
											<textarea name="description" class="form-control"></textarea>
										</div>
										<div class="form-group col-xs-12 col-sm-4">
											<label>Prioridad<span class="required">*</span></label>
											<select name="ticketpriorities" data-label="ticketpriorities" required="" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Low">Bajo</option>
												<option value="Normal" selected="">Normal</option>
												<option value="High">Alta</option>
												<option value="Urgent">Urgente</option>
											</select>
										</div>
										<div class="form-group col-xs-12 col-sm-4">
											<label>Estado<span class="required">*</span></label>
											<select name="ticketstatus" data-label="ticketstatus" required="" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Open" selected="">Abierta</option>
												<option value="In Progress">En Progreso</option>
												<option value="Wait For Response">Esperando</option>
												<option value="Closed">Cerrada</option>
											</select>
										</div>
										<div class="form-group col-xs-12 col-sm-4">
											<label>Urgencia</label>
											<select name="ticketseverities" data-label="ticketseverities" class="form-control">
												<option value="">--Selecciona Valor--</option>
												<option value="Minor">Menor</option>
												<option value="Major">Mayor</option>
												<option value="Feature">Característica</option>
												<option value="Critical">Crítica</option>
											</select>
										</div>
										<div class="form-group col-xs-12">
											<label>Solución</label>
											<textarea name="solution" class="form-control"></textarea>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="panel-footer">
								<?php if ($cliente_vtiger): ?>
									<input class="btn btn-primary floatR" type="submit" value="Enviar">
								<?php endif; ?>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin panel -->

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
