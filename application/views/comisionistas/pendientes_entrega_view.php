<html>
	<head>
		
		<?php $this->load->view('head_view'); ?>
		
	</head>
	<body>
						
		<?php $this->load->view('menu_view'); ?>
		
		<!-- Inicio panel -->
		<div class="container gc-container container-propio">
			
			<div class="row">
				<div class="table-section">
					<div class="table-container">
						<form accept-charset="utf-8" id="gcrud-search-form" autocomplete="off" method="post">
							<div class="header-tools">
								<div class="clear"></div>
							</div>
							<table class="table table-bordered grocery-crud-table table-hover">
								<thead>
									<tr class="titulos-columnas">
										<th class="celda-titulo-columna">Pedido</th>
										<th class="celda-titulo-columna">Fecha</th>
										<th class="celda-titulo-columna">Cód. Artículo</th>
										<th class="celda-titulo-columna">Descripción</th>
										<th class="celda-titulo-columna">Uds. Pedidas</th>
										<th class="celda-titulo-columna">Uds. Pendientes</th>
										<th class="celda-titulo-columna">Estado</th>
									</tr>
								
								</thead>
								<tbody class="listado-registros">
									
									<?php if (count ($pedidos) > 0): ?>
									
										<?php 
										$codigoCliente = $pedidos[0]->codigoCliente; 
										$nombreCliente = $pedidos[0]->nombreCliente; 
										echo "<tr class='fila-cliente'><td colspan='7'>".$codigoCliente." - ".$nombreCliente."</td></tr>";
										?>
	
										<?php foreach($pedidos as $index => $pedido): ?>
											
											<?php 
											if ($pedido->codigoCliente != $codigoCliente){
												$codigoCliente = $pedido->codigoCliente; 
												$nombreCliente = $pedido->nombreCliente; 
												echo "<tr class='fila-cliente'><td colspan='7'>".$codigoCliente." - ".$nombreCliente."</td></tr>";
											}
											?>
												
											
											<?php $odd_even = $index % 2 == 0 ? "odd" : "even"; ?>
											
											<?php 
											switch ($pedido->estado) {
												case "P": $estado = "Pendiente"; break;
												case "T": $estado = "Terminación"; break;
												case "E": $estado = "Embalaje"; break;
												case "I": $estado = "Preparación"; break;
											} 
											?>
											
											<tr class="<?php echo $odd_even; ?>">
												<td><?php echo $pedido->pedido; ?></td>
												<td><?php echo fechaToString($pedido->fecha); ?></td>
												<td><?php echo $pedido->codigoArticulo; ?></td>
												<?php if ($pedido->descripcionArticulo == "") $pedido->descripcionArticulo = "-"; ?>
												<td><?php echo $pedido->descripcionArticulo; ?></td>
												<td><?php echo round($pedido->unidadesPedidas, 2); ?></td>
												<!--<td><?php echo round($pedido->unidadesServidas, 2); ?></td>-->
												<td><?php echo round($pedido->unidadesPendientes, 2); ?></td>
												<td><?php echo $estado; ?></td>
											</tr>
											
										<?php endforeach; ?>
										
									<?php else: ?>
										
										<tr><td colspan="7"> No existen pedidos pendientes de entrega actualmente</tr>
										
									<?php endif; ?>

								</tbody>
							</table>
							<!-- Table Footer -->
							<div class="footer-tools">

								<!-- "Displaying 1 to 10 of 116 items" -->
								<div class="floatR r10 t30">
									Mostrando <?php echo $cantidadRegistros; ?> registros
								</div>
								<!-- End of "Displaying 1 to 10 of 116 items" -->

								<div class="clear"></div>
							</div>
							<!-- End of: Table Footer -->

						</form>
					</div>
				</div>

			</div>
			
		</div>
			
		<div class="div-cargando hidden">
			Cargando...
			<div class="fondo-cargando"></div>
		</div>
		
		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>
		
		<!-- Fin panel -->
	
		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>