<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<div class="container container-propio titulo-cliente">
			<h4><?php echo trim($cliente->codigo)." - ".trim($cliente->nombre); ?></h4>
		</div>

		<!-- Inicio panel -->
		<div class="container gc-container container-propio">

			<div class="row">
				<div class="col-xs-12 pad0" >
					<?php $this->load->view("menu_cliente_view"); ?>
				</div>
				<div class="table-section">
					<div class="table-container">
						<form accept-charset="utf-8" id="gcrud-search-form" autocomplete="off" method="post">
							<div class="header-tools">
								<div class="clear"></div>
							</div>
							<table class="table table-bordered grocery-crud-table table-hover">
								<thead>
									<tr class="titulos-columnas">
										<th data-order-by="ac.CodigoArticulo" class="column-with-ordering celda-titulo-columna">Código</th>
										<th data-order-by="a.DescripcionArticulo" class="column-with-ordering celda-titulo-columna">Descripción</th>
										<th data-order-by="FechaPrimerAlbaran" class="column-with-ordering celda-titulo-columna">1<sup>er</sup> albarán</th>
										<th data-order-by="FechaUltimoAlbaran" class="column-with-ordering celda-titulo-columna">Últ. albarán</th>
										<th data-order-by="UnidadesUltimoAlbaran" class="column-with-ordering celda-titulo-columna">Unidades</th>
									</tr>

									<tr class="filter-row gc-search-row">
										<td>
										<input type="text" name="ac.CodigoArticulo" placeholder="Código" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="a.DescripcionArticulo" placeholder="Descripción" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="FechaPrimerAlbaran" placeholder="1er albarán" class="form-control searchable-input floatL datepicker-input"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="FechaUltimoAlbaran" placeholder="Últ. albarán" class="form-control searchable-input floatL datepicker-input"><i class='fa fa-times clear-search hidden'></i>
										</td>
										<td>
										<input type="text" name="UnidadesUltimoAlbaran" placeholder="Unidades" class="form-control searchable-input floatL"><i class='fa fa-times clear-search hidden'></i>
										</td>
									</tr>

								</thead>
								<tbody class="listado-registros">

									<?php foreach($registros_iniciales as $index => $registro): ?>

										<?php $odd_even = $index % 2 == 0 ? "odd" : "even"; ?>

										<tr class="<?php echo $odd_even; ?>">
											<td><?php echo $registro->codigo; ?></td>
											<?php if ($registro->descripcion == "") $registro->descripcion = "-"; ?>
											<td><?php echo $registro->descripcion; ?></td>
											<td><?php echo $registro->fechaPrimer; ?></td>
											<td><?php echo $registro->fechaUltimo; ?></td>
											<td><?php echo round($registro->unidadesUltimo, 2); ?></td>
										</tr>

									<?php endforeach; ?>

								</tbody>
							</table>
							<!-- Table Footer -->
							<div class="footer-tools">

								<!-- "Show 10/25/50/100 entries" (dropdown per-page) -->
								<div class="floatL t20 l5">
									<div class="floatL t10">
										Mostrar
									</div>
									<div class="floatL r5 l5 t3">
										<select class="per_page form-control" name="per_page">
											<option value="10"> 10&nbsp;&nbsp; </option>
											<option selected="selected" value="25"> 25&nbsp;&nbsp; </option>
											<option value="50"> 50&nbsp;&nbsp; </option>
											<option value="100"> 100&nbsp;&nbsp; </option>
										</select>
									</div>
									<div class="floatL t10">
										registros
									</div>
									<div class="clear"></div>
								</div>
								<!-- End of "Show 10/25/50/100 entries" (dropdown per-page) -->

								<div class="floatR r5">

									<!-- Buttons - First,Previous,Next,Last Page -->
									<ul class="pagination">
										<li class="disabled paging-first">
											<a href="#"><i class="fa fa-step-backward"></i></a>
										</li>
										<li class="prev disabled paging-previous">
											<a href="#"><i class="fa fa-chevron-left"></i></a>
										</li>
										<li>
											<span class="page-number-input-container">
												<input type="numeric" class="form-control page-number-input text-center" value="1">
											</span>
										</li>
										<li class="disabled next paging-next">
											<a href="#"><i class="fa fa-chevron-right"></i></a>
										</li>
										<li class="disabled paging-last">
											<a href="#"><i class="fa fa-step-forward"></i></a>
										</li>
									</ul>
									<!-- End of Buttons - First,Previous,Next,Last Page -->

									<input type="hidden" value="1" class="page-number-hidden" name="page_number">

									<!-- Start of: Settings button -->
									<button class="btn btn-default floatR t20 l10" type="button" id="clear-filtering">
										<span class="text-info"><i class="fa fa-eraser"></i> Limpiar filtro</span>
									</button>
									<!-- End of: Settings button -->

								</div>

								<!-- "Displaying 1 to 10 of 116 items" -->
								<div class="floatR r10 t30">
									<?php $registro_inicial = $cantidad_registros_iniciales > 1 ? 1 : 0; ?>
									Mostrando <span class="paging-starts"><?php echo $registro_inicial; ?></span> a <span class="paging-ends"><?php echo $cantidad_registros_iniciales; ?></span> de <span class="current-total-results"><?php echo $cantidad_registros_totales; ?></span> registros <span class="full-total-container hidden"> (filtrando de <span class="full-total"><?php echo $cantidad_registros_totales; ?></span> total registros) </span>
								</div>
								<!-- End of "Displaying 1 to 10 of 116 items" -->

								<div class="clear"></div>
							</div>
							<!-- End of: Table Footer -->

							<input type="hidden" name="codigoCliente" id="codigoCliente" value="<?php echo $cliente->codigo; ?>">

						</form>
					</div>
				</div>

			</div>

		</div>

		<div class="div-cargando hidden">
			Cargando...
			<div class="fondo-cargando"></div>
		</div>

		<!-- Fin panel -->

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
