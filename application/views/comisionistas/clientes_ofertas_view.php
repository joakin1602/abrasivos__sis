<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<div class="container container-propio titulo-cliente">
			<h4><?php echo trim($cliente->codigo)." - ".trim($cliente->nombre); ?></h4>
		</div>

		<!-- Inicio panel -->
		<div class="container container-perfil container-propio">
			<div class="row">
				<div class="col-xs-12 pad0" >
					<?php $this->load->view("menu_cliente_view"); ?>
				</div>
				<div class="col-xs-12 pad0" >
					<div class="panel panel-primary">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<div class="row">
								<?php if (count($ofertas)): ?>
									<div class="col-xs-12">
										<div class="contenedor-botones-ofertas">
											<button id="desplegar-ofertas" class="btn btn-primary">Desplegar todas las ofertas</button>
											<button id="plegar-ofertas" class="btn btn-primary">Plegar todas las ofertas</button>
										</div>
									</div>
									<?php foreach ($ofertas as $oferta): ?>
										<div class="col-xs-12">
											<div class="oferta plegada">
												<div class="cabecera-oferta">
													<h4>
														<span class="codigo-oferta">
															OFERTA
															<?php echo $oferta->serie."/".$oferta->numero; ?>
														</span>
														<span class="fecha-oferta">
															(<?php echo $oferta->fecha; ?>)
														</span>
														<span class="texto-oferta">
															<?php echo $oferta->texto_primera_linea; ?>
														</span>
												</h4>
													<i class="fa fa-angle-down icono-flecha"></i>
												</div>
												<div class="lineas-oferta" style="height:0px">
													<table>
														<tr>
															<th>Cód. Artículo</th>
															<th>Descripción</th>
															<th>Uds. Pedidas</th>
															<th>Precio</th>
															<th>Descuento</th>
														</tr>
														<?php foreach ($oferta->lineas as $linea): ?>
															<tr>
																<td class="codigo-articulo"><?php echo $linea->codigoArticulo; ?></td>
																<td class="descripcion-articulo"><?php echo $linea->descripcionArticulo; ?></td>
																<td class="unidades-pedidas"><?php echo $linea->unidadesPedidas; ?> uds.</td>
																<td class="precio"><?php echo $linea->precio; ?> €</td>
																<td class="descuento"><?php echo $linea->descuento; ?> %</td>
															</tr>
														<?php endforeach; ?>
													</table>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								<?php else: ?>
									<div class="col-xs-12">
										<p class="text-center">No existen ofertas en el historial del cliente</p>
									</div>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Fin panel -->

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
