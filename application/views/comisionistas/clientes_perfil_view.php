<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<div class="container container-propio titulo-cliente">
			<h4><?php echo trim($cliente->codigo)." - ".trim($cliente->nombre); ?></h4>
		</div>

		<!-- Inicio panel -->
		<div class="container container-perfil container-propio">
			<div class="row">
				<div class="col-xs-12 pad0" >
					<?php $this->load->view("menu_cliente_view"); ?>
				</div>
				<div class="col-xs-12 pad0" >
					<div class="panel panel-primary">
						<div class="panel-heading"></div>
						<div class="panel-body">
							<div class="row">
								<div class=" col-md-12 col-lg-12">
									<?php if ($cliente->BloqueoPedido == -1): ?>
										<div class="alert alert-danger pedidos-bloqueados">
											<h4><i class="fa fa-warning"></i> El cliente tiene los pedidos bloqueados</h4>
										</div>
									<?php endif; ?>
									<table class="table table-user-information">
										<tbody>
											<tr>
												<td><strong>Código</strong></td>
												<td><?php echo $cliente->codigo; ?></td>
											</tr>
											<tr>
												<td><strong>Nombre</strong></td>
												<td><?php echo $cliente->nombre; ?></td>
											</tr>
											<tr>
												<td><strong>Domicilio</strong></td>
												<td><?php echo $cliente->domicilio; ?></td>
											</tr>
											<tr>
											<tr>
												<td><strong>Municipio</strong></td>
												<td><?php echo $cliente->municipio.", ".$cliente->cp; ?></td>
											</tr>
											<tr>
												<td><strong>Provincia</strong></td>
												<td><?php echo $cliente->provincia; ?></td>
											</tr>
											<tr>
												<td><strong>Email</strong></td>
												<td><?php echo $cliente->email; ?></td>
											</tr>
											<tr>
												<td><strong>Teléfono</strong></td>
												<td><?php echo $cliente->telefono; ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="panel-footer">
							<a data-original-title="Enviar correo" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary" href="mailto:<?php echo $cliente->email; ?>" target="_blank"><i class="fa fa-envelope"></i></a>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- Fin panel -->

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
