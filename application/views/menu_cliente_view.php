<ul class="nav nav-tabs">
  <li <?php if ($menu_cliente == "perfil"): ?> class="active" <?php endif;?>>
    <a href="/index.php/clientes/perfil/<?php echo $cliente->codigo; ?>">Perfil</a>
  </li>
  <li <?php if ($menu_cliente == "historico"): ?> class="active" <?php endif;?>>
    <a href="/index.php/clientes/historico/<?php echo $cliente->codigo; ?>">Histórico</a>
  </li>
  <li <?php if ($menu_cliente == "pendientes_entrega"): ?> class="active" <?php endif;?>>
    <a href="/index.php/clientes/pendientes_entrega/<?php echo $cliente->codigo; ?>">Pendientes</a>
  </li>
  <li <?php if ($menu_cliente == "albaranes_reserva"): ?> class="active" <?php endif;?>>
    <a href="/index.php/clientes/albaranes_reserva/<?php echo $cliente->codigo; ?>">Albaranes</a>
  </li>
  <li <?php if ($menu_cliente == "ofertas"): ?> class="active" <?php endif;?>>
    <a href="/index.php/clientes/ofertas/<?php echo $cliente->codigo; ?>">Ofertas</a>
  </li>
  <?php if ($cliente_vtiger): ?>
    <li <?php if ($menu_cliente == "oportunidad"): ?> class="active" <?php endif;?>>
      <a href="/index.php/clientes/oportunidad/<?php echo $cliente->codigo; ?>">Oportunidad</a>
    </li>
    <li <?php if ($menu_cliente == "caso"): ?> class="active" <?php endif;?>>
      <a href="/index.php/clientes/caso/<?php echo $cliente->codigo; ?>">Caso</a>
    </li>
  <?php endif; ?>
  <?php if ($cliente->BloqueoPedido != -1 && $this->session->tipo_usuario == 'comisionista'): ?>
    <li class="pedido-presupuesto"><a href="/index.php/pedidos_presupuestos/nuevo/cliente/<?php echo $cliente->codigo; ?>">Pedido/Presu.</a></li>
  <?php endif; ?>
</ul>
