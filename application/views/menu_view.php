<?php

	// Nombre del apartado del menu
	$menu = isset($menu) ? $menu : "";

	// Obtenemos el nombre que aparecera en la esquina superior derecha, del comisionista o del cliente
	if ($this->session->tipo_usuario == 'admin') $nombre = "Admin";

	elseif ($this->session->tipo_usuario == 'comisionista') $nombre = ucwords(strtolower(trim($this->session->nombre_comisionista)));

	elseif ($this->session->tipo_usuario == 'cliente') $nombre = nombre_empresa_ucwords($this->session->nombre_cliente);

?>

<div id='cssmenu'>
	<ul>
	   <li class="logo-menu"><img src="<?php echo base_url(); ?>content/images/logo.png"></li>
	   <li><a class="<?php if ($menu == "inicio") echo "active" ?>" href='<?php echo base_url(); ?>'><span>Inicio</span></a></li>
	   <li><a class="<?php if ($menu == "articulos") echo "active" ?>" href='<?php echo base_url(); ?>index.php/articulos'><span>Artículos</span></a></li>
	   <?php if ($this->session->tipo_usuario != 'cliente'): ?>
	   <li><a class="<?php if ($menu == "clientes") echo "active" ?>" href='<?php echo base_url(); ?>index.php/clientes'><span>Clientes</span></a></li>
	   <?php endif; ?>
	   <?php if ($this->session->tipo_usuario == 'cliente'): ?>
	   <li><a class="<?php if ($menu == "historico") echo "active" ?>" href='<?php echo base_url(); ?>index.php/historico'><span>Histórico</span></a></li>
	   <?php endif; ?>
	   <li><a class="<?php if ($menu == "pendientes_entrega") echo "active" ?>" href='<?php echo base_url(); ?>index.php/pendientes_entrega'><span>Pendientes ent.</span></a></li>
		 <?php if ($this->session->tipo_usuario != 'cliente'): ?>
		 <li><a class="<?php if ($menu == "precontacto") echo "active" ?>" href='<?php echo base_url(); ?>index.php/precontacto'><span>Precontacto</span></a></li>
	   <?php endif; ?>
	   <?php if ($this->session->tipo_usuario != 'admin'): ?>
	   <li><a class="<?php if ($menu == "pedidos") echo "active" ?>" href='<?php echo base_url(); ?>index.php/pedidos_presupuestos'><span>Pedidos/presup.</span></a></li>
	   <?php endif; ?>
		 <li class='has-sub usuario-menu'><a href='#'><span><?php echo $nombre; ?></span></a>
	      <ul class='submenu'>
					<?php if ($this->session->tipo_usuario == 'admin'): ?>
 				 	<li><a class="<?php if ($menu == "keys_login") echo "active" ?>" href='<?php echo base_url(); ?>index.php/keys_login'><span>Keys Login</span></a></li>
 				 	<li><a class="<?php if ($menu == "configuracion") echo "active" ?>" href='<?php echo base_url(); ?>index.php/configuracion'><span>Configuración</span></a></li>
					<li><a class="<?php if ($menu == "importador_pedidos") echo "active" ?>" href='<?php echo base_url(); ?>index.php/importador_pedidos'><span>Importador de pedidos</span></a></li>
 			 	 	<?php endif; ?>
	   		 	<li><a href='<?php echo base_url(); ?>index.php/login/cerrar_sesion'><span>Cerrar sesión</a></li>
	   	  </ul>
	   </li>
	</ul>
</div>
