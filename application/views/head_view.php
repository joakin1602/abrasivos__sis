<meta charset="UTF-8">
<title>Abrasivos<?php if (isset($title)) echo " - ".$title; ?></title>
<link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>content/images/favicon.ico"/>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/bootstrap/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/font-awesome/css/font-awesome.min.css" />

<?php if (isset($css_panel) && $css_panel): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/panel/common.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/panel/list.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/panel/general.css" />
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/panel/plugins/animate.min.css" />
<?php endif; ?>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/menu.css?v=1.00" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/main.css?v=1.02" />

<?php if (isset($css_postmain)): ?>
	<?php foreach ($css_postmain as $css_postmain_file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>content/css/<?php echo $css_postmain_file; ?>" />
	<?php endforeach; ?>
<?php endif; ?>
