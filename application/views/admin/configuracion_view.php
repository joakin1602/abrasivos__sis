<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container marg-top-15' id='configuracion'>
			<h2>Configuración</h2>
			<hr>
			<div class="row margin-bottom-15">
				<div class="col-xs-12">
					<label>Emails de envío de los pedidos<span class="required">*</span> <span class="no-negrita">(si es más de uno, separar con comas)</span></label>
					<input type="text" id="emails-envio" class="form-control obligatorio" value="<?php echo $configuracion->emails_envio; ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<button type="button" id="guardar-configuracion" class="btn btn-primary pull-right">Guardar</button>
				</div>
			</div>
		</div>

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
