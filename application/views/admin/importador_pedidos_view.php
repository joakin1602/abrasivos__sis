<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container marg-top-15' id="importador-pedidos">
      <h2>Importador de pedidos</h2>
			<hr>
      <?php if ($mensaje == 1): ?>
        <p>El pedido se han importado correctamente.</p>
        <a href="/index.php/importador_pedidos"><button type="button" class="btn btn-primary">Volver</button></a>
      <?php elseif ($mensaje == -1): ?>
        <p>Los datos del pedido son incorrectos.</p>
        <a href="/index.php/importador_pedidos"><button type="button" class="btn btn-primary">Volver</button></a>
      <?php elseif ($mensaje == -2): ?>
        <p>Ha ocurrido un error durante la importación.</p>
        <a href="/index.php/importador_pedidos"><button type="button" class="btn btn-primary">Volver</button></a>
      <?php else: ?>
        <form action="/index.php/importador_pedidos" method="post" enctype="multipart/form-data">
    			<div class="row margin-bottom-15">
    				<div class="col-xs-6">
    					<label>Cliente</label>
    					<select name="cliente" class="form-control" required>
                <option value="2955">WURTH ESPAÑA, S.A.</option>
              </select>
    				</div>
            <div class="col-xs-6">
              <label>Fichero</label>
              <input type="file" name="fichero" required>
            </div>
          </div>
          <div class="row margin-bottom-15">
    				<div class="col-xs-12">
    					<button type="submit" class="btn btn-primary">Importar</button>
    				</div>
    			</div>
        </form>
      <?php endif; ?>
    </div>

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
