<html>
	<head>

		<?php $this->load->view('head_view'); ?>

	</head>
	<body>

		<?php $this->load->view('menu_view'); ?>

		<!-- Contenido -->
		<div class='container gc-container container-propio marg-top-15'>
      <form id="nueva-key" action=".">
  			<div class='row'>
  				<div class="col-xs-3">
            <label for="key-login">Key<span class="required">*</span></label>
            <div class="input-group form-group">
              <input type="text" id="key-login" class="form-control">
              <span class="input-group-addon" id="generar-key"><i class="fa fa-random"></i></span>
            </div>
          </div>
          <div class="col-xs-3">
            <label for="tipo-usuario">Tipo de usuario<span class="required">*</span></label>
            <div class="form-group">
              <select id="tipo-usuario" class="form-control">
                <option value=""></option>
                <?php /* <option value="admin">Admin</option> */ ?>
                <option value="comisionista">Comercial</option>
                <option value="cliente">Cliente</option>
              </select>
            </div>
          </div>
          <div class="col-xs-3">
            <label for="key-login">Usuario<span class="required">*</span></label>
            <div class="form-group">
              <select id="id_usuario" class="form-control">
                <option value=""></option>
              </select>
            </div>
          </div>
          <div class="col-xs-3">
            <label for="descripcion">Descripción</label>
            <div class="form-group">
              <input type="text" id="descripcion" class="form-control">
            </div>
          </div>
  			</div>

        <div class='row'>
          <div class="col-xs-12">
            <button type="submit" class="btn btn-primary floatR">Añadir</button>
          </div>
        </div>
      </form>

      <div class='row margin-bottom-15'>
        <div class="col-xs-12">
          <h3 class="subrayado-azul">Listado de keys</h3>
        </div>
      </div>

      <div id='listado-keys' class='row'>
        <div class="table-section col-xs-12">
          <div class="table-container">
            <table class="table table-bordered grocery-crud-table table-hover">
              <thead>
                <tr class="titulos-columnas">
                  <th class="celda-titulo-columna"></th>
                  <th class="celda-titulo-columna">Key</th>
                  <th class="celda-titulo-columna">Tipo usuario</th>
                  <th class="celda-titulo-columna">Usuario</th>
                  <th class="celda-titulo-columna">Descripcion</th>
                  <th class="celda-titulo-columna">Último Acceso</th>
                </tr>
              </thead>
              <tbody class="listado-registros">
                <?php $odd_even = "even"; ?>
                <?php foreach ($keys as $key): ?>
                  <?php $odd_even = $odd_even == "odd" ? "even" : "odd"; ?>
                  <tr class="<?php echo $odd_even; ?>">
                    <td>
                      <button class="btn btn-danger borrar-key"><i class="fa fa-trash"></i></button>
                      <input type="hidden" class="id-key" value="<?php echo $key->id; ?>">
                    </td>
                    <td><?php echo $key->key; ?></td>
                    <td><?php echo $key->tipo_usuario; ?></td>
                    <td><?php echo $key->id_usuario; ?></td>
                    <td><?php echo $key->descripcion; ?></td>
                    <td><?php echo $key->ultimo_acceso; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
            <div class="footer-tools">
								<div class="floatR r10 t30">
									Mostrando <?php echo count($keys); ?> registros
								</div>
								<div class="clear"></div>
							</div>
          </div>
        </div>
      </div>

		</div>

    <div class="datos-js">
      <input type="hidden" id="comisionistas" value='<?php echo json_encode($comisionistas); ?>'>
      <input type="hidden" id="clientes" value='<?php echo json_encode($clientes); ?>'>
    </div>

		<button class="boton-arriba-pagina" type="button"><i class="fa fa-angle-double-up"></i></button>

		<?php $this->load->view('scripts_view'); ?>

	</body>
</html>
