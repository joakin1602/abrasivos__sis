<script src='<?php echo base_url(); ?>content/js/jquery-1.11.1.min.js'></script>
<script src='<?php echo base_url(); ?>content/js/bootstrap.min.js'></script>

<?php if (isset($js_premain)): ?>
<?php foreach ($js_premain as $js_premain_file): ?> 	
<script src='<?php echo base_url(); ?>content/js/<?php echo $js_premain_file; ?>'></script>	
<?php endforeach; ?>
<?php endif; ?>

<script src='<?php echo base_url(); ?>content/js/main.js?v=1.00'></script>

<?php if (isset($js_postmain)): ?>
<?php foreach ($js_postmain as $js_postmain_file): ?> 	
<script src='<?php echo base_url(); ?>content/js/<?php echo $js_postmain_file; ?>'></script>	
<?php endforeach; ?>
<?php endif; ?>