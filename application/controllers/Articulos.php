<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Articulos extends CI_Controller {

	function __construct() {

		parent::__construct();

	}

	function index() {

		// Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada();

		$output = new stdClass();
		$output->menu = "articulos";
		$output->title = "Artículos";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("articulos.js");

		// Si es cliente
		if ($this->session->tipo_usuario == 'cliente') {

			// Marcamos que solo se obtendran los articulos para los clientes
			$articulos_cliente = true;

			// Obtenemos los datos del cliente
			$cliente = $this->Propio_model->obtenerDatosCliente($this->session->cliente);

			// Definimos la tarifa a partir de los datos del comprobarUsuarioClienteCorrecto
			$tarifa = $cliente->tarifa;

		}
		// Si no es cliente
		else {

			$articulos_cliente = false;
			$tarifa = 2;

		}

		$output->registros_iniciales = $this->Propio_model->obtenerArticulosInicial($articulos_cliente, $tarifa);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadArticulosTotales($articulos_cliente);

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		// Nos recorremos los articulos para calcular los datos del stockDisponible
		foreach ($output->registros_iniciales as $index => $registro) {

			// Redondeamos los valores del stock
			$stock_virtual = round_to_2dp($registro->stockVirtual);
			$stock_disponible = round_to_2dp($registro->stockDisponible);
			$pendiente_servir = round_to_2dp($registro->pendienteServir);
			$pendiente_recibir = round_to_2dp($registro->pendienteRecibir);
			$pendiente_fabricar = round_to_2dp($registro->pendienteFabricar);

			// Obtenemos el boton del stock
			$boton_stock = obtener_boton_stock ($stock_virtual, $stock_disponible, $pendiente_servir, $pendiente_recibir, $pendiente_fabricar);

			$output->registros_iniciales[$index]->boton_stock = $boton_stock;

		}

		$this->load->view('general/articulos_view',$output);

	}

	function obtener_otros_articulos () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && $this->session->login) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
      $columnaOrdenacion = $_POST["columna_ordenacion"];

			// Si es cliente
			if ($this->session->tipo_usuario == 'cliente') {

				// Marcamos que solo se obtendran los articulos para los clientes
				$articulos_cliente = true;

				// Obtenemos los datos del cliente
				$cliente = $this->Propio_model->obtenerDatosCliente($this->session->cliente);

				// Definimos la tarifa a partir de los datos del comprobarUsuarioClienteCorrecto
				$tarifa = $cliente->tarifa;

			}
			// Si no es cliente
			else {

				$articulos_cliente = false;
				$tarifa = 2;

			}

			// Obtenemos los registros y el número de registros para ese filtro
			$registros = $this->Propio_model->obtenerOtrosArticulos($inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion, $articulos_cliente, $tarifa);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosArticulos($filtros, $articulos_cliente);

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				if ($registro->descripcion == "") $registro->descripcion = "-";

				// Redondeamos los valores del stock
				$stock_virtual = round_to_2dp($registro->stockVirtual);
				$stock_disponible = round_to_2dp($registro->stockDisponible);
				$pendiente_servir = round_to_2dp($registro->pendienteServir);
				$pendiente_recibir = round_to_2dp($registro->pendienteRecibir);
				$pendiente_fabricar = round_to_2dp($registro->pendienteFabricar);

				// Obtenemos el boton del stock
				$boton_stock = obtener_boton_stock ($stock_virtual, $stock_disponible, $pendiente_servir, $pendiente_recibir, $pendiente_fabricar);

				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->codigo."</td>".
								"<td>".$registro->descripcion."</td>".
								"<td>".round($registro->precio, 3)." €</td>".
								"<td>".
								"<button type='button' class='btn ".$boton_stock." boton-stock-articulo'>".$stock_virtual."</button>".
								"<input type='hidden' class='stock_codigo_articulo' value='".$registro->codigo."'>".
								"<input type='hidden' class='stock_virtual_articulo' value='".$stock_virtual."'>".
								"<input type='hidden' class='stock_disponible_articulo' value='".$stock_disponible."'>".
								"<input type='hidden' class='pendiente_servir_articulo' value='".$pendiente_servir."'>".
								"<input type='hidden' class='pendiente_recibir_articulo' value='".$pendiente_recibir."'>".
								"<input type='hidden' class='pendiente_fabricar_articulo' value='".$pendiente_fabricar."'>".
								"</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

}
