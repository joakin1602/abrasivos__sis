<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pedidos_presupuestos extends CI_Controller {

	function index() {

		// Si no hay sesion requerida iniciada, mandarlo al login
		comprobarSesionIniciada(Array('cliente','comisionista'));

		$output = new stdClass();
		$output->menu = "pedidos";
		$output->title = "Pedidos / presupuestos";
		$output->css_postmain = Array("pedidos_presupuestos.css");
		$output->js_postmain = Array("pedidos_presupuestos.js");

		// Definimos el codigo_usuario, que sera el codigo del cliente o del comisionista que este logeado, asi como el tipo_usuario
		if ($this->session->tipo_usuario == 'comisionista') {
			$output->codigo_usuario = $this->session->comisionista;
			$output->tipo_usuario = "comisionista";
		}
		elseif ($this->session->tipo_usuario == 'cliente') {
			$output->codigo_usuario = $this->session->cliente;
			$output->tipo_usuario = "cliente";
		}

		$this->load->view('general/pedidos_presupuestos_view',$output);

	}

	function nuevo ($elemento="", $codigo="") {

		// Si no hay sesion requerida iniciada, mandarlo al login
		comprobarSesionIniciada(Array('cliente','comisionista'));

		$output = new stdClass();
		$output->menu = "pedidos";
		$output->title = "Nuevo pedido / presupuesto";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css","formulario_pedidos_presupuestos.css?v=1.00");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("formulario_pedido_presupuesto.js?v=1.00");

		// Comprobamos si esta accediendo un comisionista o un cliente
		if ($this->session->tipo_usuario == 'comisionista') {

			// Definimos los datos del usuario
			$output->codigo_comisionista = $this->session->comisionista;
			$output->tipo_usuario = "comisionista";
			$output->cliente = false;

		}
		elseif ($this->session->tipo_usuario == 'cliente') {

			// Definimos el codigo del comisionista como 0, que indicara que ha sido el cliente el que ha hecho el pedido
			$output->codigo_comisionista = 0;

			// Definimos los datos del usuario
			$output->tipo_usuario = "cliente";
			$output->codigo_cliente = $this->session->cliente;
			$output->cliente = true;

			// Obtenemos los datos del cliente
			$datosCliente = $this->Propio_model->obtenerDatosCliente($this->session->cliente, 0);

			if ($datosCliente == false) {
				header("Location: ".base_url()."index.php/pedidos_presupuestos/nuevo/");
				die;
			}
			else {
				$output->nombre_cliente = trim($datosCliente->nombre);
				$output->email_cliente = trim($datosCliente->email);
				$output->tarifa_cliente = trim($datosCliente->tarifa);
			}

		}

		$output->numero = obtenerNumeroFormularioPedidos();

		// Si se intenta acceder a un borrador
		if ($elemento == "borrador") {
			if ($codigo >= 0) {
				$output->borrador = $codigo;
			}
			else {
				header("Location: ".base_url()."index.php/pedidos_presupuestos/nuevo/");
			}
		}
		// Si se intenta acceder al formulario, con el cliente preseleccionado, y desde el backend del comisionista
		elseif ($elemento == "cliente" && $this->session->tipo_usuario == 'comisionista') {
			$output->borrador = "";
			$output->cliente = true;
			$output->codigo_cliente = $codigo;
			$datosCliente = $this->Propio_model->obtenerDatosCliente($codigo, $this->session->comisionista);

			// Si no se encuentra nombre para ese código, es que no existe, con lo que se nos devolvera false y redirigimos al formulario en blanco
			if ($datosCliente == false) {
				header("Location: ".base_url()."index.php/pedidos_presupuestos/nuevo/");
				die;
			}
			else {
				$output->nombre_cliente = trim($datosCliente->nombre);
				$output->email_cliente = trim($datosCliente->email);
				$output->tarifa_cliente = trim($datosCliente->tarifa);
			}
		}
		// Si se intenta continuar el ultimo formulario
		elseif ($elemento == "continuar") {
			// Si se introduce codigo, se quita, ya que no se acceder a /continuar/$codigo
			if ($codigo == "") {
				$output->borrador = "";
			}
			else {
				header("Location: ".base_url()."index.php/pedidos_presupuestos/nuevo/continuar/");
			}
		}
		// Si se intenta acceder al formulario en blanco
		elseif ($elemento == "" && $codigo == "") {
			$output->borrador = "";
		}
		// Si se intenta acceder a algo que no se tiene en cuenta, se redirige al formulario en blanco
		else {
			header("Location: ".base_url()."index.php/pedidos_presupuestos/nuevo");
		}

		$this->load->view('general/formulario_pedido_presupuesto_view',$output);

	}

	function obtener_articulos () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->login && isset($_POST['codigo_cliente']) && isset($_POST['nombre_articulo']) && isset($_POST['codigo_articulo']) && isset($_POST['filtros']) && isset($_POST['limit'])) {

			$codigo_cliente = $_POST['codigo_cliente'];
			$nombre_articulo = $_POST['nombre_articulo'];
			$codigo_articulo = $_POST['codigo_articulo'];
			$filtros = json_decode($_POST['filtros']);
			$limit = $_POST['limit'];

			$cliente_correcto = false;
			$articulos_cliente = false;

			// Si la sesion es de cliente
			if ($this->session->cliente) {

				// Marcamos que solo se obtendran los articulos para los clientes
				$articulos_cliente = true;

				// Marcamos el cliente como correcto si corresponde al que tiene iniciada la sesion
				if ($this->session->cliente == $codigo_cliente) $cliente_correcto = true;

			}
			// Si la sesion es de comisionista
			elseif ($this->session->comisionista) {

				// Comprobamos si es cliente del comisionista que tiene iniciada la sesion
				$cliente_correcto = $this->Propio_model->comprobarClienteComisionista($codigo_cliente, $this->session->comisionista);

			}

			// Si el cliente recibido es correcto
			if ($cliente_correcto) {

				// Obtenemos los datos del cliente
				$cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente);

				// Si existen filtros, se llamara al modelo que los aplica, si no, se llamara al que no
				if (count($filtros)) {
					$respuesta->articulos = $this->Propio_model->obtenerArticulosFormularioPedidoConFiltros($nombre_articulo, $codigo_articulo, $filtros, $limit, $articulos_cliente, $cliente->tarifa);
					// Hallamos el total de articulos
					$respuesta->cantidadArticulosTotal = $this->Propio_model->obtenerCantidadArticulosTotalesConFiltros($nombre_articulo, $codigo_articulo, $filtros, $articulos_cliente);
				}
				else {
					$respuesta->articulos = $this->Propio_model->obtenerArticulosFormularioPedidoSinFiltros($nombre_articulo, $codigo_articulo, $limit, $articulos_cliente, $cliente->tarifa);
					// Hallamos el total de articulos
					$respuesta->cantidadArticulosTotal = $this->Propio_model->obtenerCantidadArticulosTotalesSinFiltros($nombre_articulo, $codigo_articulo, $articulos_cliente);
				}

				// Si la sesion iniciada es de un cliente
				if ($this->session->cliente) {

					// Hallamos los descuentos por articulo del cliente de la sesion
					$descuentos_cliente =  $this->Propio_model->obtenerDescuentosArticuloCliente($this->session->cliente);


				}
				// Si la sesion iniciada no es de un cliente
				else {

					// Definimos como vacio el array de descuentos
					$descuentos_cliente = Array();

				}

				$respuesta->articulos = calcular_descuentos_articulos($respuesta->articulos, $descuentos_cliente);

				$respuesta->resuelto = "OK";

			}

		}

		echo json_encode($respuesta);

	}

	function obtener_clientes () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->comisionista){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->comisionista && isset($_POST['nombre_cliente']) && isset($_POST['codigo_cliente']) && isset($_POST['filtros'])) {

			$comisionista = $this->session->comisionista;
			$nombre_cliente = $_POST['nombre_cliente'];
			$codigo_cliente = $_POST['codigo_cliente'];
			$filtros = json_decode($_POST['filtros']);

			// Si existen filtros, se llamara al modelo que los aplica, si no, se llamara al que no
			if (count($filtros)) {
				$respuesta->clientes = $this->Propio_model->obtenerClientesFormularioPedidoConFiltros($comisionista, $nombre_cliente, $codigo_cliente, $filtros);
			}
			else {
				$respuesta->clientes = $this->Propio_model->obtenerClientesFormularioPedidoSinFiltros($comisionista, $nombre_cliente, $codigo_cliente);
			}


			$respuesta->resuelto = "OK";

		}

		echo json_encode($respuesta);

	}

	function obtener_historico () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->login && isset($_POST['cliente']) && isset($_POST['filtros'])) {

			$cliente = $_POST['cliente'];
			$filtros = json_decode($_POST['filtros']);

			$cliente_correcto = false;

			// Si la sesion es de cliente
			if ($this->session->cliente) {

				// Marcamos el cliente como correcto si corresponde al que tiene iniciada la sesion
				if ($this->session->cliente == $cliente) $cliente_correcto = true;

			}
			// Si la sesion es de comisionista
			elseif ($this->session->comisionista) {

				// Comprobamos si es cliente del comisionista que tiene iniciada la sesion
				$cliente_correcto = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);

			}

			// Si el cliente recibido es correcto
			if ($cliente_correcto) {

				// Obtenemos los datos del cliente
				$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

				$respuesta->historico = $this->Propio_model->obtenerHistoricoFormularioPedido($datos_cliente, $filtros);
				$respuesta->resuelto = "OK";

			}

		}

		echo json_encode($respuesta);

	}

	function obtener_pendientes () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->login && isset($_POST['cliente']) && isset($_POST['filtros'])) {

			$codigo_cliente = $_POST['cliente'];
			$comisionista = $this->session->comisionista;
			$filtros = json_decode($_POST['filtros']);

			$cliente_correcto = false;

			// Si la sesion es de cliente
			if ($this->session->cliente) {

				// Marcamos el cliente como correcto si corresponde al que tiene iniciada la sesion
				if ($this->session->cliente == $codigo_cliente) $cliente_correcto = true;

			}
			// Si la sesion es de comisionista
			elseif ($this->session->comisionista) {

				// Comprobamos si es cliente del comisionista que tiene iniciada la sesion
				$cliente_correcto = $this->Propio_model->comprobarClienteComisionista($codigo_cliente, $this->session->comisionista);

			}

			// Si el cliente recibido es correcto
			if ($cliente_correcto) {

				// Obtenemos los datos del cliente
				$datos_cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente);

				$respuesta->pendientes = $this->Propio_model->obtenerPendientesFormularioPedido($datos_cliente, $filtros);

				$respuesta->resuelto = "OK";

			}

		}

		echo json_encode($respuesta);

	}

	function obtener_datos_articulo () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->login && isset($_POST['codigo_articulo']) && isset($_POST['codigo_cliente'])) {

			$codigo_articulo = $_POST['codigo_articulo'];
			$codigo_cliente = $_POST['codigo_cliente'];

			// Obtenemos los datos del cliente
			$cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente);

			// Obtenemos los datos del articulo
			$respuesta->articulo = $this->Propio_model->obtenerDatosArticulo($codigo_articulo, $cliente->tarifa);

			if ($respuesta->articulo) $respuesta->resuelto = "OK";

		}

		echo json_encode($respuesta);

	}

	function enviar_pedido_presupuesto () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->login){

			$respuesta->sesion_expirada = true;

		}
		elseif ($this->session->login && comprobar_existen_posiciones_array($_POST, Array('numero','fecha','comisionista','codigo_cliente','nombre_cliente','tipo','subPedido','emails_envio','observaciones','lineas'))) {

			// Incluimos las funciones para generar el pedido
			$this->load->helper("pedidos_helper");

			// Recogemos los datos del pedido
			$pedido = recoger_datos_pedido_presupuesto($_POST);

			// Procesamos el pedido
			$respuesta->resuelto = procesar_pedido_presupuesto($pedido);

		}

		echo JSON_encode($respuesta);

	}

}
