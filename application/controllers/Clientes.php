<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

	function __construct() {

		parent::__construct();

		include APPPATH.'third_party/vtwsclib/Vtiger/WSClient.php';

	}

	function index() {

		// Si no hay ninguna sesion iniciada de comisionista o de admin, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		$output = new stdClass();
		$output->menu = "clientes";
		$output->title = "Clientes";
		$output->css_panel = true;
		$output->css_postmain = Array("clientes/clientes_list.css");
		$output->js_postmain = Array("comisionistas/list_clientes.js");

		if ($this->session->tipo_usuario == 'admin') {
			$output->registros_iniciales = $this->Propio_model->obtenerClientesIniciales();
			$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadClientesTotales();
		}
		elseif ($this->session->tipo_usuario == 'comisionista') {
			$output->registros_iniciales = $this->Propio_model->obtenerClientesIniciales($this->session->comisionista);
			$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadClientesTotales($this->session->comisionista);
		}

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		$this->load->view('comisionistas/clientes_list_view',$output);

	}

	function obtener_otros_clientes() {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!($this->session->admin || $this->session->comisionista)){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && ($this->session->admin || $this->session->comisionista)) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
            $columnaOrdenacion = $_POST["columna_ordenacion"];

			if ($this->session->tipo_usuario == 'admin') {
				$registros = $this->Propio_model->obtenerOtrosClientes(false, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion);
				$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosClientes(false, $filtros);
			}
			elseif ($this->session->tipo_usuario == 'comisionista') {
				$comisionista = $this->session->comisionista;
				$registros = $this->Propio_model->obtenerOtrosClientes($comisionista, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion);
				$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosClientes($comisionista, $filtros);
			}

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				$tarifa2 = $registro->tarifa == 2 ? "cliente-tarifa2" : "";

				if ($registro->telefono == "") $registro->telefono = "-";
				if ($registro->municipio == "") $registro->municipio = "-";

				$html .=    "<tr class='".$odd_even." ".$tarifa2."'>".
								"<td style='border-right: none;'></td>".
								"<td style='border-left: none;'>".
								"<div style='white-space: nowrap' class='only-desktops'>".
									"<a href='".base_url()."index.php/clientes/perfil/".$registro->codigo."' class='btn btn-primary'>Seleccionar</a>".
								"</div>".
								"<div class='only-mobiles'>".
									"<div class='btn-group dropdown'>".
										"<button data-toggle='dropdown' class='btn btn-primary dropdown-toggle' type='button'>".
											"Acciones <span class='caret'></span>".
										"</button>".
										"<ul class='dropdown-menu'>".
											"<li>".
												"<a href='".base_url()."index.php/clientes/edit/".$registro->codigo."'> <i class='fa fa-pencil'></i> Editar </a>".
											"</li>".
										"</ul>".
									"</div>".
								"</div></td>".
								"<td>".$registro->codigo."</td>".
								"<td>".$registro->nombre."</td>".
								"<td>".$registro->municipio."</td>".
								"<td>".$registro->telefono."</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $inicioLimit + 1;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

	function perfil ($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

		}

		// Obtenemos el cliente de vTiger, para saber si existe o no
		$cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		$output = Array(
			"menu" => "clientes",
			"menu_cliente" => "perfil",
			"title" => "Perfil de cliente",
			"css_postmain" => Array("clientes/perfil_cliente.css","clientes/menu_cliente.css?v=1.02"),
			"cliente" => $datos_cliente,
			"cliente_vtiger" => $cliente_vtiger
		);

		$this->load->view('comisionistas/clientes_perfil_view',$output);

	}

	function historico ($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

		}

		$output = new stdClass();
		$output->menu = "clientes";
		$output->menu_cliente = "historico";
		$output->title = "Histórico de cliente";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css","clientes/clientes_historico.css","clientes/menu_cliente.css?v=1.01");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("comisionistas/historico_clientes.js");

		$output->cliente = $datos_cliente;
		$output->registros_iniciales = $this->Propio_model->obtenerHistoricoClienteInicial($datos_cliente);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadHistoricoCliente($datos_cliente);

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		foreach ($output->registros_iniciales as $index => $registro) {

			$output->registros_iniciales[$index]->fechaPrimer = fechaToString($registro->fechaPrimer);
			$output->registros_iniciales[$index]->fechaUltimo = fechaToString($registro->fechaUltimo);

		}

		$output->cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		$this->load->view('comisionistas/clientes_historico_view',$output);

	}

	function obtener_otros_historico () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!($this->session->admin || $this->session->comisionista || $this->session->cliente)){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && isset($_POST['cliente']) && ($this->session->admin || $this->session->comisionista || $this->session->cliente)) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
      $columnaOrdenacion = $_POST["columna_ordenacion"];
			$cliente = $_POST['cliente'];

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Obtenemos los registros y el número de registros para ese filtro
			$registros = $this->Propio_model->obtenerOtrosHistoricoCliente($datos_cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosHistoricoCliente($datos_cliente, $filtros);

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				if ($registro->descripcion == "") $registro->descripcion = "-";

				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->codigo."</td>".
								"<td>".$registro->descripcion."</td>".
								"<td>".fechaToString($registro->fechaPrimer)."</td>".
								"<td>".fechaToString($registro->fechaUltimo)."</td>".
								"<td>".round($registro->unidadesUltimo, 2)."</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

	function pendientes_entrega($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista o admin, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

		}

		$output = new stdClass();
		$output->menu = "clientes";
		$output->menu_cliente = "pendientes_entrega";
		$output->title = "Pendientes de entrega a cliente";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css","clientes/menu_cliente.css?v=1.01","clientes/pendientes_entrega.css");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("comisionistas/pendientes_entrega_clientes.js");

		$output->cliente = $datos_cliente;
		$output->registros_iniciales = $this->Propio_model->obtenerPendientesEntregaClienteInicial($datos_cliente);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadPendientesEntregaCliente($datos_cliente);

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		$output->cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		$this->load->view('comisionistas/clientes_pendientes_entrega_view',$output);

	}

	function obtener_otros_pendientes_entrega () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!($this->session->admin || $this->session->comisionista)){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && isset($_POST['cliente']) && ($this->session->admin || $this->session->comisionista)) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
      $columnaOrdenacion = $_POST["columna_ordenacion"];
			$cliente = $_POST['cliente'];

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Obtenemos los registros y el número de registros para ese filtro
			$registros = $this->Propio_model->obtenerOtrosPendientesEntregaCliente($datos_cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosPendientesEntregaCliente($datos_cliente, $filtros);

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				switch ($registro->estado) {
					case "P": $estado = "Pendiente"; break;
					case "T": $estado = "Terminación"; break;
					case "E": $estado = "Embalaje"; break;
					case "I": $estado = "Preparación"; break;
				}

				if ($registro->descripcionArticulo == "") $registro->descripcionArticulo = "-";

				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->pedido."</td>".
								"<td>".fechaToString($registro->fecha)."</td>".
								"<td>".$registro->codigoArticulo."</td>".
								"<td>".$registro->descripcionArticulo."</td>".
								"<td>".round($registro->unidadesPedidas, 2)."</td>".
								"<td>".round($registro->unidadesPendientes, 2)."</td>".
								"<td>".$estado."</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

	function albaranes_reserva($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista o admin, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

			// Tomamos el comisionista
			$comisionista = $this->session->comisionista;

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Definimos el comisionista como false
			$comisionista = false;

		}

		$output = new stdClass();
		$output->menu = "clientes";
		$output->menu_cliente = "albaranes_reserva";
		$output->title = "Albaranes de reserva cliente";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css","clientes/menu_cliente.css?v=1.01");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("comisionistas/albaranes_reserva_clientes.js");

		$output->cliente = $datos_cliente;

		$serieNumeroAlbaranesReserva = $this->Propio_model->obtenerSerieNumeroAlbaranesReservaClienteInicial($datos_cliente, $comisionista);

		$output->registros_iniciales = $this->Propio_model->obtenerLineasAlbaranesReservaClienteInicial($datos_cliente, $comisionista, $serieNumeroAlbaranesReserva);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadLineasAlbaranesReservaClienteInicial($datos_cliente, $comisionista, $serieNumeroAlbaranesReserva);
		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		$output->cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		$this->load->view('comisionistas/clientes_albaranes_reserva_view',$output);

	}

	function obtener_otros_albaranes_reserva () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!($this->session->admin || $this->session->comisionista)){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && isset($_POST['cliente']) && ($this->session->admin || $this->session->comisionista)) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
      $columnaOrdenacion = $_POST["columna_ordenacion"];
			$cliente = $_POST['cliente'];

			// Si el usuario es un comisionista,
			if ($this->session->tipo_usuario == 'comisionista') {

				// Tomamos el comisionista
				$comisionista = $this->session->comisionista;

			}
			// Si es admin
			elseif ($this->session->tipo_usuario == 'admin') {

				// Definimos el comisionista como false
				$comisionista = false;

			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Obtenemos las series
			$serieNumeroAlbaranesReserva = $this->Propio_model->obtenerSerieNumeroAlbaranesReservaClienteInicial($datos_cliente, $comisionista);

			// Obtenemos los registros y el número de registros para ese filtro
			$registros = $this->Propio_model->obtenerOtrosAlbaranesReservaCliente($datos_cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion, $comisionista, $serieNumeroAlbaranesReserva);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosAlbaranesReservaCliente($datos_cliente, $filtros, $comisionista, $serieNumeroAlbaranesReserva);

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				if ($registro->descripcionArticulo == "") $registro->descripcionArticulo = "-";

				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->albaran."</td>".
								"<td>".fechaToString($registro->fecha)."</td>".
								"<td>".$registro->codigoArticulo."</td>".
								"<td>".$registro->descripcionArticulo."</td>".
								"<td>".round($registro->unidades, 2)."</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

	function oportunidad ($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

		}

		// Obtenemos los datos del cliente de vtiger
		$cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		/* LOS SELECTORES DINAMICOS SE DESACTIVARON
		// Si obtuvimos el cliente de vtiger, obtenemos datos extra del formulario_pedidos_presupuestos
		if ($cliente_vtiger) {

			$contactos_vtiger = $this->obtener_contactos_vtiger();

		}
		// Si no obtuvimos el cliente, no obtenemos los datos extra
		else {

			$contactos_vtiger = false;

		}
		*/

		$output = Array(
			"menu" => "clientes",
			"menu_cliente" => "oportunidad",
			"title" => "Oportunidad cliente",
			"css_panel" => true,
			"css_postmain" => Array("formulario_pedidos_presupuestos.css","clientes/menu_cliente.css?v=1.01"),
			"js_postmain" => Array("vtiger.js"),
			"cliente" => $datos_cliente,
			"cliente_vtiger" => $cliente_vtiger
			//"contactos_vtiger" => $contactos_vtiger
		);

		$this -> load -> view('comisionistas/clientes_oportunidad_view', $output);

	}

	function caso ($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

		}

		// Obtenemos los datos del cliente de vtiger
		$cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		/* LOS SELECTORES DINAMICOS SE DESACTIVARON
		// Si obtuvimos el cliente de vtiger, obtenemos datos extra del formulario_pedidos_presupuestos
		if ($cliente_vtiger) {

			$contactos_vtiger = $this->obtener_contactos_vtiger();
			$productos_vtiger = $this->obtener_productos_vtiger();

		}
		// Si no obtuvimos el cliente, no obtenemos los datos extra
		else {

			$contactos_vtiger = false;
			$productos_vtiger = false;

		}
		*/

		$output = Array(
			"menu" => "clientes",
			"menu_cliente" => "caso",
			"title" => "Caso cliente",
			"css_panel" => true,
			"css_postmain" => Array("formulario_pedidos_presupuestos.css","clientes/menu_cliente.css?v=1.01"),
			"js_postmain" => Array("vtiger.js"),
			"cliente" => $datos_cliente,
			"cliente_vtiger" => $cliente_vtiger,
			//"contactos_vtiger" => $contactos_vtiger,
			//"productos_vtiger" => $productos_vtiger
		);

		$this -> load -> view('comisionistas/clientes_caso_view', $output);

	}

	function obtener_cliente_vtiger ($codigo_cliente) {

		$cliente_vtiger = false;

		$client = new Vtiger_WSClient('https://crm.abrastar.com:8443');

		$login = $client->doLogin('vtws', 'Atxk550YLQPp9Iw');

		if ($login)  {

			$query = "SELECT * FROM Accounts WHERE cf_851 = '".intval($codigo_cliente)."' LIMIT 1";

			$result = $client->doQuery($query);

			if ($result) {

				$columns = $client->getResultColumns($result);

				$cliente_vtiger = (object) $result[0];
				$cliente_vtiger->id = $client->getRecordId($cliente_vtiger->id);

			}

		}

		return $cliente_vtiger;

	}

	function obtener_contactos_vtiger () {

		$contactos = Array();

		$client = new Vtiger_WSClient('https://crm.abrastar.com:8443');

		$login = $client->doLogin('vtws', 'Atxk550YLQPp9Iw');

		if ($login)  {

			$mas_contactos = true;
			$limit = 0;

			while ($mas_contactos) {

				$query = "SELECT * FROM Contacts ORDER BY firstname,lastname LIMIT ".$limit.",100";

				$result = $client->doQuery($query);

				// Añadimos los resultados al array, si es que hay
				if ($result) {

					foreach ($result as $row) {

						$row['id'] = $client->getRecordId($row['id']);
						$contactos[] = (object) $row;

					}

				}

				// Hacemos otra comprobacion para ver si tiene que parar de obtenerClientesIniciales
				if (!$result || count($result) < 100) {

					$mas_contactos = false;

				}

				$limit += 100;

			}

		}

		return $contactos;

	}

	function obtener_productos_vtiger () {

		$productos = Array();

		$client = new Vtiger_WSClient('https://crm.abrastar.com:8443');

		$login = $client->doLogin('vtws', 'Atxk550YLQPp9Iw');

		if ($login)  {

			$mas_productos = true;
			$limit = 0;

			while ($mas_productos) {

				$query = "SELECT * FROM Products ORDER BY productname LIMIT ".$limit.",100";

				$result = $client->doQuery($query);

				// Añadimos los resultados al array, si es que hay
				if ($result) {

					foreach ($result as $row) {

						$row['id'] = $client->getRecordId($row['id']);
						$productos[] = (object) $row;

					}

				}

				// Hacemos otra comprobacion para ver si tiene que parar de obtenerClientesIniciales
				if (!$result || count($result) < 100) {

					$mas_productos = false;

				}

				$limit += 100;

			}

		}

		return $productos;

	}

	public function ofertas ($cliente) {

		// Si no hay ninguna sesion iniciada de comisionista o admin, mandarlo al login
		comprobarSesionIniciada(Array('comisionista','admin'));

		// Si el usuario es un comisionista,
		if ($this->session->tipo_usuario == 'comisionista') {

			// Comprobamos que el cliente pertenece a dicho comisionista
			$clienteComisionista = $this->Propio_model->comprobarClienteComisionista($cliente, $this->session->comisionista);
			if (!$clienteComisionista) {
				header("location:".base_url()."index.php/clientes");
				die;
			}

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente, $this->session->comisionista);

			// Tomamos el comisionista
			$comisionista = $this->session->comisionista;

		}
		// Si es admin
		elseif ($this->session->tipo_usuario == 'admin') {

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Definimos el comisionista como false
			$comisionista = false;

		}

		// Obtenemos las ofertas del cliente
		$ofertas = $this->Propio_model->obtener_ofertas_cliente($datos_cliente->codigo);

		// Nos recorremos las ofertas
		foreach ($ofertas as $oferta) {

			// Obtenemos las lineas de la oferta
			$oferta->lineas = $this->Propio_model->obtener_lineas_oferta($oferta);

		}

		// Procesamos los datos de las ofertas
		$ofertas = $this->procesar_datos_ofertas($ofertas);

		// Obtenemos el cliente de vTiger para ver si existe
		$cliente_vtiger = $this->obtener_cliente_vtiger($datos_cliente->codigo);

		$output = Array(
			"menu" => "clientes",
			"menu_cliente" => "ofertas",
			"title" => "Ofertas cliente",
			"css_panel" => true,
			"css_postmain" => Array("clientes/ofertas_cliente.css","clientes/menu_cliente.css?v=1.02"),
			"js_postmain" => Array("comisionistas/ofertas_clientes.js"),
			"cliente" => $datos_cliente,
			"ofertas" => $ofertas,
			"cliente_vtiger" => $cliente_vtiger
		);

		$this->load->view('comisionistas/clientes_ofertas_view',$output);

	}

	private function procesar_datos_ofertas ($ofertas) {

		foreach ($ofertas as $i => $oferta) {

			// Si tiene lineas la oferta
			if (count($oferta->lineas)) {

				$texto_primera_linea = false;

				// Transformamos la fecha
				$oferta->fecha = fechaToString($oferta->fecha);

				// Recorremos las lineas de la oferta
				foreach ($oferta->lineas as $linea) {

					// Si aun no hemos recogido el texto de la primera linea, lo recogemos
					if ( ! $texto_primera_linea) $texto_primera_linea = $linea->descripcionArticulo;

					// Transformamos los numeros
					$linea->precio = number_format($linea->precio, 3);
					$linea->unidadesPedidas = number_format($linea->unidadesPedidas, 2);
					$linea->descuento = number_format($linea->descuento, 2);

				}

				// Asignamos el texto de la primera linea a la oferta
				$oferta->texto_primera_linea = $texto_primera_linea;

			}
			// Si no tiene lineas la oferta, lo borraremos
			else unset($ofertas[$i]);

		}

		return $ofertas;

	}

}
