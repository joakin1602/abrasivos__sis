<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Importador_pedidos extends CI_Controller {

  public function index () {

    // Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada(Array("admin"));

    // Con esto se mostrara el formulario
    $mensaje = 0;

    // Si se recibe el formulario
    if (comprobar_existen_posiciones_array($_POST, Array("cliente")) && isset($_FILES["fichero"])) {

      $codigo_cliente = $_POST['cliente'];
      $fichero = $_FILES['fichero'];

      // Mensaje para los campos incorrectos
      $mensaje = -1;

      // Se reciben un cliente y un fichero correctos
      if ($this->comprobar_parametros_correctos($codigo_cliente, $fichero)) {

        // Obtenemos los datos del cliente
        $cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente);

        // WURTH
        if ($codigo_cliente == "2955") $correcto = $this->procesar_pedido_wurth($fichero, $cliente);

        // Mensaje correcto si todo ha ido bien
        if ($correcto) $mensaje = 1;

        // Si algo ha ido mal
        else $mensaje = -2;

      }

    }

    $output = Array(
      "menu" => "importador_pedidos",
      "title" => "Importador de Pedidos",
      "mensaje" => $mensaje
    );

    $this->load->view('admin/importador_pedidos_view', $output);

  }

  private function comprobar_parametros_correctos ($cliente, $fichero) {

    $correcto = false;

    $tipo_fichero = $fichero['type'];

    // WURTH
    if ($cliente == "2955" && $tipo_fichero == "text/xml") $correcto = true;

    return $correcto;

  }

  private function enviar_pedido ($pedido) {

    $correcto = false;

    // Incluimos las funciones para generar el pedido
    $this->load->helper("pedidos_helper");

    // Procesamos el pedido
    $resuelto = procesar_pedido_presupuesto($pedido);

    // Si la respuesta es OK, todo habra ido bien
    if ($resuelto == "OK") $correcto = true;

    return $correcto;

  }

  private function procesar_pedido_wurth ($fichero, $cliente) {

    $correcto = false;

    // Obtenemos el pedido del XML
    $pedido_xml = simplexml_load_file($fichero['tmp_name']);
    $pedido_json = json_encode($pedido_xml);
    $pedido = json_decode($pedido_json);

    // Obtenemos el numero del pedido
    $numero = obtenerNumeroFormularioPedidos();

    // Obtenemos las lineas del pedido
    $lineas = $this->obtener_lineas_pedido_wurth($pedido, $cliente);

    // Definimos las observaciones del pedido, donde incluiremos campos como la direccion
    $observaciones = $this->definir_observaciones_pedido_wurth($pedido);

    // Si hay lineas
    if ($lineas) {

      // Definimos los parametros del pedido
      $pedido = (object) Array(
        "comisionista" => comisionista_pedido_vacio(),
        "cliente" => $cliente,
        "tipo_usuario" => "admin",
        "empresa" => 1,
        "numero" => $numero,
        "fecha" => date("Y-m-d"),
        "tipo" => "Pedido",
        "subPedido" => "",
        "emails_envio" => Array(),
        "observaciones" => $observaciones,
        "lineas" => $lineas
      );

      /***************************************************************/
      /*** PRUEBAS ****************************************************
      // Definimos el cliente
      $cliente_pedido = (object) Array(
        "codigo" => "3590",
        "nombre" => "FERRETERIA LA PAZ, S.L."
      );
      $pedido = (object) Array(
        "comisionista" => comisionista_pedido_vacio(),
        "cliente" => $cliente_pedido,
        "tipo_usuario" => "admin",
        "empresa" => 1,
        "numero" => $numero,
        "fecha" => date("Y-m-d"),
        "tipo" => "Pedido",
        "subPedido" => "",
        "emails_envio" => Array("joaquin@asesoresnt.com"),
        "observaciones" => "PEDIDO DE PRUEBA !!! -".$observaciones,
        "lineas" => $lineas
      );
      ****************************************************************/
      /***************************************************************/

      // Enviamos el pedido
      $correcto = $this->enviar_pedido($pedido);

    }

    return $correcto;

  }

  private function obtener_lineas_pedido_wurth ($pedido, $cliente) {

    $lineas = Array();

    $lineas_fichero = $pedido->DETALLE_PED->LINEA;

    // Si es una sola linea, sera un objeto, esto es por la conversion del XML
    if (gettype($lineas_fichero) == "object") {

      // Lo arreglamos de la siguiente manera
      $lineas_fichero = $pedido->DETALLE_PED;

    }

    // Nos recorremos las lineas del fichero
    foreach ($lineas_fichero as $linea_fichero) {

      // Obtenemos el codigo del articulo de MURANO
      $codigo_articulo = $this->Propio_model->obtener_codigo_articulo_cliente($cliente->codigo, $linea_fichero->NARTICULO);

      if ($codigo_articulo) {

        // Obtenemos los datos del articulo
        $articulo = $this->Propio_model->obtener_datos_articulo($codigo_articulo);

        // El precio dependera de la tarifa del cliente
        if ($cliente->tarifa == 1) $precio = $articulo->precio_1;
        else $precio = $articulo->precio_2;

        // Realizamos calculos para la linea
        $cantidad = $linea_fichero->CANT / $linea_fichero->POR;
        $total = $precio * $cantidad;
        $neto = $precio;

        // Definimos la linea
        $linea = (object) Array(
          "prioridad" => "0",
          "codigo" => $articulo->codigo,
          "nombre" => $articulo->nombre,
          "codigo_nombre" => $articulo->codigo." - ".$articulo->nombre,
          "cantidad" => $cantidad,
          "stock" => $articulo->stockVirtual,
          "precio" => $precio,
          "descuento" => 0,
          "neto" => $neto,
          "total" => $total
        );

        // Añadimos la linea al array de lineas
        $lineas[] = $linea;

      }

    }

    return $lineas;

  }

  private function definir_observaciones_pedido_wurth ($pedido) {

    // Obtenemos la direccion de entrega
    $direccion = $pedido->CABECERA->OTROS->ENTREGA;

    // Obtenemos la fecha de entrega
    $fecha =  $pedido->CABECERA->OTROS->FECHA;
    $fecha = substr($fecha, 6, 2)."/".substr($fecha, 4, 2)."/".substr($fecha, 0, 4);

    // Definimos las observaciones
    $observaciones = "ENTREGA: ".$direccion." (".$fecha.")";

    return $observaciones;

  }

}
