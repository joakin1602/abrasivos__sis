<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Keys_login extends CI_Controller {

	public function index() {

    // Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada(Array("admin"));

    // Obtenemos todos los comisionistas y clientes con acceso
    $comisionistas = $this->Propio_model->obtener_todos_comisionistas_con_acceso();
    $clientes = $this->Propio_model->obtener_todos_clientes_con_acceso();

    // Obtenemos las keys registradas por el momento
    $keys = $this->Propio_model->obtener_keys_login();

		// Ponemos la fecha de ultimo acceso mas estetica
		foreach($keys as $key) {

			// Si es 1, pondremos la fecha
			if ($key->registrada == 1) $key->ultimo_acceso = fechaHoraToString($key->ultimo_acceso);

			// Si no esta registrada, lo indicaremos mediante la fecha
			else $key->ultimo_acceso = "No registrada";

		}

    $output = Array(
      "menu" => "keys_login",
      "title" => "Keys de inicio de sesión",
      "css_panel" => true,
      "js_postmain" => Array("admin/keys_login.js"),
      "comisionistas" => $comisionistas,
      "clientes" => $clientes,
      "keys" => $keys
    );

    $this->load->view('admin/keys_login_view',$output);

  }

  public function registrar_key () {

    $respuesta = Array (
      "resuelto" => "ER",
      "mensaje" => "Ha ocurrido un error",
      "sesion_expirada" => false
    );

    if (!$this->session->admin) {

			$respuesta->sesion_expirada = true;

		}

    elseif ($this->session->admin && comprobar_existen_posiciones_array($_POST, Array("key", "tipo_usuario", "id_usuario", "descripcion"))) {

      $datos = (object) Array(
        "key" => $_POST["key"],
        "tipo_usuario" => $_POST["tipo_usuario"],
        "id_usuario" => $_POST["id_usuario"],
        "descripcion" => $_POST["descripcion"],
        "registrada" => 0
      );

			// Realizamos varias comprobaciones para ver que la key es correcta
      if ($this->comprobar_datos_key_correctos($datos)) {

				$keys_usuario = $this->Propio_model->obtener_keys_login_usuario($datos->tipo_usuario, $datos->id_usuario);

				// Si el usuario tiene mas de 3 keys registradas
				if (count($keys_usuario) >= 3 && $datos->tipo_usuario != 'admin') {

					$respuesta = Array (
			      "resuelto" => "ER",
			      "mensaje" => "El usuario tiene ya 3 keys registradas. Borra una antes de ese usuario si quieres añadir más.",
			      "sesion_expirada" => false
			    );

				}
				// Si el usuario tiene menos de 3 keys registradas
				else{

					// Insertamos la key
	        $this->Propio_model->insertar_key_login($datos);

	        $respuesta = Array (
	          "resuelto" => "OK"
	        );

				}

      }

    }

    echo json_encode($respuesta);

  }

  private function comprobar_datos_key_correctos ($datos) {

    $correcto = true;

    // Algun campo con valor vacio
    if (!(strlen($datos->key) > 0) || !(strlen($datos->tipo_usuario) > 0) || !(strlen($datos->id_usuario) > 0)) {

      $correcto = false;

    }
    // Tipo de usuario incorrecto
    elseif ($datos->tipo_usuario != "admin" && $datos->tipo_usuario != "comisionista" && $datos->tipo_usuario != "cliente") {

      $correcto = false;

    }

    // Admin incorrecto
    else if ($datos->tipo_usuario == "admin" && $datos->id_usuario != "admin") {

      $correcto = false;

    }

    // Comisionista incorrecto
    elseif ($datos->tipo_usuario == "comisionista" && !($this->Propio_model->comprobar_comisionista_existe_con_acceso($datos->id_usuario))) {

      $correcto = false;

    }

    // Cliente incorrecto
    elseif ($datos->tipo_usuario == "cliente" && !($this->Propio_model->comprobar_cliente_existe_con_acceso($datos->id_usuario))) {

      $correcto = false;

    }

    return $correcto;

  }

  public function eliminar_key () {

    $respuesta = Array (
      "resuelto" => "ER",
      "mensaje" => "Ha ocurrido un error",
      "sesion_expirada" => false
    );

    if (!$this->session->admin) {

      $respuesta->sesion_expirada = true;

    }

    elseif ($this->session->admin && comprobar_existen_posiciones_array($_POST, Array("id_key"))) {

      $id_key = $_POST["id_key"];

      if ($this->Propio_model->comprobar_key_login_existe($id_key)) {

        $this->Propio_model->eliminar_key($id_key);

        $respuesta = Array (
          "resuelto" => "OK",
          "mensaje" => "Key borrada con éxito"
        );

      }

    }

    echo json_encode($respuesta);

  }

}
