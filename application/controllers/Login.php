<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index() {

		$output = Array(
			"js_premain" => Array("jquery.md5.js"),
			"js_postmain" => Array("login.js")
		);

		$this->load->view('general/login_view', $output);

	}

	public function validar_login() {

		$respuesta = Array(
			"resuelto" => "ER"
		);

		if (isset($_POST['usuario']) && isset($_POST['clave']) && isset($_POST['key'])) {

			$usuario = strtolower($_POST["usuario"]);
			$clave = $_POST["clave"];
			$key = $_POST["key"];
			$key_correcta = false;

			// Comprobamos si el login es de admin
			if ($usuario == "admin" && $clave == "2be0f3b42e9d93a44cd2c68fcdd004c6") {

				/* DESACTIVADO EL USO DE LA KEY PARA EL ADMIN
				// Si recibimos una key, comprobamos que exista
				if ($key != "") $key_correcta = $this->comprobar_key_login_registrada_correcta("admin", "admin", $key);

				// Si la key es correcta, iniciamos sesion
				if ($key_correcta) {

					inicioSesionAdmin();
					$this->Propio_model->crearLogInicioSesion("admin");

					$respuesta = Array(
						"resuelto" => "OK",
						"key" => 1
					);

				}
				// Si la key es incorrecta, lo indicamos
				else {

					$respuesta = Array(
						"resuelto" => "OK",
						"key" => 0
					);

				}
				*/

				inicioSesionAdmin();
				$this->Propio_model->crearLogInicioSesion("admin");

				$respuesta = Array(
					"resuelto" => "OK",
					"key" => 1
				);

			}
			// Si falla el login de admin
			else {

				$datosComisionista = $this->Propio_model->comprobarUsuarioComisionistaCorrecto($usuario, $clave);

				// Comprobamos que el login del comisionista haya sido correcto
				if ($datosComisionista->correcto) {

					// Si recibimos una key, comprobamos que exista
					if ($key != "") $key_correcta = $this->comprobar_key_login_registrada_correcta("comisionista", $datosComisionista->codigo, $key);

					// Si la key es correcta, iniciamos sesion
					if ($key_correcta) {

						inicioSesionComisionista($datosComisionista);
						$this->Propio_model->crearLogInicioSesion("comisionista");

						$respuesta = Array(
							"resuelto" => "OK",
							"key" => 1
						);

					}
					// Si la key es incorrecta, lo indicamos
					else {

						$respuesta = Array(
							"resuelto" => "OK",
							"key" => 0
						);

					}

				}
				// Si ha fallado el login del comisionista, probamos con el del cliente
				else {

					$datosCliente = $this->Propio_model->comprobarUsuarioClienteCorrecto($usuario, $clave);

					// Comprobamos que el login del cliente haya sido correcto y si no lo es, el login ha fallado
					if ($datosCliente->correcto) {

						// Si recibimos una key, comprobamos que exista
						if ($key != "") $key_correcta = $this->comprobar_key_login_registrada_correcta("cliente", $datosCliente->codigo, $key);

						// Si la key es correcta, iniciamos sesion
						if ($key_correcta) {

							inicioSesionCliente($datosCliente);
							$this->Propio_model->crearLogInicioSesion("cliente");

							$respuesta = Array(
								"resuelto" => "OK",
								"key" => 1
							);

						}
						// Si la key es incorrecta, lo indicamos
						else {

							$respuesta = Array(
								"resuelto" => "OK",
								"key" => 0
							);

						}

					}
				}
			}
		}

		echo json_encode($respuesta);

	}

	function cerrar_sesion() {
		$this->session->sess_destroy();
		header("Location: ".base_url()."index.php/login");
		die;
	}

	public function registrar_key () {

		$respuesta = Array(
			"resuelto" => "ER",
			"mensaje" => "Ha ocurrido un error"
		);

		if (isset($_POST['usuario']) && isset($_POST['clave']) && isset($_POST['key']) && $_POST['key'] != "") {

			$usuario = strtolower($_POST["usuario"]);
			$clave = $_POST["clave"];
			$key = $_POST["key"];

			// Comprobamos si el login es de admin
			if ($usuario == "admin" && $clave == "2be0f3b42e9d93a44cd2c68fcdd004c6") {

				// Comprobamos que exista la key
				$key_correcta = $this->comprobar_key_login_sin_registrar_correcta("admin", "admin", $key);

				// Si la key es correcta
				if ($key_correcta) {

					$respuesta = Array(
						"resuelto" => "OK"
					);

				}
				// Key incorrecta
				else {

					$respuesta = Array(
						"resuelto" => "ER",
						"mensaje" => "La key introducida es incorrecta"
					);

				}

			}
			// Si falla el login de admin
			else {

				$datosComisionista = $this->Propio_model->comprobarUsuarioComisionistaCorrecto($usuario, $clave);

				// Comprobamos que el login del comisionista haya sido correcto
				if ($datosComisionista->correcto) {

					// Comprobamos que exista la key
					$key_correcta = $this->comprobar_key_login_sin_registrar_correcta("comisionista", $datosComisionista->codigo, $key);

					// Si la key es correcta
					if ($key_correcta) {

						$respuesta = Array(
							"resuelto" => "OK"
						);

					}
					// Key incorrecta
					else {

						$respuesta = Array(
							"resuelto" => "ER",
							"mensaje" => "La key introducida es incorrecta"
						);

					}

				}
				// Si ha fallado el login del comisionista, probamos con el del cliente
				else {

					$datosCliente = $this->Propio_model->comprobarUsuarioClienteCorrecto($usuario, $clave);

					// Comprobamos que el login del cliente haya sido correcto y si no lo es, el login ha fallado
					if ($datosCliente->correcto) {

						// Comprobamos que exista la key
						$key_correcta = $this->comprobar_key_login_sin_registrar_correcta("cliente", $datosCliente->codigo, $key);

						// Si la key es correcta
						if ($key_correcta) {

							$respuesta = Array(
								"resuelto" => "OK"
							);

						}
						// Key incorrecta
						else {

							$respuesta = Array(
								"resuelto" => "ER",
								"mensaje" => "La key introducida es incorrecta"
							);

						}

					}
				}
			}
		}

		echo json_encode($respuesta);

	}

	private function comprobar_key_login_registrada_correcta ($tipo_usuario, $usuario, $key) {

		$correcta = false;

		// Tratamos de obtener la key correspondiente
		$key_login = $this->Propio_model->obtener_key_login($tipo_usuario, $usuario, $key, 1);

		// Si la key es correcta
		if ($key_login) {

			$datos = Array(
				"ultimo_acceso" => date("Y-m-d")."T".date("H:i:s")
			);

			// Actualizamos el ultimo acceso de la key
			$this->Propio_model->update_key_login($key_login->id, $datos);

			$correcta = true;

		}

		return $correcta;

	}

	private function comprobar_key_login_sin_registrar_correcta ($tipo_usuario, $usuario, $key) {

		$correcta = false;

		// Tratamos de obtener la key correspondiente
		$key_login = $this->Propio_model->obtener_key_login($tipo_usuario, $usuario, $key, 0);

		// Si la key es correcta
		if ($key_login) {

			$datos = Array(
				"registrada" => 1
			);

			// Marcamos la key como registrada
			$this->Propio_model->update_key_login($key_login->id, $datos);


			$correcta = true;

		}

		return $correcta;

	}

}
