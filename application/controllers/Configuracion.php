<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {

	public function index() {

    // Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada(Array("admin"));

    // Definimos las configuraciones
    $configuracion = new stdClass;

    // Obtenemos los emails de envio del pedido
    $configuracion->emails_envio = $this->Propio_model->obtener_valor_configuracion("emails_envio_pedido");

    $output = Array(
      "menu" => "configuracion",
      "title" => "Configuración",
      "js_postmain" => Array("admin/configuracion.js"),
      "configuracion" => $configuracion
    );

    $this->load->view('admin/configuracion_view', $output);

  }

  public function guardar_configuracion () {

    $respuesta = Array (
      "resuelto" => "ER",
      "mensaje" => "Ha ocurrido un error",
      "sesion_expirada" => false
    );

    if (!$this->session->admin) {

			$respuesta->sesion_expirada = true;

		}

    elseif ($this->session->admin && comprobar_existen_posiciones_array($_POST, Array("emails_envio"))) {

      $emails_envio = $_POST["emails_envio"];

      // Guardamos los emails de envio, en el caso que no esten vacios
      if ($emails_envio != "") {

        // Definimos el valor y realizamos el update
        $datos = Array("valor" => $emails_envio);
        $this->Propio_model->update_configuracion("emails_envio_pedido", $datos);

      }

      $respuesta = Array (
        "resuelto" => "OK"
      );

    }

    echo json_encode($respuesta);

  }

}
