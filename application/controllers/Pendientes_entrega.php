<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pendientes_entrega extends CI_Controller {

	function __construct() {

		parent::__construct();

	}

	function index() {

		// Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada();

		if ($this->session->tipo_usuario == "admin") $this->pendientes_entrega_admin();

		if ($this->session->tipo_usuario == "comisionista") $this->pendientes_entrega_comisionista();

		elseif ($this->session->tipo_usuario == "cliente") $this->pendientes_entrega_cliente();

	}

	function pendientes_entrega_admin() {

		// Si no hay ninguna sesion iniciada de admin, mandarlo al login
		comprobarSesionIniciada(Array('admin'));

		$output = new stdClass();
		$output->menu = "pendientes_entrega";
		$output->title = "Pendientes de entrega";
		$output->css_panel = true;
		$output->css_postmain = Array("pendientes_entrega.css");

		$output->pedidos = $this->Propio_model->obtenerPedidosPendientesEntrega();

		$output->cantidadRegistros = count($output->pedidos);

		$this->load->view('comisionistas/pendientes_entrega_view',$output);

	}

	function pendientes_entrega_comisionista() {

		// Si no hay ninguna sesion iniciada de comisionista, mandarlo al login
		comprobarSesionIniciada(Array('comisionista'));

		$output = new stdClass();
		$output->menu = "pendientes_entrega";
		$output->title = "Pendientes de entrega";
		$output->css_panel = true;
		$output->css_postmain = Array("pendientes_entrega.css");

		$output->pedidos = $this->Propio_model->obtenerPedidosPendientesEntrega ($this->session->comisionista);

		$output->cantidadRegistros = count($output->pedidos);

		$this->load->view('comisionistas/pendientes_entrega_view',$output);

	}

	function pendientes_entrega_cliente() {

		// Si no hay sesion requerida iniciada, mandarlo al login
		comprobarSesionIniciada(Array('cliente'));

		$output = new stdClass();
		$output->menu = "pendientes_entrega";
		$output->title = "Pendientes de entrega";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css","pendientes_entrega.css");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("clientes/pendientes_entrega.js");

		$codigo_cliente = $this->session->cliente;
		$codigo_comisionista = $this->Propio_model->obtenerCodigoComisionistaCliente($codigo_cliente);

		$output->cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente, $codigo_comisionista);
		$output->registros_iniciales = $this->Propio_model->obtenerPendientesEntregaClienteInicial($output->cliente);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadPendientesEntregaCliente($output->cliente);

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		$this->load->view('clientes/pendientes_entrega_view',$output);

	}

	function obtener_otros_pendientes_entrega () {

		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;

		if (!$this->session->cliente){

			$respuesta->sesion_expirada = true;

		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && isset($_POST['cliente'])) {

			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
      $columnaOrdenacion = $_POST["columna_ordenacion"];
			$cliente = $_POST['cliente'];

			// Obtenemos los datos del cliente
			$datos_cliente = $this->Propio_model->obtenerDatosCliente($cliente);

			// Obtenemos los registros y el número de registros para ese filtro
			$registros = $this->Propio_model->obtenerOtrosPendientesEntregaCliente($datos_cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosPendientesEntregaCliente($datos_cliente, $filtros);

			$html = "";

			foreach ($registros as $index => $registro) {

				$odd_even = $index % 2 == 0 ? "odd" : "even";

				switch ($registro->estado) {
					case "P": $estado = "Pendiente"; break;
					case "T": $estado = "Terminación"; break;
					case "E": $estado = "Embalaje"; break;
					case "I": $estado = "Preparación"; break;
				}

				if ($registro->descripcionArticulo == "") $registro->descripcionArticulo = "-";

				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->pedido."</td>".
								"<td>".fechaToString($registro->fecha)."</td>".
								"<td>".$registro->codigoArticulo."</td>".
								"<td>".$registro->descripcionArticulo."</td>".
								"<td>".round($registro->unidadesPedidas, 2)."</td>".
								"<td>".round($registro->unidadesPendientes, 2)."</td>".
								/*"<td>".round($registro->unidadesServidas, 2)."</td>".*/
								"<td>".$estado."</td>".
							"</tr>";

			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}

		echo json_encode($respuesta);

	}

}
