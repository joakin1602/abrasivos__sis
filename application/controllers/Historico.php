<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Historico extends CI_Controller {

	function __construct() {

		parent::__construct();

	}

	function index () {

		// Si no hay sesion requerida iniciada, mandarlo al login
		comprobarSesionIniciada(Array('cliente'));

		$codigo_cliente = $this->session->cliente;
		$codigo_comisionista = $this->Propio_model->obtenerCodigoComisionistaCliente($codigo_cliente);

		$output = new stdClass();
		$output->menu = "historico";
		$output->title = "Histórico de cliente";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("comisionistas/historico_clientes.js");

		$output->cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente, $codigo_comisionista);
		$output->registros_iniciales = $this->Propio_model->obtenerHistoricoClienteInicial($output->cliente);
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadHistoricoCliente($output->cliente);

		$output->cantidad_registros_iniciales = count($output->registros_iniciales);

		foreach ($output->registros_iniciales as $index => $registro) {

			$output->registros_iniciales[$index]->fechaPrimer = fechaToString($registro->fechaPrimer);
			$output->registros_iniciales[$index]->fechaUltimo = fechaToString($registro->fechaUltimo);

		}

		$this->load->view('clientes/historico_view',$output);

	}

}
