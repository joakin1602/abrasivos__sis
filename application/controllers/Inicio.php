<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct() {

		parent::__construct();

		// Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada();

	}

	public function index() {

		$output = new stdClass();
		$output->menu = "inicio";
		$output->title = "Inicio";
		$output->css_postmain = Array("inicio.css");

		if ($this->session->tipo_usuario == "admin") {
			$output->clientesConPendientesEntrega = $this->Propio_model->obtenerClientesPendientesEntrega();
			$output->clientesConAlbaranesReserva = $this->Propio_model->obtenerClientesAlbaranesReserva();
		}
		elseif ($this->session->tipo_usuario == "comisionista") {
			$output->clientesConPendientesEntrega = $this->Propio_model->obtenerClientesPendientesEntrega($this->session->comisionista);
			$output->clientesConAlbaranesReserva = $this->Propio_model->obtenerClientesAlbaranesReserva($this->session->comisionista);
		}

		$this -> load -> view('general/inicio_view', $output);

	}

}
