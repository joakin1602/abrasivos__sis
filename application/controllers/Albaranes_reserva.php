<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Albaranes_reserva extends CI_Controller {

	function __construct() {

		parent::__construct();
		
	}

	function index() {
		
		// Si no hay sesion requerida iniciada, mandarlo al login
		comprobarSesionIniciada(Array('cliente'));
		
		$codigo_cliente = $this->session->cliente;
		$codigo_comisionista = $this->Propio_model->obtenerCodigoComisionistaCliente($codigo_cliente);
					
		$output = new stdClass();
		$output->menu = "albaranes_reserva";
		$output->title = "Albaranes en reserva";
		$output->css_panel = true;
		$output->css_postmain = Array("datepicker.css", "albaranes_reserva.css");
		$output->js_premain = Array("bootstrap-datepicker.js");
		$output->js_postmain = Array("clientes/albaranes_reserva.js");
		
		$output->cliente = $this->Propio_model->obtenerDatosCliente($codigo_cliente, $codigo_comisionista);
		
		$serieNumeroAlbaranesReserva = $this->Propio_model->obtenerSerieNumeroAlbaranesReservaClienteInicial($codigo_cliente, $codigo_comisionista);
		$output->registros_iniciales = $this->Propio_model->obtenerLineasAlbaranesReservaClienteInicial($codigo_cliente, $codigo_comisionista, $serieNumeroAlbaranesReserva);
		
		$output->cantidad_registros_totales = $this->Propio_model->obtenerCantidadLineasAlbaranesReservaClienteInicial($codigo_cliente, $codigo_comisionista, $serieNumeroAlbaranesReserva);			
		$output->cantidad_registros_iniciales = count($output->registros_iniciales);
		
		$this->load->view('clientes/albaranes_reserva_view',$output);
		
	}
	
	function obtener_otros_albaranes_reserva () {
		
		$respuesta = new stdClass();
		$respuesta->resuelto = "ER";
		$respuesta->sesion_expirada = false;
		
		if (!$this->session->cliente){
			
			$respuesta->sesion_expirada = true;
			
		}
		elseif (isset($_POST['agrupacion']) && isset($_POST['pagina']) && $this->session->cliente) {
			
			$agrupacion = $_POST['agrupacion'];
			$inicioLimit = $_POST['pagina'] * $agrupacion - $agrupacion;
			$filtros = json_decode($_POST["filtros"]);
			$tipoOrdenacion = $_POST["tipo_ordenacion"];
            $columnaOrdenacion = $_POST["columna_ordenacion"];
			
			$codigo_cliente = $this->session->cliente;
			$codigo_comisionista = $this->Propio_model->obtenerCodigoComisionistaCliente($codigo_cliente);
			
			$serieNumeroAlbaranesReserva = $this->Propio_model->obtenerSerieNumeroAlbaranesReservaClienteInicial($codigo_cliente, $codigo_comisionista);
			
			// Obtenemos los registros y el número de registros para ese filtro			
			$registros = $this->Propio_model->obtenerOtrosAlbaranesReservaCliente($codigo_cliente, $inicioLimit, $agrupacion, $filtros, $tipoOrdenacion, $columnaOrdenacion, $codigo_comisionista, $serieNumeroAlbaranesReserva);
			$respuesta->registrosTotales = $this->Propio_model->obtenerCantidadOtrosAlbaranesReservaCliente($codigo_cliente, $filtros, $codigo_comisionista, $serieNumeroAlbaranesReserva);

			$html = "";
			
			foreach ($registros as $index => $registro) {
				
				$odd_even = $index % 2 == 0 ? "odd" : "even";
				
				if ($registro->descripcionArticulo == "") $registro->descripcionArticulo = "-";
				
				$html .=    "<tr class='".$odd_even."'>".
								"<td>".$registro->albaran."</td>".
								"<td>".fechaToString($registro->fecha)."</td>".
								"<td>".$registro->codigoArticulo."</td>".
								"<td>".$registro->descripcionArticulo."</td>".
								"<td>".round($registro->unidades, 2)."</td>".
							"</tr>";
				
			}

			$respuesta->registros = $html;
			$respuesta->registroInicial = $respuesta->registrosTotales > 0 ? $inicioLimit + 1 : 0;
			$respuesta->registroFinal = ($inicioLimit + $agrupacion) < $respuesta->registrosTotales ? ($inicioLimit + $agrupacion) : $respuesta->registrosTotales;

			$respuesta->resuelto = "OK";
		}
		
		echo json_encode($respuesta);
		
	} 
	
}