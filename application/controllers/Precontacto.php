<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Precontacto extends CI_Controller {

	function __construct() {

		parent::__construct();

		// Si no hay ninguna sesion iniciada, mandarlo al login
		comprobarSesionIniciada();

	}

	public function index() {

		$output = Array(
			"menu" => "precontacto",
			"title" => "Precontacto",
			"css_postmain" => Array("formulario_pedidos_presupuestos.css"),
			"js_postmain" => Array("vtiger.js")
		);

		$this -> load -> view('general/precontacto_view', $output);

	}

	public function oportunidad() {

		$output = Array(
			"menu" => "vtiger",
			"title" => "vTiger",
			"css_postmain" => Array("formulario_pedidos_presupuestos.css"),
			"js_postmain" => Array("vtiger.js")
		);

		$this -> load -> view('vtiger/oportunidad_view', $output);

	}

	public function casos() {

		$output = Array(
			"menu" => "vtiger",
			"title" => "vTiger",
			"css_postmain" => Array("formulario_pedidos_presupuestos.css"),
			"js_postmain" => Array("vtiger.js")
		);

		$this -> load -> view('vtiger/casos_view', $output);

	}

}
