<?php

  function recoger_datos_pedido_presupuesto ($post) {

    $ci = & get_instance();

    $tipo_usuario = $post['tipo_usuario'];

    // Comisionista
    if ($tipo_usuario == "comisionista") {

      // El comisionista sera el logeado
      $comisionista = (object) Array(
        "codigo" => $ci->session->comisionista,
        "nombre" => $ci->session->nombre_comisionista,
        "email" => $ci->session->email_comisionista
      );

      // Recibimos el cliente
      $cliente = (object) Array(
        "codigo" => $post['codigo_cliente'],
        "nombre" => $post['nombre_cliente']
      );

      // Obtenemos la empresa del comisionista logeado
      $empresa = $ci->session->empresa;

    }
    // Cliente
    elseif ($tipo_usuario == "cliente") {

      // Obtenemos el email del comisionista que tiene asignado el cliente. En el caso de que el comisionista no este configurado para recibir los emails de sus clientes, obtendremos false
      $comisionista = (object) Array(
        "codigo" => 0,
        "nombre" => "",
        "email" => $ci->Propio_model->obtenerEmailReenvioClienteComisionista('0002')
      );

      // El cliente sera el logeado
      $cliente = (object) Array(
        "codigo" => $ci->session->cliente,
        "nombre" =>$ci->session->nombre_cliente
      );

      // Obtenemos la empresa del cliente logeado
      $empresa = $ci->Propio_model->obtenerEmpresaCliente($ci->session->cliente);

    }

    // Definimos el objeto pedido
    $pedido = (object) Array(
      "comisionista" => $comisionista,
      "cliente" => $cliente,
      "empresa" => $empresa,
      "tipo_usuario" => $tipo_usuario,
      "numero" => $post['numero'],
      "fecha" => $post['fecha'],
      "tipo" => $post['tipo'],
      "subPedido" => $post['subPedido'],
      "emails_envio" => explode(",",$post['emails_envio']),
      "observaciones" => $post['observaciones'],
      "lineas" => json_decode($post['lineas']),
      "total" => 0
    );

    return $pedido;

  }

  function procesar_pedido_presupuesto ($pedido) {

    $resuelto = "ERROR.";

    // Generamos el pdf del pedido o presupuesto
    $pdf_correcto = generacion_pdf_pedido($pedido);

    // Si no fue correcto el pdf
    if (!$pdf_correcto) $resuelto .= " Error en la generación del pdf.";

    // Enviamos el email con los datos del pedido o presupuesto
    $email_correcto = enviar_email_pedido_presupuesto($pedido);

    // Si no fue correcto el email
    if (!$email_correcto) $resuelto .= " Error en el envío del correo.";

    // Si es un pedido
    if ($pedido->tipo == "Pedido") {

      // Realizamos la insercion del pedido en la BD
      $insercion_correcta = insercion_pedido_base_datos($pedido);

      // Si no fue correcto el email
      if (!$insercion_correcta) $resuelto .= " Error en la inserción del pedido en la base de datos.";

    }

    // Si llega hasta aqui el resuelto sin modificar, es que no ha habido errores
    if ($resuelto == "ERROR.") $resuelto = "OK";
    // Si no, añadimos otra frase más al error
    else $resuelto .= " Por favor, contacte con la empresa.";

    return $resuelto;

  }

  function generacion_pdf_pedido ($pedido) {

    $pdf_correcto = false;

    $ci = & get_instance();

    $ci->load->library('fpdf');
    define('EURO',chr(128));

    $pdf = new FPDF('l');
    $pdf->SetAutoPageBreak(false);

    $pdf->AddPage();

    // Logo
    $pdf->image(base_url()."content/images/logo.png", 10, 15, 26, 26);

    // Tipo, número, subpedido y fecha
    $x = 247;
    $pdf->setY(15); $pdf->setx($x);

    // Tipo
    $pdf->SetFont('arial', 'B' ,21);
    $pdf->setTextColor(5, 20, 80);
    $pdf->Cell(40, 5, $pedido->tipo, 0, "", "R");
    $pdf->Ln(9); $pdf->setx($x);

    // Número
    $pdf->SetFont('arial','B',13);
    $pdf->setx($x);
    $pdf->Cell(40, 5, "Número: ".$pedido->numero, 0, "", "R");

    // SubPedido
    $pdf->SetFont('arial','',11);
    if ($pedido->subPedido != "" && $pedido->tipo == "Pedido") {
      $pdf->Ln(7); $pdf->setx($x);
      $pdf->Cell(40, 4, "SubPedido: ".$pedido->subPedido,0,"","R");
    }
    else{
      $pdf->Ln(2);
    }

    // Fecha
    $pdf->Ln(5); $pdf->setx($x);
    $pdf->Cell(40, 5, "Fecha: ".$pedido->fecha,0,"","R");

    // Cuadro comercial y cliente
    $x = 50;
    $pdf->setXY($x,10);
    $pdf->SetFont('arial','',11);
    $pdf->setFillColor(230,230,230);
    $pdf->setTextColor(10,10,10);
    if ($pedido->tipo_usuario == "comisionista") $pdf->Cell(92, 5, "Comercial y cliente: ");
    elseif ($pedido->tipo_usuario == "cliente") $pdf->Cell(92, 5, "Cliente: ");
    $pdf->setY(15); $pdf->setx($x);
    $pdf->Cell(92, 30, "",0,"","",1);

    // Datos comercial y cliente
    $x = 53;
    $pdf->setXY($x,17);
    $pdf->SetFont('arial','',10);
    $pdf->setTextColor(5,20,80);
    if ($pedido->tipo_usuario == "comisionista") {
      $pdf->MultiCell(86, 5, "Comercial: ".$pedido->comisionista->codigo." - ".$pedido->comisionista->nombre);
      $pdf->Ln(); $pdf->setx($x);
    }
    $pdf->MultiCell(86, 5, "Cliente: ".$pedido->cliente->codigo." - ".$pedido->cliente->nombre);

    // Cuadro observaciones
    $x = 147;
    $pdf->setXY($x,10);
    $pdf->SetFont('arial','',11);
    $pdf->setFillColor(230,230,230);
    $pdf->setTextColor(10,10,10);
    $pdf->Cell(92, 5, "Observaciones:");
    $pdf->setY(15); $pdf->setx($x);
    $pdf->Cell(92, 30, "",1);

    // Observaciones
    $x = 150;
    $pdf->setXY($x,17);
    $pdf->SetFont('arial','',10);
    $pdf->setTextColor(5,20,80);
    $pdf->MultiCell(86, 5, limit_text($pedido->observaciones, 200));

    $odd_even = 0;
    $primera_linea = true;
    $primera_pagina = true;

    // Definimos el total del pedido a 0, ya que se recalculara a continuacion
    $pedido->total = 0;

    foreach ($pedido->lineas as $linea) {

      //Si es el final de la hoja, hacemos un salto de pagina
      if ($pdf->getY() > 160) {
        $pdf->AddPage();
        $primera_linea = true;
      }

      if ($primera_linea) {

        // Cabecera lineas
        $x = 12;
        if ($primera_pagina) {
          $pdf->setXY($x,55);
          $primera_pagina = false;
        }
        else $pdf->setX($x);
        $pdf->SetFont('arial','B',10);
        $pdf->SetDrawColor(150,150,150);
        $pdf->setTextColor(10,10,10);
        $pdf->Cell(35, 7, "Código","TRBL");
        $pdf->Cell(127, 7, "Descripción","TRB");
        $pdf->Cell(20, 7, "Unidades","TRB","","C");
        $pdf->Cell(20, 7, "Precio","TRB","","C");
        $pdf->Cell(20, 7, "Descuento","TRB","","C");
        $pdf->Cell(20, 7, "Total","TRB","","C");
        $pdf->Cell(30, 7, "Prioridad","TRB","","C");
        $pdf->Ln(7);

        $border_1 = "BL";
        $border_2 = "RBL";
        $primera_linea = false;

      }
      else {
        $border_1 = "BL";
        $border_2 = "RBL";
      }

      if ($odd_even % 2 == 0) $pdf->setTextColor(5,20,80);
      else $pdf->setTextColor(10,10,10);
      $odd_even++;

      $pedido->total = $pedido->total + $linea->total;

      switch ($linea->prioridad) {
        case "0" : $prioridad = "Normal"; break;
        case "1" : $prioridad = "Urgente"; break;
        case "2" : $prioridad = "Muy urgente"; break;
      }

      $pdf->setX($x);
      $pdf->SetFont('arial','',8);
      $pdf->Cell(35, 10, preg_replace('/\s\s+/', ' ',$linea->codigo),$border_1,"","L","","",1);
      $pdf->Cell(127, 10, preg_replace('/\s\s+/', ' ',$linea->nombre),$border_1,"","L","","",1);
      $pdf->SetFont('arial','',9);
      $pdf->Cell(20, 10, round($linea->cantidad,2),$border_1,"","C","","",0);
      $pdf->Cell(20, 10, number_format($linea->precio,3).EURO,$border_1,"","C","","",0);
      $pdf->Cell(20, 10, " ".round($linea->descuento,2)."%",$border_1,"","C","","",0);
      $pdf->Cell(20, 10, " ".number_format(round($linea->total,2), 2).EURO,$border_1,"","C","","",0);
      $pdf->Cell(30, 10, $prioridad,$border_2,"","C","","",0);
      $pdf->Ln();
    }
    // Total
    if (count($pedido->lineas)) {
      $pdf->Ln(10);
      $pdf->setX(230);
      $pdf->SetFont('arial','B',10);
      $pdf->setTextColor(5,20,80);
      $pdf->Cell(20, 7, " Total:",0,"","L",1);
      $pdf->SetFont('arial','',10);
      $pdf->Cell(20, 7, number_format($pedido->total, 2)." ".EURO,0,"","R",1,"",0);
    }

    // Comprobamos que no haya que añadir una pagina para el texto de proteccion
    if ($pdf->getY() > 180) $pdf->AddPage();

    // Texto de protección de datos
    $texto_proteccion_1 = "Le informamos que sus datos personales que puedan constar en esta comunicación, están incorporados en un fichero propiedad de LUIS DOMINGUEZ E HIJO, S.L. con la finalidad de gestionar la relación negocial que nos vincula e informarle de nuestros productos y servicios. Si desea ejercitar los derechos de acceso, rectificación, cancelación y oposición puede dirigirse por escrito a: LUIS DOMINGUEZ E HIJO, S.L. Polígono LA VIÑUELA - C/ HOSTELERIA, S/N, 14900, LUCENA, CORDOBA. En el caso de que no desee recibir más información sobre los servicios que ofrecemos puede enviar un mensaje a la siguiente dirección de correo electrónico:";
    $texto_proteccion_2 = "valentin@abrastar.com";
    $texto_proteccion_3 = "El contenido del correo electrónico y sus anexos son estrictamente confidenciales. En caso de no ser usted el destinatario y haber recibido este mensaje por error, agradeceríamos que lo comunique inmediatamente al remitente, sin difundir, almacenar o copiar su contenido.";
    $texto_proteccion_4 = "LUIS DOMINGUEZ E HIJO, S.L. Polígono LA VIÑUELA - C/ HOSTELERIA, S/N, 14900, LUCENA, CORDOBA. Tel: +34 957 202 202 - Fax: +34 957 502 926";
    $texto_proteccion_5 = "www.abrasivos.net - www.abrastar.com";
    $pdf->SetFont('arial','',5);
    $pdf->setY(185); $pdf->setX(10);
    for ($i=1; $i<6; $i++) {
      $pdf->MultiCell(277, 3, ${"texto_proteccion_".$i}, 0, "C");
    }

    $result = $pdf->Output('tmp/'.$pedido->numero.".pdf");

    // Si el PDF es correcto, lo indicamos
    if ($result !== false) $pdf_correcto = true;

    return $pdf_correcto;

  }

  function enviar_email_pedido_presupuesto ($pedido) {

    $email_correcto = false;

    $ci = & get_instance();

    $ci->load->library('email');

    // Asunto
    $subject = $pedido->tipo.' web: '.$pedido->numero;

    // Mensaje
    $nombreCreador = $pedido->tipo_usuario == "comisionista" ? $pedido->comisionista->nombre : $pedido->cliente->nombre;
    $message = '
      <p>'.strtoupper($pedido->tipo).'</p>
      <p>Codigo del cliente: '.$pedido->cliente->codigo.'</p>
      <p>Nombre del cliente: '.$pedido->cliente->nombre.'</p>
      <p>Fecha: '.$pedido->fecha.'</p>
      <p>Creado por: '.$nombreCreador.' ('.$pedido->tipo_usuario.')</p>
      <p>Observaciones: '.$pedido->observaciones.'</p>
      <p>Subpedido web: '.$pedido->subPedido.'</p>
      <p>Total: '.$pedido->total.' €</p>';

    // Email en HTML:
    $body =
    '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset='.strtolower(config_item('charset')).'" />
        <title>'.html_escape($subject).'</title>
        <style type="text/css">
    body {
    font-family: Arial, Verdana, Helvetica, sans-serif;
    font-size: 16px;
    }
        </style>
      </head>
      <body>
        '.$message.'
      </body>
    </html>';

    // Asignamos los valores al email y lo enviamos
    $ci->email->from('webcic_noreply@abrastar.com', 'Abrastar');

    // Obtenemos los emails de envio que haya configurado el admin
    $emails_configuracion = $ci->Propio_model->obtener_emails_configuracion();

    // Nos recorremos los emails de la configuracion para enviarles el pedido
    foreach($emails_configuracion as $email_configuracion) {
      $email_configuracion = trim($email_configuracion);
      if (valid_email($email_configuracion)) $ci->email->to($email_configuracion);
    }

    // Le enviamos el pedido por email al comisionista, si es un email valido
    if (valid_email($pedido->comisionista->email)) $ci->email->to($pedido->comisionista->email);

    // Enviamos la factura a cada email escrito
    foreach($pedido->emails_envio as $email_envio) {
      $email_envio = trim($email_envio);
      if (valid_email($email_envio)) $ci->email->to($email_envio);
    }
    $ci->email->subject($subject);
    $ci->email->message($body);

    // Adjuntamos el pdf si existe
    if (file_exists("tmp/".$pedido->numero.".pdf")) $ci->email->attach("tmp/".$pedido->numero.".pdf");

    $ci->email->set_smtp_conn_options(Array(
      "ssl" => Array(
        "verify_peer" => false,
        "verify_peer_name" => false,
        "allow_self_signed" => true
      )
    ));
    $result = $ci->email->send();

    // Borramos el pdf si existe
    if (file_exists("tmp/".$pedido->numero.".pdf")) unlink ("tmp/".$pedido->numero.".pdf");

    // Si se produce un error en el envio del correo, lo advertimos
    if ($result !== false) $email_correcto = true;

    return $email_correcto;

  }

  function insercion_pedido_base_datos ($pedido) {

    $insercion_correcta = false;

    $ci = & get_instance();

    // Obtenemos las lineas con el nombre de los productos recortado, por si excede el maximo de caracteres
    $lineas_nombre_recortado = recortar_nombre_productos($pedido->lineas);
    // Recortamos tambien las observaciones, por si fallan tambien
    if (strlen($pedido->observaciones) > 50) $pedido->observaciones = substr($pedido->observaciones, 0, 46)." [+]";

    /*** INSERCION EN LA BASE DE DATOS *****************************************************************/
    $datos = (object) Array(
      "empresaComisionista" => $pedido->empresa,
      "codigoCliente" => $pedido->cliente->codigo,
      "nombreCliente" => str_replace("'","''",$pedido->cliente->nombre),
      "numeroPedido" => $pedido->numero,
      "subpedido" => str_replace("'","''",$pedido->subPedido),
      "vpedidoweb" => date("Y")."/".$pedido->numero, // Unión del año con el numero del pedido web
      "observaciones" => $pedido->observaciones,
      "lineas" => $lineas_nombre_recortado
    );

    // Insertamos el pedido mediante su correspondiente modelo
    $result_1 = $ci->Propio_model->insercionPedido($datos);

    // Actualizamos el registro
    $result_2 = $ci->Propio_model->actualizarRegistroPedido($pedido->cliente->codigo, $pedido->comisionista->codigo, $pedido->numero);

    // Si se produce un error en la inserción del pedido, lo advertimos
    if (($result_1 !== false) && ($result_2 !== false)) $insercion_correcta = true;

    return $insercion_correcta;

  }

?>
