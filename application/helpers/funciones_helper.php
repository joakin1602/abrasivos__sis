<?php

	function comprobarSesionIniciada($tipos_usuario=false) {
		$CI = & get_instance();
		// Si queremos comprobar que existe la sesion abierta de un usuario de cualquier tipo
		if (!$tipos_usuario) {
			// No hay ninguna sesion
			if (!($CI -> session -> login)){
				header("Location: ".base_url()."index.php/login");
				die;
			}
		}
		// Si queremos comprobar que exista la sesion de uno o unos tipos de usuario en concreto
		else {
			// No hay ninguna sesion
			if (!($CI -> session -> login)){
				header("Location: ".base_url()."index.php/login");
				die;
			}
			elseif ((!in_array($CI -> session -> tipo_usuario, $tipos_usuario))) {
				header("Location: ".base_url());
				die;
			}
		}
	}

	function inicioSesionAdmin(){
		$CI = & get_instance();
		$CI -> session -> set_userdata('login',true);
		$CI -> session -> set_userdata('tipo_usuario','admin');
		$CI -> session -> set_userdata('empresa', 1);
		$CI -> session -> set_userdata('admin', 1);
		$CI -> session -> set_userdata('ultima_accion',time());
	}

	function inicioSesionComisionista($datos){
		$CI = & get_instance();
		$CI -> session -> set_userdata('login',true);
		$CI -> session -> set_userdata('tipo_usuario','comisionista');
		$CI -> session -> set_userdata('empresa',trim($datos->empresa));
		$CI -> session -> set_userdata('comisionista',trim($datos->codigo));
		$CI -> session -> set_userdata('nombre_comisionista',trim($datos->nombre));
		$CI -> session -> set_userdata('email_comisionista',trim($datos->email));
		$CI -> session -> set_userdata('ultima_accion',time());
	}

	function inicioSesionCliente($datos){
		$CI = & get_instance();
		$CI -> session -> set_userdata('login',true);
		$CI -> session -> set_userdata('tipo_usuario','cliente');
		$CI -> session -> set_userdata('empresa',trim($datos->empresa));
		$CI -> session -> set_userdata('cliente',trim($datos->codigo));
		$CI -> session -> set_userdata('nombre_cliente',trim($datos->nombre));
		$CI -> session -> set_userdata('email_cliente',trim($datos->email));
		$CI -> session -> set_userdata('ultima_accion',time());
	}

	function fechaToString ($fechaSQL) {

		$fechaSQL = explode(" ", $fechaSQL);
		$fechaSQL = explode("-", $fechaSQL[0]);

		if (count($fechaSQL) >= 3){
			$fechaString = $fechaSQL[2]."/".$fechaSQL[1]."/".$fechaSQL[0];
		}
		else {
			$fechaString = "-";
		}

		return $fechaString;

	}

	function fechaToSQL ($fechaString) {

		$fechaString = explode("/", $fechaString);

		if (count($fechaString) >= 3){
			$fechaSQL = $fechaString[2]."-".$fechaString[1]."-".$fechaString[0];
		}
		else {
			$fechaSQL = "-";
		}

		return $fechaSQL;

	}

	function fechaHoraToString ($fechaSQL) {

		$fechaSQL = explode(" ", $fechaSQL);

		if (count($fechaSQL) == 2){

			$diaSQL = explode("-", $fechaSQL[0]);
			$horaSQL = explode(":", $fechaSQL[1]);

			$fechaString = $diaSQL[2]."/".$diaSQL[1]."/".$diaSQL[0]." ".$horaSQL[0].":".$horaSQL[1];

		}
		else $fechaString = "-";

		return $fechaString;

	}

	function obtenerNumeroFormularioPedidos() {

		$archivo = "numero.txt";

		$fp = fopen($archivo,"r");
		$numero1 = intval(fgets($fp, 26));
		fclose($fp);

		$numero2 = $numero1 + 1;

		$fp = fopen($archivo,"w");
		fwrite($fp, $numero2, 26);
		fclose($fp);

		return $numero1;

	}

	function nombre_empresa_ucwords ($nombre_empresa) {

		$nombre_empresa_final = "";

		// Separamos la empresa en palabras, en un array
		$nombre_empresa_inicial = explode(" ", trim($nombre_empresa));

		// Definimos las abreviaturas, que no se pasaran a minusculas
		$abreviaturas = Array('S.L.','SL','S.A.','SA','C.B.','CB','S.C.A','SCA','S.L.U.','SLU','S.L.L.','SLL','S.C.','SC','S.R.L.','SRL','SL*');

		// Nos recorremos las palabras
		foreach ($nombre_empresa_inicial as $palabra_nombre_empresa) {

			// Ejecutamos ucwords a las que no sean abreviaturas
			if (!in_array($palabra_nombre_empresa, $abreviaturas)) $palabra_nombre_empresa = ucwords(strtolower($palabra_nombre_empresa));

			$nombre_empresa_final .= " ".$palabra_nombre_empresa;

		}

		return $nombre_empresa_final;

	}

	function recortar_nombre_productos ($lineas) {

		// Nos recorremos las lineas
		foreach ($lineas as $index => $linea) {

			// Si el nombre del producto excede de 50 caracteres, lo recortamos
			if (strlen($linea->nombre) > 50) {

				$lineas[$index]->nombre = substr($linea->nombre, 0, 46)." [+]";

			}

		}

		return $lineas;

	}

	function limit_text($text, $limit) {
		if (strlen($text) > $limit) {

			$text = substr($text, 0, $limit) . '[...]';

		}

		return $text;
    }

	function pasar_pedido_a_fichero ($datos) {

		$resuelto = false;

		$empresa = $datos['empresa'];
		$numero = $datos['numero'];
		$fecha = date("Y-m-d");
		$cliente = $datos['cliente'];
		$subpedido= $datos['subpedido'];
		$observaciones = $datos['observaciones'];
		$lineas = $datos['lineas'];

		//lo primero que vamos a hacer es que si el archivo existe ver por k pedido va de murano para rescatarlo;
		$url = "./importarpedidos/pedido.txt";
		if (file_exists($url))
		{
			if(filesize($url) > 0)
			{
				//creamos una variable para saber cual es el maximo pedido
				$max=000000;

				$file = fopen($url, "r");
				while ($linea = fgets($file,1024))
				{
					//leemos linea por linea mientras creamos la sentencia de
					if ($linea)
					{
						if($linea!='-'.PHP_EOL)
						{
							$linea = explode(";",$linea);
							$linea[1]=(int)$linea[1];

							//si este pedido es el maximo se mete en la variable max
							if($max<$linea[1])
							{
								$max=$linea[1];
							}
						}
					}
				}
				//Cerramos el fichero
				fclose ($file);
				//ponemos max como el ultimo mas 1
				$max++;
			}
			else
			{
				//si el archivo existe pero esta vacio ponemos max automaticamente a 1
				$max = 00001;
			}
		}
		else
		{
			//si el archivo no existe ponemos max automaticamente a 1
			$max = 00001;
		}

		//esta variable es la que almacenara todos los datos
		$total='';

		// Nos recorremos las lineas, en el caso que haya, para generar el texto a introducir en el fichero
		$total_lineas = count($lineas);
		if ($total_lineas > 0) {

			for ($i=0; $i<$total_lineas; $i++) {

				// Los datos generales del pedido se repiten en todas las lineas
				$total.=$empresa.";".$max.";".$numero.";".$fecha.";".$cliente.";";

				// Añadimos el texto de cada linea en concreto
				$total.=$lineas[$i]->codigo.";";
				$total.=$lineas[$i]->nombre.";";
				$total.=$lineas[$i]->cantidad.";";
				$total.=$lineas[$i]->precio.";";
				$descuento = str_replace(',','.',$lineas[$i]->descuento);
				$formateardescuento = number_format($descuento,0,',','.');
				$total.=$formateardescuento.";";
				$total.=$lineas[$i]->neto.";";
				$total.=$lineas[$i]->total.";";

				// El subpedido también aparece en todas las líneas
				$total.=$subpedido."<br>";

			}

		}

		// Finalmente incluimos la linea que contiene el comentario del pedido
		$total.=$empresa.";".$max.";".$numero.";".$fecha.";".$cliente.";*;".$observaciones."<br>";

		// Volvemos a abrir el fichero, para cambiar el modo de lectura a escritura
		$file = fopen($url, "a");

		// Indicamos si se realiza la inserción correctamente
		if (fputs($file, $total)) $resuelto = true;

		fclose ($file);

		return $resuelto;

	}

	function calcular_descuentos_articulos ($articulos, $descuentos_cliente) {

		// Nos recorremos los articulos
		foreach ($articulos as $index => $articulo) {

			// Le damos un valor al descuento de 0 inicialmente
			$articulos[$index]->descuento = 0.00;

			// Nos recorremos los descuentos, buscando el descuento para el articulo actual
			foreach ($descuentos_cliente as $descuento) {

				// Si encuentra descuento para el articulo actual
				if ($articulo->codigo == $descuento->CodigoArticulo) {

					// Asignamos el descuento correspondiente si este no es del 100%
					if ($descuento->descuento != 100) $articulos[$index]->descuento = $descuento->descuento;

					// Dejamos de buscar descuento para este articulo
					break;

				}

			}

		}

		return $articulos;

	}

	function obtener_boton_stock ($stock_virtual, $stock_disponible, $pendiente_servir, $pendiente_recibir, $pendiente_fabricar) {

		// Si hay stock disponible del articulo, y no hay salidas previstas
		if ($stock_disponible > 0 && ($pendiente_recibir + $pendiente_fabricar) >= $pendiente_servir) $boton_stock = "btn-success";
		// Si hay stock disponible del articulo, pero hay salidas previstas
		elseif ($stock_disponible > 0 && ($pendiente_recibir + $pendiente_fabricar) < $pendiente_servir) $boton_stock = "btn-warning";
		// Si no hay stock disponible del articulo pero hay pendiente de recibir o fabricar
		else if ($stock_disponible <= 0 && (($pendiente_recibir + $pendiente_fabricar) > 0)) $boton_stock = "btn-warning";
		else $boton_stock = "btn-danger";

		return $boton_stock;

	}

	function obtener_total_pedido ($lineas) {

		$total_pedido = 0;

		foreach ($lineas as $linea) {

			$total_pedido += $linea->total;

		}

		return $total_pedido;

	}

  function comisionista_pedido_vacio () {

    $comisionista = (object) Array(
      "codigo" => 0,
      "nombre" => "",
      "email" => ""
    );

    return $comisionista;

  }

	function comprobar_existen_posiciones_array ($array, $posiciones_array) {

		$todos_existen = true;

		foreach ($posiciones_array as $posicion) {

			if (!isset($array[$posicion])) $todos_existen = false;

		}

		return $todos_existen;

	}

	function valid_email($address) {

        $patternselect = 'auto';

        if (function_exists('idn_to_ascii') && $atpos = strpos($address, '@')) {
            $address = substr($address, 0, ++$atpos).idn_to_ascii(substr($address, $atpos));
        }

        if (!$patternselect or $patternselect == 'auto') {
            //Check this constant first so it works when extension_loaded() is disabled by safe mode
            //Constant was added in PHP 5.2.4
            if (defined('PCRE_VERSION')) {
                //This pattern can get stuck in a recursive loop in PCRE <= 8.0.2
                if (version_compare(PCRE_VERSION, '8.0.3') >= 0) {
                    $patternselect = 'pcre8';
                } else {
                    $patternselect = 'pcre';
                }
            } elseif (function_exists('extension_loaded') and extension_loaded('pcre')) {
                //Fall back to older PCRE
                $patternselect = 'pcre';
            } else {
                //Filter_var appeared in PHP 5.2.0 and does not require the PCRE extension
                if (version_compare(PHP_VERSION, '5.2.0') >= 0) {
                    $patternselect = 'php';
                } else {
                    $patternselect = 'noregex';
                }
            }
        }
        switch ($patternselect) {
            case 'pcre8':
                /**
                 * Uses the same RFC5322 regex on which FILTER_VALIDATE_EMAIL is based, but allows dotless domains.
                 * @link http://squiloople.com/2009/12/20/email-address-validation/
                 * @copyright 2009-2010 Michael Rushton
                 * Feel free to use and redistribute this code. But please keep this copyright notice.
                 */
                return (boolean)preg_match(
                    '/^(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){255,})(?!(?>(?1)"?(?>\\\[ -~]|[^"])"?(?1)){65,}@)' .
                    '((?>(?>(?>((?>(?>(?>\x0D\x0A)?[\t ])+|(?>[\t ]*\x0D\x0A)?[\t ]+)?)(\((?>(?2)' .
                    '(?>[\x01-\x08\x0B\x0C\x0E-\'*-\[\]-\x7F]|\\\[\x00-\x7F]|(?3)))*(?2)\)))+(?2))|(?2))?)' .
                    '([!#-\'*+\/-9=?^-~-]+|"(?>(?2)(?>[\x01-\x08\x0B\x0C\x0E-!#-\[\]-\x7F]|\\\[\x00-\x7F]))*' .
                    '(?2)")(?>(?1)\.(?1)(?4))*(?1)@(?!(?1)[a-z0-9-]{64,})(?1)(?>([a-z0-9](?>[a-z0-9-]*[a-z0-9])?)' .
                    '(?>(?1)\.(?!(?1)[a-z0-9-]{64,})(?1)(?5)){0,126}|\[(?:(?>IPv6:(?>([a-f0-9]{1,4})(?>:(?6)){7}' .
                    '|(?!(?:.*[a-f0-9][:\]]){8,})((?6)(?>:(?6)){0,6})?::(?7)?))|(?>(?>IPv6:(?>(?6)(?>:(?6)){5}:' .
                    '|(?!(?:.*[a-f0-9]:){6,})(?8)?::(?>((?6)(?>:(?6)){0,4}):)?))?(25[0-5]|2[0-4][0-9]|1[0-9]{2}' .
                    '|[1-9]?[0-9])(?>\.(?9)){3}))\])(?1)$/isD',
                    $address
                );
            case 'pcre':
                //An older regex that doesn't need a recent PCRE
                return (boolean)preg_match(
                    '/^(?!(?>"?(?>\\\[ -~]|[^"])"?){255,})(?!(?>"?(?>\\\[ -~]|[^"])"?){65,}@)(?>' .
                    '[!#-\'*+\/-9=?^-~-]+|"(?>(?>[\x01-\x08\x0B\x0C\x0E-!#-\[\]-\x7F]|\\\[\x00-\xFF]))*")' .
                    '(?>\.(?>[!#-\'*+\/-9=?^-~-]+|"(?>(?>[\x01-\x08\x0B\x0C\x0E-!#-\[\]-\x7F]|\\\[\x00-\xFF]))*"))*' .
                    '@(?>(?![a-z0-9-]{64,})(?>[a-z0-9](?>[a-z0-9-]*[a-z0-9])?)(?>\.(?![a-z0-9-]{64,})' .
                    '(?>[a-z0-9](?>[a-z0-9-]*[a-z0-9])?)){0,126}|\[(?:(?>IPv6:(?>(?>[a-f0-9]{1,4})(?>:' .
                    '[a-f0-9]{1,4}){7}|(?!(?:.*[a-f0-9][:\]]){8,})(?>[a-f0-9]{1,4}(?>:[a-f0-9]{1,4}){0,6})?' .
                    '::(?>[a-f0-9]{1,4}(?>:[a-f0-9]{1,4}){0,6})?))|(?>(?>IPv6:(?>[a-f0-9]{1,4}(?>:' .
                    '[a-f0-9]{1,4}){5}:|(?!(?:.*[a-f0-9]:){6,})(?>[a-f0-9]{1,4}(?>:[a-f0-9]{1,4}){0,4})?' .
                    '::(?>(?:[a-f0-9]{1,4}(?>:[a-f0-9]{1,4}){0,4}):)?))?(?>25[0-5]|2[0-4][0-9]|1[0-9]{2}' .
                    '|[1-9]?[0-9])(?>\.(?>25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}))\])$/isD',
                    $address
                );
            case 'html5':
                /**
                 * This is the pattern used in the HTML5 spec for validation of 'email' type form input elements.
                 * @link http://www.whatwg.org/specs/web-apps/current-work/#e-mail-state-(type=email)
                 */
                return (boolean)preg_match(
                    '/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}' .
                    '[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/sD',
                    $address
                );
            case 'noregex':
                //No PCRE! Do something _very_ approximate!
                //Check the address is 3 chars or longer and contains an @ that's not the first or last char
                return (strlen($address) >= 3
                    and strpos($address, '@') >= 1
                    and strpos($address, '@') != strlen($address) - 1);
            case 'php':
            default:
                return (boolean)filter_var($address, FILTER_VALIDATE_EMAIL);
        }
    }

	function round_to_2dp ($number) {

		return number_format((float)$number, 2, '.', '');

	}

?>
